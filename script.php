<?php
include "shopifyapps/config.php";
include "shopifyapps/shopify_call.php";
$token = "99d9b0ea5d09d6e51fa4a25487d993a9";
$shop = "auto-add.myshopify.com";
$theme_get = shopify_call($token, $shop, "/admin/themes.json", array('role' => 'main'), 'GET');
$theme_get = json_decode($theme_get['response']);
$theme_id = $theme_get->themes[0]->id;
    $asset = array("asset" =>
        array(
            "key" => "snippets/activecart.liquid",
            "value" => '<script type="text/javascript">
  window.saso = {
    shop: "{{shop.permanent_domain}}",
    shop_slug: "{{shop.permanent_domain | remove: \'.myshopify.com\' }}",
    money_format: "{{shop.money_format}}",
    customer: null,
    cart: null,
  }
</script> 
<script>
  function StartCart(){
    jQuery.ajax({
      cache: false,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      type: "POST",
      url: "cart.js",
      success: function(res) {
        if(res.item_count > 0){
          window.saso.cart = res;
          StartApp();
        }
      }
    });
  }
  function StartApp(){
    jQuery.ajax({
      dataType: "json",
      type: "POST",
      url: "https://www.activecartapp.com/activecart/activecart.php?action=autoadd",
      data: JSON.stringify(window.saso),
      success: function(data) {
        //alert(data.cart_items.length);  
        var cart_items = data.cart_items;
        var i;
        var items = "";
        for (i = 0; i < cart_items.length; i++) { 
          if(items == ""){
            items += \'updates[\'+cart_items[i]+\']=1\';
          }else{
            items += \'&updates[\'+cart_items[i]+\']=1\';
          }
        }
        if(data.cart_reload == 0){
          $.ajax({
            type: "POST",
            url: "/cart/update.js",
            dataType: "json",
            data: items,
            success: function(){
              location.reload();
            },
            error: function(){}
          });
        }
        var auto_add_items = data.auto_add_items;
        var offer_price = data.offer_price;
        var j;
        window.saso.auto_add_items = auto_add_items;
        window.saso.offer_price = offer_price;
        for (j = 0; j < auto_add_items.length; j++) { 
          
          var original_price = sasoShopifyformatMoney(auto_add_items[j].price,window.saso.money_format);
          var discounted_price = sasoShopifyformatMoney((auto_add_items[j].price - (auto_add_items[j].price * offer_price) /100),window.saso.money_format); 

          var original_line_price = sasoShopifyformatMoney(auto_add_items[j].line_price,window.saso.money_format);
          var discounted_line_price = sasoShopifyformatMoney((auto_add_items[j].line_price - (auto_add_items[j].line_price * offer_price) /100),window.saso.money_format); 

          var id = auto_add_items[j].product_id;
          jQuery(".saso-cart-item-price[data-key=\'"+id+"\']").html("<span class=\"original_price\">"+original_price+"</span>" + "<span class=\'discounted_price\'>"+discounted_price+"</span>");
          jQuery(".saso-cart-item-line-price[data-key=\'"+id+"\']").html("<span class=\"original_price\">"+original_line_price+"</span>" + "<span class=\'discounted_price\'>"+discounted_line_price+"</span>");
          jQuery(".saso-cart-original-total").css("text-decoration","line-through");
        }
        jQuery(".saso-cart-total").html(sasoShopifyformatMoney(data.main_total,window.saso.money_format));
      }
    });
  }
  function sasoShopifyformatMoney(cents, format) {
    if (typeof cents == "undefined" || cents == null) {
      return ""
    }
    if (typeof cents == "string" && cents.length == 0) {
      return ""
    }

    var value = "",
        placeholderRegex = /\{\{\s*(\w+)\s*\}\}/,
        formatString = format || this.money_format;
    if (typeof cents == "string") {
      cents = cents.replace(".", "")
    }

    function defaultOption(opt, def) {
      return typeof opt == "undefined" ? def : opt
    }

    function formatWithDelimiters(number, precision, thousands, decimal) {
      precision = defaultOption(precision, 2);
      thousands = defaultOption(thousands, ",");
      decimal = defaultOption(decimal, ".");
      if (isNaN(number) || number == null) {
        return 0
      }
      number = (number / 100).toFixed(precision);
      var parts = number.split("."),
          dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + thousands),
          cents = parts[1] ? decimal + parts[1] : "";
      return dollars + cents
    }
    switch (formatString.match(placeholderRegex)[1]) {
      case "amount":
        value = formatWithDelimiters(cents, 2);
        break;
      case "amount_no_decimals":
        value = formatWithDelimiters(cents, 0);
        break;
      case "amount_with_comma_separator":
        value = formatWithDelimiters(cents, 2, ".", ",");
        break;
      case "amount_no_decimals_with_comma_separator":
        value = formatWithDelimiters(cents, 0, ".", ",");
        break
    }
    return formatString.replace(placeholderRegex, value)
  }
  var dtolCheckoutButtonSelector = ["[name=\'checkout\']", "[href=\'/checkout\']", "form[action=\'/checkout\'] input[type=\'submit\']", "input[name=\'checkout\']", "button[name=\'checkout\']", "button.new_checkout_button", "input[value=\'Checkout\']", "input[value=\'Check out\']", "button:contains(\'Checkout\'):not(.cart_button)", "button:contains(\'Check out\'):not(.cart_button)", "a[href=\'/checkout\']", "input[name=\'goto_pp\']", "button.additional-checkout-button", "div.additional-checkout-button","#paypal-express-button",".additional-checkout-button--apple-pay"].join(", ");
  var directCheckoutButtonSelector = ["button:contains(\'Checkout\'):not(.cart_button)","button:contains(\'Check Out\'):not(.cart_button)","button:contains(\'Check out\'):not(.cart_button)", "button:contains(\'CHECKOUT\'):not(.cart_button)"].join(", ");
  $(document).on("click.olEvents", dtolCheckoutButtonSelector, StartCheckour);
  function StartCheckour(e, clicked_element){
    
    
    var note_attributes = [];
    jQuery("[name^=\'attributes\']").each(function() {
        var $a = jQuery(this);
        var name = jQuery(this).attr("name");
        name = name.replace(/^attributes\[/i, "").replace(/\]$/i, "");
        var v = {
            name: name,
            value: $a.val()
        };
        if (v.value == "") {
            return
        }
        switch ($a[0].tagName.toLowerCase()) {
            case "input":
                if ($a.attr("type") == "checkbox") {
                    if ($a.is(":checked")) {
                        note_attributes.push(v)
                    }
                } else {
                    note_attributes.push(v)
                }
                break;
            default:
                note_attributes.push(v)
        }
    });
    
    var note = "";
    if (jQuery("[name=\'note\']").length == 1 && jQuery("[name=\'note\']")[0].value) {
        note = jQuery("[name=\'note\']")[0].value
    }
    window.saso.cart.note_attributes = note_attributes;
    window.saso.cart.note = note;
    e.preventDefault();
    $.ajax({
      dataType: "json",
      type: "POST",
      url: "https://www.activecartapp.com/activecart/activecart.php?action=checkout",
      data: JSON.stringify(window.saso),
      success: function(data) {
        if(data.invoice_url != ""){
        	window.location.href = data.invoice_url.substring(0, 2e3)
        }else{
        	alert("test");
        }
      },
      error: function(){}
    });
  }

  StartCart();
  /*$(document).ready(function () {
    $(document).ajaxSuccess(function(a, b, c) {
      if (c.url === "/cart/add.js" || c.url === "/cart/change.js" || c.url === "/cart.js") {
        alert();

      }
    });
  });*/
</script>
<style>
  .saso-cart-item-price .original_price, .saso-cart-item-line-price .original_price {
    display: block; 
    text-decoration: line-through; 
  }
  .saso-cart-item-price .discounted_price, .saso-cart-item-line-price .discounted_price,.saso-cart-total {
    display: block;
    font-weight: bold;
  }
</style>'
        )
    );
    /* create auto-add snippets */
    $snippest_add = shopify_call($token, $shop, "/admin/themes/" . $theme_id . "/assets.json", $asset, 'PUT');
    /* For add snippets include in theme.liquid file*/
    $theme_file_get = shopify_call($token, $shop, "/admin/themes/{$theme_id}/assets.json?asset[key]=layout/theme.liquid&theme_id={$theme_id}", array(), 'GET');
    if ($theme_file_get['response'] != '') {
        $theme_file_get = json_decode($theme_file_get['response']);
        $data = $theme_file_get->asset->value;
        $position_snippets = strpos($data, "{% include 'auto-add' %}");
        if ($position_snippets === false) {
            $data = str_replace("</body>", "{% include 'auto-add' %}</body>", $data);
            $theme_file_get = shopify_call($token, $shop, "/admin/themes/{$theme_id}/assets.json", array('asset' => array('key' => 'layout/theme.liquid', 'value' => $data)), 'PUT');
        }
    }
    /* for add class in cart tamplate */
    $cart_file_get = shopify_call($token, $shop, "/admin/themes/{$theme_id}/assets.json?asset[key]=templates/cart.liquid&theme_id={$theme_id}", array(), 'GET');
    if ($cart_file_get['response'] != '') {
        $cart_file_get = json_decode($cart_file_get['response']);
        $item_price = '<span class="saso-cart-item-price" data-key="{{item.product.id}}">{{ item.price | money }}</span>';
        $variables_item = array('{{ item.price | money }}' => $item_price,
                    '{{ item.price | money_without_trailing_zeros }}' => $item_price,
                    '{{ item.price | money_with_currency }}' => $item_price
                );
        $asset_value = $cart_file_get->asset->value;
        foreach ($variables_item as $key => $value) {
            $asset_value = str_replace($key, $value, $asset_value, $count);
        }
        $item_line_price = '<span class="saso-cart-item-line-price" data-key="{{item.product.id}}">{{ item.line_price | money }}</span>';
        $variables_line_item = array('{{ item.line_price | money }}' => $item_line_price,
                    '{{ item.line_price | money_without_trailing_zeros }}' => $item_line_price,
                    '{{ item.line_price | money_with_currency }}' => $item_line_price
                );
        foreach ($variables_line_item as $key => $value) {
            $asset_value = str_replace($key, $value, $asset_value, $count);
        }
        $cart_price = '<span class="saso-cart-original-total">{{ cart.total_price | money }}</span><span class="saso-cart-total"></span>';
        $variables_cart = array('{{ cart.total_price | money }}' => $cart_price,
                    '{{ cart.total_price | money_without_trailing_zeros }}' => $cart_price,
                    '{{ cart.total_price | money_with_currency }}' => $cart_price
                );
        foreach ($variables_cart as $key => $value) {
            $asset_value = str_replace($key, $value, $asset_value, $count);
        }
        $theme_file_get = shopify_call($token, $shop, "/admin/themes/{$theme_id}/assets.json", array('asset' => array('key' => 'templates/cart.liquid', 'value' => $asset_value)), 'PUT');
    }
?>