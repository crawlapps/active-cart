<?php
/* include main config file file */
include_once('include/config.php');
include_once 'shopifyapps/config.php';

$shop = "crawlapps-info.myshopify.com";

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSS -->

    <script src="assets/js/jquery-2.1.1.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>

    <!-- *********** Start : App Bridge ************ -->
    <script src="https://unpkg.com/@shopify/app-bridge@2.0.5"></script>
    <script src="https://unpkg.com/@shopify/app-bridge-utils"></script>
    <script>
        async function getToken() {
            var AppBridge = window['app-bridge'];
            var createApp = AppBridge.default;
            var getSessionToken = window['app-bridge-utils'].getSessionToken;
            var app = createApp({
                apiKey: '<?php echo SHOPIFY_API_KEY; ?>',
                shopOrigin: 'crawlapps-info.myshopify.com'
            });
            let sessionToken = getSessionToken(app)
            await sessionToken.then(function(result) {
                document.getElementById('token').value = result;
                console.log(result);
            });
            console.log(document.getElementById('token').value)
            // XMLHttpRequest.setRequestHeader("Authorization", "Bearer "+document.getElementById('token').value)

        }
        getToken();
        $.ajax({
            type: "GET",
            url: "/activecart/shopifyapps/config.php",
            success: function(responseData) {
                console.log('responce ', responseData);
            }
        });
    </script>


</head>

<body>
    <input type="text" id="token">