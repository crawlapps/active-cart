<?php

include_once '../include/config.php';
include "../shopifyapps/shopify_call.php";
include_once '../include/front-functions.php';
$general_function = new Front_functions();


$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$product_update = file_get_contents('php://input');
$verified = $general_function->verify_webhook($product_update, $hmac_header); 

if (isset($_GET['inapp']) && $_GET['inapp'] == '1') {
    $where = "WHERE store_name = '" . $_GET['store'] . "'";
    $shop = $general_function->select(TABLE_CLIENT_STORES, $where);

    if (isset($shop) && $shop->num_rows > 0) {
        $shop_details = $shop->fetch_object();
        $store_client_id = $shop_details->store_client_id;
        $productinfo = json_decode($product_update);
        $variants_id = $productinfo->variants[0]->id;

        $fields = array(
            'variant_id' => $variants_id
        );
        $where = 'store_client_id = ' . $store_client_id . ' AND product_id = "' . $productinfo->id . '"';
        $general_function->update(TABLE_GETY_PRODUCTS, $fields, $where);

        $fields = array(
            'title' => $productinfo->title,
            'image' => $productinfo->image == '' ? '' : $productinfo->image->src,
        );
        $where = 'store_client_id = ' . $store_client_id . ' AND product_id = "' . $productinfo->id . '"';
        $general_function->update(TABLE_PRODUCT_LIST, $fields, $where);
    }
}