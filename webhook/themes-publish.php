<?php

include_once '../include/config.php';
include "../shopifyapps/config.php";
include "../shopifyapps/shopify_call.php";
include_once '../include/front-functions.php';
$general_function = new Front_functions();

$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$theme_publish = file_get_contents('php://input');
$verified = $general_function->verify_webhook($theme_publish, $hmac_header);
if (isset($_GET['inapp']) && $_GET['inapp'] == '1' && $verified != "") {
    $shop = $_GET['store'];
    $where = "where store_name = '" . $shop . "'";
    $shop_details = $general_function->select(TABLE_CLIENT_STORES, $where);
    if (isset($shop_details) && $shop_details->num_rows > 0) {
        $shop_detail = $shop_details->fetch_object();
        $store_client_id = $shop_detail->store_client_id;
        $token = $shop_detail->token;

        $theme_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes.json", array('role' => 'main'), 'GET');
        $theme_get = json_decode($theme_get['response']);
        $theme_id = $theme_get->themes[0]->id;

        $theme_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=layout/theme.liquid&theme_id={$theme_id}", array(), 'GET');
        if ($theme_file_get['response'] != '') {
            $theme_file_get = json_decode($theme_file_get['response']);
            $data = $theme_file_get->asset->value;
            $position_snippets = strpos($data, "{{ '" . SITE_PATH . "/assets/js/activecart.js' | script_tag }}");
            if ($position_snippets === false) {
                $data = str_replace("</body>", "\r\n<!-- Active Cart Code Start -->\r\n{{ '" . SITE_PATH . "/assets/js/activecart.js' | script_tag }}\r\n<!-- Active Cart Code End --></body>", $data);
                $theme_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'layout/theme.liquid', 'value' => $data)), 'PUT');
            }
        }

        $cart_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=templates/cart.liquid&theme_id={$theme_id}", array(), 'GET');
        if ($cart_file_get['response'] != '') {
            $count_templates_cart = 0;
            $cart_file_get = json_decode($cart_file_get['response']);
            $item_price = '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>';
            $variables_item = array('{{ item.price | money }}' => $item_price,
                '{{ item.price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.price | money_with_currency }}' => $item_price,
                '{{ item.original_price | money }}' => $item_price,
                '{{ item.original_price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.original_price | money_with_currency }}' => $item_price
            );
            $asset_value = $cart_file_get->asset->value;
            $asset_value_bkp = $cart_file_get->asset->value;
            foreach ($variables_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $item_line_price = '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>';
            $variables_line_item = array('{{ item.line_price | money }}' => $item_line_price,
                '{{ item.line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.line_price | money_with_currency }}' => $item_line_price,
                '{{ item.original_line_price | money }}' => $item_line_price,
                '{{ item.original_line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.original_line_price | money_with_currency }}' => $item_line_price
            );
            foreach ($variables_line_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $cart_price = '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>';
            $variables_cart = array('{{ cart.total_price | money }}' => $cart_price,
                '{{ cart.total_price | money_without_trailing_zeros }}' => $cart_price,
                '{{ cart.total_price | money_with_currency }}' => $cart_price
            );
            foreach ($variables_cart as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            if ($count_templates_cart > 0) {
                $cart_file_get_bkp = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=templates/cart-auto-add-bkp.liquid&theme_id={$theme_id}", array(), 'GET');
                if ($cart_file_get_bkp['response'] == '') {
                    shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'templates/cart-auto-add-bkp.liquid', 'value' => $asset_value_bkp)), 'PUT');
                }
                shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'templates/cart.liquid', 'value' => $asset_value)), 'PUT');
            }
        }
        $cart_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=sections/cart-template.liquid&theme_id={$theme_id}", array(), 'GET');
        if ($cart_file_get['response'] != '') {
            $count_sections_cart = 0;
            $cart_file_get = json_decode($cart_file_get['response']);
            $item_price = '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>';
            $variables_item = array('{{ item.price | money }}' => $item_price,
                '{{ item.price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.price | money_with_currency }}' => $item_price,
                '{{ item.original_price | money }}' => $item_price,
                '{{ item.original_price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.original_price | money_with_currency }}' => $item_price
            );
            $asset_value = $cart_file_get->asset->value;
            $asset_value_bkp = $cart_file_get->asset->value;
            foreach ($variables_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $item_line_price = '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>';
            $variables_line_item = array('{{ item.line_price | money }}' => $item_line_price,
                '{{ item.line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.line_price | money_with_currency }}' => $item_line_price,
                '{{ item.original_line_price | money }}' => $item_line_price,
                '{{ item.original_line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.original_line_price | money_with_currency }}' => $item_line_price
            );
            foreach ($variables_line_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $cart_price = '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>';
            $variables_cart = array('{{ cart.total_price | money }}' => $cart_price,
                '{{ cart.total_price | money_without_trailing_zeros }}' => $cart_price,
                '{{ cart.total_price | money_with_currency }}' => $cart_price
            );
            foreach ($variables_cart as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_sections_cart++;
                    }
                }
            }
            
            if ($count_sections_cart > 0) {
                $cart_file_get_bkp = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=sections/cart-template-auto-add-bkp.liquid&theme_id={$theme_id}", array(), 'GET');
                if ($cart_file_get_bkp['response'] == '') {
                    shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'sections/cart-template-auto-add-bkp.liquid', 'value' => $asset_value_bkp)), 'PUT');
                }
                shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'sections/cart-template.liquid', 'value' => $asset_value)), 'PUT');
            }
        }
    }
}