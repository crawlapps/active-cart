<?php
include_once '../include/config.php';
include_once '../include/en.php';
include_once '../include/functions.php';
$general_function = new general_function();
//$_GET['inapp'] = 1;
//$_GET['store'] = 'auto-add.myshopify.com';
if (isset($_GET['inapp']) && $_GET['inapp'] == '1') {
    $fields = array(
        'status' => '0',
        'charge_approve' => '0'
    );
    $store_name = $_GET['store'];
    $where = "WHERE store_name='$store_name' LIMIT 0,1";
    $is_store = $general_function->select(TABLE_CLIENT_STORES, $where);
    if (isset($is_store) && $is_store->num_rows > 0) {
        $is_store = $is_store->fetch_object();
        $client_id = $is_store->client_id;
        $shop_name = $is_store->shop_name;
        $Install_date = $is_store->created;
        $where = "WHERE client_id='$client_id' LIMIT 0,1";
        $result = $general_function->select(TABLE_CLIENTS, $where);
    } else {
        $where = "WHERE email='$user_email' LIMIT 0,1";
        $result = $general_function->select(TABLE_CLIENTS, $where);
    }
    if (isset($result) && $result->num_rows > 0) {
        $client = $result->fetch_object();
        $user_email = $client->email;
        $subject = 'Thank you for Installing. Could you provide quick feedback?';
        $message = 'Hello '.$shop_name.',
                    <p>Robert here from Active Cart. I wanted to say thank you for installing and trying Active Cart with your business, '.$shop_name.', since '.$Install_date.'. We noticed you uninstalled and wanted to reach out. </p>
                    <p>Could you provide a little insight / explain a little about your experience? Anything helps! Especially if you could share a little more on why you uninstalled.</p>
                    <p>We just launched Active Cart and are actively working to make it better for you and other Shopify entrepreneurs. Any feedback would be greatly appreciated.</p>
                    <p>Hope to hear back from you soon!</p>';
       $send_email = $general_function->send_email(SITE_NAME, SITE_ADMIN_EMAIL, $user_email, $subject, $message);
    }
    $where = "store_name = '" . $_GET['store'] . "'";
    $general_function->update(TABLE_CLIENT_STORES, $fields, $where);
} else {
    echo "Access Denied";
    exit;
}
?>