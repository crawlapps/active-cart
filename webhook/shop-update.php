<?php
include_once '../include/config.php';
include "../shopifyapps/config.php";
include "../shopifyapps/shopify_call.php";
include_once '../include/front-functions.php';
$general_function = new Front_functions();

$hmac_header = $_SERVER['HTTP_X_SHOPIFY_HMAC_SHA256'];
$shop_update = file_get_contents('php://input');
$verified = $general_function->verify_webhook($shop_update, $hmac_header);
if (isset($_GET['inapp']) && $_GET['inapp'] == '1') {
    $store_name = $_GET['store'];
    $shopinfo = json_decode($shop_update);
    $fields = array('money_format' => htmlspecialchars($shopinfo->money_format, ENT_QUOTES, "ISO-8859-1"),'shop_name' => htmlspecialchars($shopinfo->name, ENT_QUOTES, "ISO-8859-1"));
    $where = "store_name = '" . $store_name . "'";
    $general_function->update(TABLE_CLIENT_STORES, $fields, $where);
}