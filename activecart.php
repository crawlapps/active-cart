<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Origin, Content-Type, accept, authorization, X-Requested-With');
/* include main config file file */
include_once ('include/config.php');
/* include main client function file */
include_once ('include/front-functions.php');

if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'autoadd') {
    $functions = new Front_functions(); /* we set customer cart in constructor */
    $reload = 1;
    $post = $functions->get_user_cart();
    if (isset($post['shop']) && $post['shop'] != "") {
        $shop = $post['shop'];
        $shopinfo = $functions->get_shop($shop);
        if (isset($shopinfo) && $shopinfo->num_rows > 0) {
            $shopinfo = $shopinfo->fetch_object();
            $store_client_id = $shopinfo->store_client_id;
            $money_format = $shopinfo->money_format;
            if (isset($post['cart']) && isset($post['cart']['items']) && !empty($post['cart']['items'])) {

                /* set all required (entity) value */
                $functions->set_all_variable($store_client_id);

                /* get main product from cart */
                $main_prod_id_array = array_diff($functions->cart_prod_id_arr, $functions->cart_tracking_prod_id_arr);
                
                /* get offer product from cart */
                $offer_prod_id_array = array_diff($functions->cart_prod_id_arr, $main_prod_id_array);

                /* we go for check offer product removed or not (This portion is used when cart has already some product)*/
                $need_remove_prod = $functions->is_need_remove_offer_prod($store_client_id, $offer_prod_id_array);

                /* we set product on which we give discount according to type1 */
                $functions->set_type1_offer_prod_id_arr($store_client_id, $main_prod_id_array);
                
                /* We get type1 offer product id array */
                $type1_offer_prod_id_arr = $functions->get_type1_offer_prod_id_arr();
                
                /* remove offer product(but only auto added prouduct) when main product remove */
                $remove_cart_tracking_prod_arr = array_diff($offer_prod_id_array, array_keys($type1_offer_prod_id_arr));
                //$remove_cart_tracking_prod_arr = array_diff($remove_cart_tracking_prod_arr, array_column((array)$functions->type2_cart_tracking_product_arr,'product_id'));
                /* we remove all offer product if its main product remove */
                $update_remove_prod_arr = array();
                $is_main_rmv_flag=0;
                if (!empty($remove_cart_tracking_prod_arr)) {
                    $update_remove_prod_arr = $functions->remove_offer_prod_cart_tracking($store_client_id, $remove_cart_tracking_prod_arr);
                    $is_main_rmv_flag=1;
                }
                
                /* End Of remove offer product when main product remove */


                /* now we insert all those product in to cart tracking product */
                $functions->insert_into_cart_tracking_table($store_client_id, $type1_offer_prod_id_arr);

                /* now we get those product which we required to add in to cart 
                 * (means if product already in cart than no need to add )
                 * this process is for only and only type1 (buy X get Y product)
                 */
                
                $update_cart_prod_id_arr = $functions->get_type1_add_prod_id_arr();

                /* Here we add all those product which we need to update with either quantity update or remove from cart */
                if (!empty($update_remove_prod_arr)) {
                    $update_cart_prod_id_arr = array_merge($update_cart_prod_id_arr, $update_remove_prod_arr);
                }
                
                if (!empty($update_cart_prod_id_arr)) {
                    echo json_encode(array(
                        /* required for add product in cart */
                        'cart_items' => $update_cart_prod_id_arr,
                        //'approx_cust_spend' => $approx_cust_spend,
                        'cart_reload' => $reload)
                    );
                } else {
                    $reload = 0;
                    $discount_display_response=$functions->get_discount_display_response();
                    //$discount_display_response['approx_cust_spend'] = $approx_cust_spend;
                    echo json_encode($discount_display_response);
                }
            } else {
                $reload = 0;
                $msg='fail';
                if(isset($store_client_id) && $store_client_id != '' && isset($post['cart']['token']) && $post['cart']['token'] != ''){
                    $msg='success';
                    $functions->delete_cart_tracking_product($store_client_id, $post['cart']['token']);
                }
                echo json_encode(array('msg' => $msg));
            }
        }
    }
} else if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'auto-add-display-discount') {
    $functions = new Front_functions(); /* we set customer cart in constructor */
    $discount_display_response=$functions->get_discount_display_response();

    echo json_encode($discount_display_response);
} else if (isset($_REQUEST['action']) && $_REQUEST['action'] == 'checkout') {
    include_once 'shopifyapps/config.php';
    include_once 'shopifyapps/shopify_call.php';
    /* we send FALSE because we want post object rather than the array means it defualt retrun array */
    $functions = new Front_functions(FALSE); /* we set customer cart in constructor */
    $post = $functions->get_user_cart();
    
    $shop = $post->shop;
    $shopinfo = $functions->get_shop($shop);
    if (isset($shopinfo) && $shopinfo->num_rows > 0) {
        $shopinfo = $shopinfo->fetch_object();
        $store_client_id = $shopinfo->store_client_id;

        $spend_offer_id = $post->spend_offer_id;
        
        $draft_order_array = array();

        $line_items = array();
        
        /* FOR DISCOUNT ON SPEND */
        if($spend_offer_id > 0){
            $discount_type = 2 ;
            $spend_offer_product_dbr = $functions->get_offer_product_dbr($store_client_id, $spend_offer_id, $discount_type);
            $cart_prod_variant_id_arr = array_column((array)$post->cart->items, 'quantity', 'id');
            
            if($spend_offer_product_dbr->num_rows > 0){
                while($offer = $spend_offer_product_dbr->fetch_assoc()){
                    $offer_spend_variant_id_arr[]=$offer['variant_id'];
                    $quantity = (isset($cart_prod_variant_id_arr[$offer['variant_id']])) ? ($cart_prod_variant_id_arr[$offer['variant_id']]+1):1;
                    $per_product_dis=$offer['product_price'] / $quantity;
                    $line_items[] = array(
                        "variant_id" => $offer['variant_id'],
                        "quantity" => $quantity,
                        "applied_discount" => array(
                            "title" => "Discount",
                            "description" => "Description of discount!",
                            "value" => $per_product_dis,
                            "value_type" => "fixed_amount",
                            "amount" => $per_product_dis * $quantity
                        )
                    );
                } 
            }
        } 
        if(!empty($post->auto_add_items) || !empty($line_items)){
            $products_variant = $post->auto_add_items;
            $auto_add_items_detail_arr = $post->auto_add_items_detail_arr;
            foreach($post->cart->items as $item){
                
                $variant_id=$item->id;
                if(in_array($variant_id, $products_variant)){
                    $decimals = $functions->decimals;
                    
//                    $discount_per = $auto_add_items_detail_arr->$variant_id->discount_per;
//                    $price = $item->price; /* Here we divede by 100 coz shopify store without fraction point value */
//                    $price = $price * $item->quantity;
//                    $price_discount = intval(($price * $discount_per) / 100);
//                    $value = $new_discount_amt = $price_discount;
//                    $value = $value / 100;  
//                    $new_discount_amt = $new_discount_amt / 100;  
                    
                    
                    
                    
                    
                    $discount_per = $auto_add_items_detail_arr->$variant_id->discount_per;
                    $discounted_price_calculted = (($item->price * $discount_per) / 100);
                    $value = $new_discount_amt = $discounted_price_calculted;
//                    $value = bcdiv($value,'100',2);  
//                    $new_discount_amt = bcdiv($new_discount_amt, '100', 2);
                    $value = truncate_number($value / 100);  
                    $value = truncate_number($value / $item->quantity);  
                    $new_discount_amt = truncate_number($new_discount_amt / 100);
                    $new_discount_amt = truncate_number($new_discount_amt / $item->quantity);
                    
//                    $line_itme_final_price = number_format($item->price / 100, 2, '.', '');
//                    $new_discount_amt = get_shopify_discount($line_itme_final_price, $discount_per);
                    
                    
                    $line_items[] = array(
                        "variant_id" => $variant_id,
                        "quantity" => $item->quantity,
                        "applied_discount" => array(
                            "title" => "Discount",
                            "description" => "Description of discount!",
                            "value" => $value,
                            "value_type" => "fixed_amount",
                            "amount" => $new_discount_amt * $item->quantity /* if value_type is percentage than used this formula(price * quantity * value) / 100 */
                        )
                    );
                }elseif(empty($offer_spend_variant_id_arr) || !in_array($variant_id, $offer_spend_variant_id_arr)){
                    $properties = (array)$item->properties;
                    $properties_arr = array();
                    $properties_arr_main = array();
                    foreach($properties as $key => $value){
                        $properties_arr['name'] = $key;
                        $properties_arr['value'] = $value;
                        $properties_arr_main[] = $properties_arr;
                    }
                    $line_items[] = array(
                        "variant_id" => $variant_id,
                        "quantity" => $item->quantity,
                        "properties" => $properties_arr_main,
                    );
//                    if($shopinfo->store_name == "buenavida-fire-pits.myshopify.com"){
//                        print_r($properties_arr_main);
//                    }
                }
            }

        }
        if(!empty($line_items)){
            $draft_order_array = array("draft_order" =>
                array(
                    "line_items" => $line_items,
                    "note" => $post->cart->note,
                    "note_attributes" => $post->cart->note_attributes
                )
            );
            if(isset($post->customer_id) && $post->customer_id > 0){
                $draft_order_array['draft_order']['customer'] = array('id'=>$post->customer_id);
            }
//            if($shopinfo->store_name == "active-cart.myshopify.com"){
//                print_r($draft_order_array);
//            }
            $draft_order = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."draft_orders.json", json_encode($draft_order_array), 'POST', array("Content-Type: application/json"));
            $temp = $draft_order;
            $draft_order = json_decode($draft_order['response']);
            if($draft_order->draft_order->invoice_url == ""){
                print_r($temp);
                 print_r(json_encode($draft_order_array));
            }
            echo json_encode(array('invoice_url' => $draft_order->draft_order->invoice_url));
            exit;
        }
        
    } /* end if which check for shop */
    echo json_encode(array('invoice_url' => ''));
    exit;
}

function truncate_number( $number, $precision = 2) {
    // Zero causes issues, and no need to truncate
    if ( 0 == (int)$number ) {
        return $number;
    }
    // Are we negative?
    $negative = $number / abs($number);
    // Cast the number to a positive to solve rounding
    $number = abs($number);
    // Calculate precision number for dividing / multiplying
    $precision = pow(10, $precision);
    // Run the math, re-applying the negative value to ensure returns correctly negative / positive
    return floor( $number * $precision ) / $precision * $negative;
}

function get_shopify_discount($amount, $rate) {
    //$discount = number_format($amount * ( $rate / 100),2,'.','');
    $discount = $amount * ( $rate / 100);
    /* Rounding the amount */
    $discount = $discount * pow(10, 2);
    $discount = floatval($discount);
    $discount = $discount / pow(10, 2);
    /* Prevent error on shopify reorder Becase shopify not want round amount on 5 */
    $pos = strpos($discount, '.');
    $sub_str = substr($discount, $pos + 3, 1);
    if ($pos != 0 && $sub_str == 5) {
        $final_discount = floor($discount * 100) / 100;
    } else {
        $final_discount = round($discount, 2);
    }
    return $final_discount;
}
?>