<?php

/**
 * Configuration for: Database Connection
 * This is the place where your database login constants are saved
 *
 * For more info about constants please @see http://php.net/manual/en/function.define.php
 * If you want to know why we use "define" instead of "const" @see http://stackoverflow.com/q/2447791/1114320
 *
 * DB_HOST: database host, usually it's "127.0.0.1" or "localhost", some servers also need port info
 * DB_NAME: name of the database. please note: database and database table are not the same thing
 * DB_USER: user for your database. the user needs to have rights for SELECT, UPDATE, DELETE and INSERT.
 *          by the way, it's bad style to use "root", but for development it will work.
 * DB_PASS: the password of the above user
 */
if ($_SERVER['SERVER_NAME'] == 'localhost') {
    define("DB_SERVER", "localhost");
    define("DB_DATABASE", "auto-add-app");
    define("DB_USERNAME", "root");
    define("DB_PASSWORD", "");
    define('MODE', 'dev');
    error_reporting(E_ALL);
    ini_set('display_errors', 1);

    /* define site url and admin details */
    // define('SITE_URL', 'http://localhost/private-apps/activecart/');
    // define('SITE_CLIENT_URL', 'http://localhost/private-apps/activecart/client/');
    // define('SITE_ADMIN_URL', 'http://localhost/private-apps/activecart/admin/');

    define('SITE_URL', 'https://localhost/activecart-dev/');
    define('SITE_CLIENT_URL', 'https://localhost/activecart-dev/client/');
    define('SITE_ADMIN_URL', 'https://localhost/activecart-dev/admin/');

    // testing configurations
} else if ($_SERVER['SERVER_NAME'] == 'www.activecart.crawlapps.com' || $_SERVER['SERVER_NAME'] == 'activecart.crawlapps.com') {

    define("DB_SERVER", "localhost");
    define("DB_DATABASE", "activecart");
    define("DB_USERNAME", "root");
    define("DB_PASSWORD", 'as345dt5dsf!dfdse4g$fh!!!547sd69df54as');
    define('MODE', 'live');
    ini_set('display_errors', 1);

    /* define site url and admin details */
    define('SITE_URL', 'https://activecart.crawlapps.com/');
    define('SITE_CLIENT_URL', 'https://activecart.crawlapps.com/client/');
    define('SITE_ADMIN_URL', 'https://activecart.crawlapps.com/admin/');

} else if ($_SERVER['SERVER_NAME'] == 'www.activecartapp.com' || $_SERVER['SERVER_NAME'] == 'activecartapp.com') {
    define("DB_SERVER", "localhost");
    define("DB_DATABASE", "activecart");
    define("DB_USERNAME", "activecart");
    define("DB_PASSWORD", "Activecart@99");
    define('MODE', 'live');

    /* define site url and admin details */
    define('SITE_URL', 'https://www.activecartapp.com/activecart/');
    define('SITE_CLIENT_URL', 'https://www.activecartapp.com/activecart/client/');
    define('SITE_ADMIN_URL', 'https://www.activecartapp.com/activecart/admin/');
}else if ($_SERVER['SERVER_NAME'] == 'activecart.test' || $_SERVER['SERVER_NAME'] == 'activecart.test') {
    define("DB_SERVER", "localhost");
    define("DB_DATABASE", "auto-add-app");
    define("DB_USERNAME", "root");
    define("DB_PASSWORD", "");
    define('MODE', 'dev');
    error_reporting(E_ALL);
    ini_set('display_errors', 1);


    /* define site url and admin details */
    define('SITE_URL', 'https://activecart.test/');
    define('SITE_CLIENT_URL', 'https://activecart.test/client/');
    define('SITE_ADMIN_URL', 'https://activecart.test/admin/');
} else {
    echo 'Undefine host';
    exit;
}

/*
 * Database connection
 *  */

class DB_Class
{

    function __construct()
    {
        $odbclink = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
        if (mysqli_connect_errno()) {
            echo "Failed : connect to MySQL: " . mysqli_connect_error();
            die;
        }
        $GLOBALS['conn'] = $odbclink;
        return $odbclink;
    }
}

define('ABS_PATH', dirname(dirname(__FILE__)));
define('SENDGRID_API_KEY', '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');
define('EMAIL_SEND', 'sendgrid');
define('SITE_NAME', 'Active Cart');
define('SITE_ADMIN_EMAIL', 'activecartapp@gmail.com');
//define('SITE_COPYRIGHT', SITE_NAME . ' © ' . date('Y') . ' - Develop by <a target="_blank" href="http://www.identixweb.com">Identixweb</a> team');
define('SITE_COPYRIGHT', '<a target="_blank" href="https://activecartapp.com">' . SITE_NAME . '</a>' . ' © ' . date('Y') . '');

/**
 * Configuration for: verification email data
 * Set the absolute URL to register.php, necessary for email verification links
 */
define("EMAIL_VERIFICATION_URL", SITE_URL);
define("EMAIL_VERIFICATION_FROM", "no-reply@example.com");
define("EMAIL_VERIFICATION_FROM_NAME", SITE_NAME);
define("EMAIL_VERIFICATION_SUBJECT", "Account activation for " . SITE_NAME);
define("EMAIL_VERIFICATION_CONTENT", "Please click on this link to activate your account:");

define('TABLE_CLIENTS', 'clients');
define('TABLE_CLIENT_STORES', 'client_stores');
define('TABLE_OFFERS', 'offers');
define('TABLE_GETY_PRODUCTS', 'gety_products');
define('TABLE_PRODUCT_LIST', 'product_list');
define('TABLE_CART_TRACKING', 'cart_tracking');
define('TABLE_SETTINGS', 'settings');
define('TABLE_FONT_FAMILY', 'font_family');

/* Database formate Date decalre */
define('DATE', date('Y-m-d'));

/* Pagination decalre */
define('PAGE_PER', '10');

/* user side error msg */
define('DONT_HAVE_PRO_PLAN_MSG', "You haven't able to chage status.You have to need upgarade your plan.");
define('DONT_HAVE_PRO_PLAN_ONLY_ONE_ADD_MSG', "You haven't pro plan so you can add only one product in offer.");

/* admin side error msg */

define("SOMETHING_WENT_WRONG_MSG", "Something went wrong");
define("SETTINGS_CREATED_SUCCESS_MSG", "Setting updated successfully");

define("PT_TEXT_COLOR_REQUIRED_MSG", "Product title text color required");
define("PT_TEXT_COLOR_FORMAT_MSG", "Product title text color should have hex value");
define("PT_FONT_SIZE_REQUIRED_MSG", "Product title font size required");
define("PT_FONT_SIZE_ONLYFLOAT_MSG", "Product title font size must be float value");

define("ORI_PRICE_COLOR_REQUIRED_MSG", "Original price color required");
define("ORI_PRICE_COLOR_FORMAT_MSG", "Original price color should have hex value");
define("ORI_PRICE_FONT_SIZE_REQUIRED_MSG", "Original price font size required");
define("ORI_PRICE_FONT_SIZE_ONLYFLOAT_MSG", "Original price font size must be float value");

define("DIS_PRICE_COLOR_REQUIRED_MSG", "Discount price color required");
define("DIS_PRICE_COLOR_FORMAT_MSG", "Discount price color should have hex value");
define("DIS_PRICE_FONT_SIZE_REQUIRED_MSG", "Discount price font size required");
define("DIS_PRICE_FONT_SIZE_ONLYFLOAT_MSG", "Discount price font size must be float value");

define("IMAGE_WIDTH_REQUIRED_MSG", "Image width required");
define("IMAGE_WIDTH_ONLYFLOAT_MSG", "Image width must be float value");

define("IMAGE_HEIGHT_REQUIRED_MSG", "Image height required");
define("IMAGE_HEIGHT_ONLYFLOAT_MSG", "Image height must be float value");

define("NOA_MESSAGE_REQUIRED_MSG", "None of above message required");

define("NOA_COLOR_REQUIRED_MSG", "None of above message color required");
define("NOA_COLOR_FORMAT_MSG", "None of above message color should have hex value");
define("NOA_FONT_SIZE_REQUIRED_MSG", "None of above message font size required");
define("NOA_FONT_SIZE_ONLYFLOAT_MSG", "None of above message font size must be float value");

define("POST_NOT_SUBMITED_ERR_MSG", "Post values are not submitted");

/*api version constant*/
define('SHOPIFY_API_VERSION', '/admin/api/2021-07/');

define('FILE_NOT_EXISTS_MSG', 'File not exists');

// Links of title bar

define('LINK_HOWTOUSE_TEXT', "How to Use?");
define('LINK_SETTINGS_TEXT', "Settings");
define('LINK_DASHBOARD_TEXT', "Dashboard");
define('LINK_SUPPORT_TEXT', "Support/FAQ");
