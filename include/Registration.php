<?php

/**
 * Handles the user registration
 * @author Panique
 * @link http://www.php-login.net
 * @link https://github.com/panique/php-login-advanced/
 * @license http://opensource.org/licenses/MIT MIT License
 */
class Registration extends general_function {

    /**
     * @var object $db_connection The database connection
     */
    public $db_connection = null;

    /**
     * @var bool success state of registration
     */
    public $registration_successful = false;

    /**
     * @var bool success state of verification
     */
    public $verification_successful = false;

    /**
     * @var array collection of error messages
     */
    public $errors = array();

    /**
     * @var array collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct() {
        if ($this->db_connection == null) {
            $db_connection = new DB_Class();
            $this->db_connection = $GLOBALS['conn'];
        }
    }

    /**
     * handles the entire registration process shopify. checks all error possibilities, and creates a new user in the database if
     * everything is fine
     */
    public function registerNewUserShopify($shop_details) {
        $fields = array();
        $fields_type_1 = array();
        $fields_type_2 = array();
        /* we just remove extra space on username and email */
        $user_email = trim($shop_details['user_email']);
        $store_name = $shop_details['shop'];
        $shop_name = $shop_details['name'];
        $shop_plan = $shop_details['shop_plan'];
        $store_token = $shop_details['token'];
        $money_format = $shop_details['money_format'];

        /* check provided data validity */
        /* TODO: check for "return true" case early, so put this first */
        $flag = false;
        if (empty($user_email)) {
            $this->errors[] = MESSAGE_EMAIL_EMPTY;
        } elseif (strlen($user_email) > 64) {
            $this->errors[] = MESSAGE_EMAIL_TOO_LONG;
        } elseif (!filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
            $this->errors[] = MESSAGE_EMAIL_INVALID;
        } elseif (empty($store_name)) {
            $this->errors[] = MESSAGE_STORE_NAME_EMPTY;
        } elseif (empty($store_token)) {
            $this->errors[] = MESSAGE_STORE_TOKEN_EMPTY;

            /* finally if all the above checks are ok */
        } else if ($this->db_connection) {
            /* check if username or email already exists */
            $where = "WHERE store_name='$store_name' LIMIT 0,1";
            $is_store = $this->select(TABLE_CLIENT_STORES, $where);
            if (isset($is_store) && $is_store->num_rows > 0) {
                $is_store = $is_store->fetch_object();
                $client_id = $is_store->client_id;
                $where = "WHERE client_id='$client_id' LIMIT 0,1";
                $result = $this->select(TABLE_CLIENTS, $where);
            } else {
                $where = "WHERE email='$user_email' LIMIT 0,1";
                $result = $this->select(TABLE_CLIENTS, $where);
            }

            /* if user already available than */
            if (isset($result) && $result->num_rows > 0) {
                $client = $result->fetch_object();

                /* insert store into table */
                /* if user already available than */
                $where = "WHERE client_id=$client->client_id AND store_name= '$store_name' LIMIT 0,1";
                $is_store_exist = $this->select(TABLE_CLIENT_STORES, $where);
                if (isset($is_store_exist) && $is_store_exist->num_rows > 0) {
                    $fields = array(
                        'token' => $store_token,
                        'status' => '1',
                        'shop_plan' => $shop_plan,
                        'money_format' => $money_format,
                        'created' => DATE
                    );
                    $where = 'client_id = ' . $client->client_id . ' AND store_name = "' . $store_name . '"';
                    $this->update(TABLE_CLIENT_STORES, $fields, $where);
                    $client_store = $is_store_exist->fetch_object();
                    $client_store_id = $client_store->store_client_id;
                } else {
                    $fields = array(
                        'client_id' => $client->client_id,
                        'store_name' => $store_name,
                        'shop_name' => $shop_name,
                        'money_format' => $money_format,
                        'token' => $store_token,
                        'status' => '1',
                        'shop_plan' => $shop_plan,
                        'created' => DATE
                    );
                    $client_store_id = $this->insert(TABLE_CLIENT_STORES, $fields);

                    $fields_type_1 = array(
                        'store_client_id' => $client_store_id,
                        'title' => 'Buy X Get Y',
                        'type' => '1',
                        'status' => '0',
                        'created' => DATE
                    );
                    $this->insert(TABLE_OFFERS, $fields_type_1);
                    /* set default css settings */
                    $this->set_default_settigs($client_store_id);
                }

                /* update client status and make new store as default */
                $fields = array(
                    'store_client_id' => $client_store_id,
                    'status' => '1',
                );
                $where = 'client_id = ' . $client->client_id;
                $this->update(TABLE_CLIENTS, $fields, $where);
                // $this->db_connection->query("UPDATE " . TABLE_CLIENTS . " SET store_client_id = $client_store_id, status = 1 where client_id = $client->client_id");
                $flag = true;
            } else {

                /* generate random hash for email verification (40 char string) */
                $user_activation_hash = sha1(uniqid(mt_rand(), true));

                /* write new users data into database */
                $app_key = md5(time() . generateRandomString());

                $fields = array(
                    'email' => $user_email,
                    'user_role' => '2',
                    'app_key' => $app_key,
                    'user_activation_hash' => $user_activation_hash,
                    'status' => '1',
                    'created' => DATE
                );
                $client_id = $this->insert(TABLE_CLIENTS, $fields);

                if ($client_id) {
                    /* insert record into client store table */
                    $fields = array(
                        'client_id' => $client_id,
                        'store_name' => $store_name,
                        'shop_name' => $shop_name,
                        'money_format' => $money_format,
                        'token' => $store_token,
                        'status' => '1',
                        'shop_plan' => $shop_plan,
                        'created' => DATE
                    );
                    $client_store_id = $this->insert(TABLE_CLIENT_STORES, $fields);

                    $fields_type_1 = array(
                        'store_client_id' => $client_store_id,
                        'title' => 'Buy X Get Y',
                        'type' => '1',
                        'status' => '0',
                        'created' => DATE
                    );
                    $this->insert(TABLE_OFFERS, $fields_type_1);
                    /* set default css settings */
                    $this->set_default_settigs($client_store_id);

                    $fields = array(
                        'store_client_id' => $client_store_id,
                        'status' => '1',
                    );
                    $where = 'client_id = ' . $client_id;
                    $this->update(TABLE_CLIENTS, $fields, $where);

                    $this->registration_successful = true;
                    $flag = true;
                } else {
                    $this->errors[] = MESSAGE_REGISTRATION_FAILED;
                }
            }
            $subject = 'Welcome to Active Cart! Let\'s start selling more by giving more';
            $message = 'Hello,
                        <p>Thank you for Installing Active Cart App today. My name is Robert and I am here to help you in any way you may see fit.</p>
                        <p></p>
                        <p>Common Suggestions to get you started;</p>
                        <p></p>
                        <ul style="list-style-type: none;">
                            <li>1. Decide what kind of campaign you want to run</li>
                            <ul style="list-style-type: none;">
                                <li>1. For a Buy X Get Y campaign we suggest the following</li>
                                <ul style="list-style-type: none;">
                                    <li>1. Decide what product you want to add an Incentive to. Your customers must purchase this item to Get the reward.</li>
                                    <li>2. Decide what product (the reward) the customer will receive.</li>
                                    <li>3. Create a graphic and add to the product page to ensure every visitor knows about the offer. See <a href="https://auto-add-2.myshopify.com/" target="_blank">example</a></li>
                                </ul>
                                <li>2. For a Spend X Get Y campaign we suggest the following</li>
                                <ul style="list-style-type: none;">
                                    <li>1. Decide how much a customer must spend in order to receive anything</li>
                                    <li>2. Decide what product the customer will receive</li>
                                    <li>3. Add a notification bar to your store ensuring all visitors know about the special. We suggest <a href="https://www.hellobar.com/" target="_blank">HelloBar</a> or a <a href="http://privy.com/" target="_blank">Privy</a> popup for this.</li>
                                </ul>
                            </ul>
                        </ul>
                        <p>I hope this helps! Please let us know if you have any questions!</p>
                        <p></p>
                        <p>From all of us at Active Cart, Welcome!
                        <p></p>
                        <p>-Closing Notes: Please ensure that the reply @ goes to <a href="mailto:'.SITE_ADMIN_EMAIL.'">'.SITE_ADMIN_EMAIL.'</a>.';
            $send_email = $this->send_email(SITE_NAME, SITE_ADMIN_EMAIL, $user_email, $subject, $message);
        }
        return $flag;
    }

    /**
     * get error and message set at the time of process action
     */
    public function getValidationMessage() {
        // show potential errors / feedback (from login object) */
        if ($this->errors) {
            echo '<div class = "alert alert-danger fade in">';
            foreach ($this->errors as $error) {
                echo $error;
            }
            echo '</div>';
        }
        if ($this->messages) {
            echo '<div class = "alert alert-success fade in">';
            foreach ($this->messages as $message) {
                echo $message;
            }
            echo '</div>';
        }
    }

    private function set_default_settigs($store_client_id){
        $insert_on_duplicate_update_query = "INSERT INTO ".TABLE_SETTINGS." (`store_client_id`,`font_family`,`product_title_css`,`original_pirce_css`,`discount_pirce_css`,`image_css`,`none_of_above_text`,`none_of_above_css`,`created_on`,`updated_on`) VALUES('$store_client_id','','a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"18\";}','a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"16\";}','a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"16\";}','a:2:{s:5:\"width\";s:2:\"65\";s:6:\"height\";s:2:\"95\";}','NONE OF ABOVE','a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"18\";}','" . date('Y-m-d H:i:s') . "','" . date('Y-m-d H:i:s') . "') ON DUPLICATE KEY UPDATE `store_client_id`='store_client_id',`font_family`='',`product_title_css`='a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"18\";}',`original_pirce_css`='a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"16\";}',`discount_pirce_css`='a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"16\";}',`image_css`='a:2:{s:5:\"width\";s:2:\"65\";s:6:\"height\";s:2:\"95\";}',`none_of_above_text`='NONE OF ABOVE',`none_of_above_css`='a:2:{s:5:\"color\";s:7:\"#000000\";s:9:\"font-size\";s:2:\"18\";}',`created_on`='" . date('Y-m-d H:i:s') . "',`updated_on`='" . date('Y-m-d H:i:s') . "';";
        $this->db_connection->query($insert_on_duplicate_update_query);
    }
    
}
