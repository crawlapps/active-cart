<?php

class general_function {

    /**
     * @var object $db_connection The database connection
     */
    public $db_connection = null;

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created
     */
    public function __construct() {
        /*
         * Start database connection
         */
        if ($this->db_connection == null) {
            $db_connection = new DB_Class();
            $this->db_connection = $GLOBALS['conn'];
        }
    }

    /* set success message */

    public function set_message($message) {
        $this->messages[] = $message;
    }

    /* set error message */

    public function set_error($message) {
        $this->errors[] = $message;
    }

    /* register webhook to shopify or curl anyi where */

    public function register_webhook($url, $params, $token) {
        $start_Curl = curl_init();
        curl_setopt($start_Curl, CURLOPT_URL, $url);
        //curl_setopt($start_Curl, CURLOPT_HTTPPOST, 1); 
        // Tell curl that this is the body of the POST
        curl_setopt($start_Curl, CURLOPT_POSTFIELDS, $params);
        curl_setopt($start_Curl, CURLOPT_HEADER, false);
        curl_setopt($start_Curl, CURLOPT_HTTPHEADER, array("Accept: application/json", "Content-Type: application/json", "X-Shopify-Access-Token: $token"));
        curl_setopt($start_Curl, CURLOPT_RETURNTRANSFER, true);
        if (preg_match("^(https)^", $url))
            curl_setopt($start_Curl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($start_Curl);
        curl_close($start_Curl);
        return $response;
    }

    /* insert into on duplicate update table using table name and column value array */

    function insert_on_duplicate_update($table, $fields) {
        $insert_columns = $values = array();
        $update_columns = '';
        foreach ($fields as $key => $value) {
            $insert_columns[] = $key;
            $values[] = $value;
            $update_columns .= $key . "='$value',";
        }
        $insert_query = "INSERT INTO $table" . ' (' . implode(',', $insert_columns) . ") VALUES('" . implode("','", $values) . "')";
        $update_query = "UPDATE " . rtrim($update_columns, ",");

        $insert_on_duplicate_update_query = $insert_query . " ON DUPLICATE KEY " . $update_query . ";";

        $this->db_connection->query($insert_on_duplicate_update_query);

        return $this->db_connection->insert_id;
    }

    /* insert into table using table name and column value array */

    function insert($table, $fields) {
        $columns = $values = array();
        foreach ($fields as $key => $value) {
            $columns[] = $key;
            $values[] = $value;
        }
        $insert_query = "INSERT INTO $table" . ' (' . implode(',', $columns) . ") VALUES('" . implode("','", $values) . "')";
        $this->db_connection->query($insert_query);
        return $this->db_connection->insert_id;
    }

    /* update into table using table name and column value array */

    function update($table, $fields, $where) {
        $update_query = "UPDATE $table SET ";
        $columns = '';
        foreach ($fields as $key => $value) {
            $columns .= $key . "='$value',";
        }
        $update_query = $update_query . rtrim($columns, ",") . " WHERE $where";
        return $this->db_connection->query($update_query);
    }

    /* delete into table using table name and where */

    function delete($table, $where) {
        $delete_query = "delete from $table WHERE $where";
        return $this->db_connection->query($delete_query);
    }

    /* return select all result */

    function select($table, $where = '') {
        $response = $this->db_connection->query("SELECT * FROM `$table` $where");
        return $response;
    }

    function query($query) {
        return $this->db_connection->query($query);
    }

    public function verify_webhook($data, $hmac_header) {
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, SHOPIFY_SECRET, true));
        return ($hmac_header == $calculated_hmac);
    }

    /* Money formate replace */

    public function money_formate_replace($money_format, $amount) {
        $beginning = "{{";
        $end = '}}';
        $beginningPos = strpos($money_format, $beginning);
        $endPos = strpos($money_format, $end);
        if ($beginningPos === false || $endPos === false) {
            return $money_format;
        }
        $textToDelete = substr($money_format, $beginningPos, ($endPos + strlen($end)) - $beginningPos);
        return str_replace($textToDelete, $amount, $money_format);
    }

    public function __number_format($price) {
        $decimals = 2;
        $price = $price * pow(10, $decimals);
        $price = intval($price);
        $price = $price / pow(10, $decimals);

        return $price;
    }

    function send_email($fromname, $from, $user_email, $subject, $message) {
        if (EMAIL_SEND == "email") {
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers .= 'From: ' . $fromname . '<' . $from . '>' . "\r\n";
            mail($user_email, $subject, $message, $headers);
        } else {
            require_once ABS_PATH . '/libraries/vendor/autoload.php';
            $from = new SendGrid\Email($fromname, $from);
            $to = new SendGrid\Email("", $user_email);
            $content = new SendGrid\Content("text/html", $message);
            $mail = new SendGrid\Mail($from, $subject, $to, $content);
            $sg = new \SendGrid(SENDGRID_API_KEY);
            $response = $sg->client->mail()->send()->post($mail);
        }
        return true;
    }

}

/* generate random string defult combination is time stamp and random string */

function generateRandomString($length = 15) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>