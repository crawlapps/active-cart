<?php

include_once ('functions.php');
include_once "../shopifyapps/shopify_call.php";
class Front_functions extends general_function {

    /**
     * @var object $db_connection The database connection
     */
    public $db_connection = null;

    /**
     * @var array collection of error messages
     */
    public $errors = array();

    /**
     * @var array collection of success / neutral messages
     */
    public $messages = array();
    
    /* set after decimals point digit value value */
    public $decimals = 2;
    
    /* set user cart value */
    public $user_cart = array();
    
    /* set cart items array */
    public $cart_items_arr = array();
    
    /* set cart product id array */
    public $cart_prod_id_arr = array();
    
    /* set cart pord id with qty arr*/
    public $cart_pord_id_with_qty_arr = array();

    /* set cart tracking product id array */
    public $cart_tracking_prod_id_arr = array();
    
    /* set cart tracking product removed (by user ) id array */
    public $cart_tracking_remove_prod_id_arr = array();
    
    /* set cart tracking auto add product id array */
    public $cart_tracking_auto_add_prod_id_arr = array();
    
    /* set cart token value */
    public $cart_token = '';
    
    /* set full cart total value */
    public $full_cart_total = '';
    
    /* set type1 offer product id array */
    public $type1_offer_prod_id_arr = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     */
    public function __construct($is_obj=TRUE) {
        /* call parent's (general_function) constructor (Start database connection)*/
        parent::__construct();
        /* set customer cart */
        $this->set_user_cart($is_obj);
    }

    public function set_all_variable($store_client_id) {
        $this->set_user_cart_item_arr();
        $this->set_cart_pord_id_arr();
        $this->set_full_cart_total();
        $this->set_cart_token();
        $this->set_cart_pord_id_with_qty_arr();
        $this->set_cart_tracking_prod_id_arr($store_client_id);
    }

    /* set cart */

    public function set_user_cart($is_obj) {
        $post = json_decode(file_get_contents("php://input"), $is_obj);
        $this->user_cart = $post;
    }

    /* get cart */

    public function get_user_cart() {
        return $this->user_cart;
    }

    /* set token */

    public function set_cart_token() {
        $post = $this->user_cart;
        $this->cart_token = $post['cart']['token'];
    }

    /* set cart item */

    public function set_user_cart_item_arr() {
        $post = $this->user_cart;
        $this->cart_items_arr = $post['cart']['items'];
    }

    /* get cart item */

    public function get_user_cart_item_arr() {
        return $this->cart_items_arr;
    }

    /* set cart item product id arr */

    public function set_cart_pord_id_arr() {
        $this->cart_prod_id_arr = array_column($this->cart_items_arr, 'product_id');
    }
    
    /* set cart product id with qty arr */

    public function set_cart_pord_id_with_qty_arr() {
        $this->cart_pord_id_with_qty_arr = array_column($this->cart_items_arr, 'quantity','product_id');
    }

    /* get cart item product id arr */

    public function get_cart_pord_id_arr() {
        return $this->cart_prod_id_arr;
    }

    /* set full cart total */

    public function set_full_cart_total() {
        $post = $this->user_cart;
        $this->full_cart_total = $post['cart']['total_price'];
    }

    /* fetch all tracking product on which we gave discount */

    function set_cart_tracking_prod_id_arr($store_client_id) {
        $cart_token=$this->cart_token;
        $where = "WHERE cart_token='$cart_token' AND store_client_id='$store_client_id'; ";
        $resource_obj = $this->select(TABLE_CART_TRACKING, $where);
        $product_id_arr = array();
        $remove_product_id_arr = $auto_add_product_id_arr = array();

        if ($resource_obj->num_rows > 0) {
            while ($cart_tracking = $resource_obj->fetch_object()) {
                
                if($cart_tracking->is_auto_add=='1'){
                    $auto_add_product_id_arr[$cart_tracking->variants_id] = $cart_tracking->product_id;
                }
                
                if($cart_tracking->is_cust_remove=='0'){
                    $product_id_arr[$cart_tracking->variants_id] = $cart_tracking->product_id;
                }else{
                    $remove_product_id_arr[$cart_tracking->variants_id] = $cart_tracking->product_id;
                }
            }
        }
        $this->cart_tracking_prod_id_arr = $product_id_arr;
        $this->cart_tracking_remove_prod_id_arr = $remove_product_id_arr;
        $this->cart_tracking_auto_add_prod_id_arr = $auto_add_product_id_arr;
    }

    /**
     * Get shop informaion by Shop name
     */
    public function get_shop($shop) {
        $where = 'WHERE store_name = "' . $shop . '" AND status=1';
        $response = $this->select(TABLE_CLIENT_STORES, $where);
        return $response;
    }

    /* helps in auto add product in cart */

    function get_cart_tracking_prod_id_arr() {
        return $this->cart_tracking_prod_id_arr;
    }

    function delete_cart_tracking_product($store_client_id, $cart_token, $product_id='') {
        $where = "store_client_id = '$store_client_id' AND cart_token='$cart_token'";//AND is_cust_remove = '0'
        if($product_id != ''){
           $where.=" AND product_id='$product_id';";
        }
        $this->delete(TABLE_CART_TRACKING, $where);
    }

    function set_type1_offer_prod_id_arr($store_client_id,$main_prod=array()) {
        if(empty($main_prod)){
            $product_ids = implode(',', $this->cart_prod_id_arr);
        }else{
            $product_ids = implode(',', $main_prod);
        }
        $get_offer_id_query = $this->query("SELECT o.id, MAX(o.at) AS at, gp.product_price, gp.product_id, gp.variant_id FROM `" . TABLE_OFFERS . "` AS o ,`" . TABLE_GETY_PRODUCTS . "` AS gp
                WHERE o.id=gp.offer_id AND o.store_client_id = '$store_client_id' AND o.status = '1' AND o.type = '1' AND o.buyx IN ($product_ids)
                GROUP BY gp.product_id;");
        if(empty($get_offer_id_query)){
            $this->query("set global sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';");
        }
        $this->query("set global sql_mode = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION';");
        $offer_id_arr = array();

        if (isset($get_offer_id_query) && $get_offer_id_query->num_rows > 0) {
            while ($offer = $get_offer_id_query->fetch_object()) {
                if(empty($this->cart_tracking_remove_prod_id_arr) || (!in_array($offer->product_id,$this->cart_tracking_remove_prod_id_arr) || in_array($offer->product_id,$this->cart_prod_id_arr))){
                    $offer_id_arr[$offer->product_id] = array(
                        'discount_per' => $offer->at,
                        'variant_id' => $offer->variant_id,
                        'price' => $offer->product_price,
                        'discount_type' => 1
                    );
                }
            }
        }
        $this->type1_offer_prod_id_arr = $offer_id_arr;
    }

    function get_type1_offer_prod_id_arr() {
        return $this->type1_offer_prod_id_arr;
    }

    function get_type1_add_prod_id_arr() {
        $type1_offer_prod_id_arr = $this->type1_offer_prod_id_arr;
        /* Here we romove product id from adding list if it is exist in cart or */
        foreach ($this->cart_prod_id_arr as $product_id) {
            if (isset($type1_offer_prod_id_arr[$product_id]) || array_search($product_id,$this->cart_tracking_prod_id_arr)!==FALSE) {
                /* we unset offer product which exsiting in cart */
                unset($type1_offer_prod_id_arr[$product_id]);
            }
        }
        return $type1_offer_prod_id_arr;
    }

    /* function helps to add/update cart product in to tracking product table
     *  it add product but if it already exist than it updated the product */
    function insert_into_cart_tracking_table($store_client_id, $product_items = array()) {
        $cart_token = $this->cart_token;

        if (!empty($product_items) && $cart_token != '') {
            foreach ($product_items as $product_id => $product_arr) {

                $fields = array(
                    'store_client_id' => $store_client_id,
                    'cart_token' => $cart_token,
                    'variants_id' => $product_arr['variant_id'],
                    'product_id' => $product_id,
                    'is_cust_remove' => 0,
                    'discount_type' => $product_arr['discount_type'],
                    'discount_per' => $product_arr['discount_per'],
                    'created' => date('Y-m-d H:i:s'),
                    'updated' => date('Y-m-d H:i:s')
                );
                
                if(in_array($product_id, $this->cart_tracking_remove_prod_id_arr)){
                    $fields['is_cust_remove'] = 1;
                }

                if (!isset($product_arr['is_auto_add']) && in_array($product_id, $this->cart_prod_id_arr) && !in_array($product_id, $this->cart_tracking_prod_id_arr)) {
                    $fields['is_auto_add'] = 0;
                }

                $this->insert_on_duplicate_update(TABLE_CART_TRACKING, $fields);
            }
        }
    }

    function get_discounted_prod_arr($store_client_id, $is_cust_remove="AND is_cust_remove = '0'") {
        $cart_token = $this->cart_token;
        $where = "WHERE store_client_id='$store_client_id' AND cart_token='$cart_token' $is_cust_remove ;";
        return $this->select(TABLE_CART_TRACKING, $where);
    }

    function get_cart_tracking_remove_prod($store_client_id) {
        $cart_token = $this->cart_token;
        $where = 'WHERE store_client_id="' . $store_client_id . '" AND cart_token="' . $cart_token . '" AND is_cust_remove="1"';
        return $this->select(TABLE_CART_TRACKING, $where);
    }
        
    /* function used to remove offer product when its main product removed also in this function we call another function
     * called "prepare_remove_product" which helps to making array which we remove from cart 
     * */
    function remove_offer_prod_cart_tracking($store_client_id,$remove_cart_tracking_prod_arr=array()) {
        if(!empty($remove_cart_tracking_prod_arr)){
            $token = $this->cart_token;
            $where="store_client_id = '$store_client_id' AND cart_token='$token' AND discount_type = '1' AND product_id IN (".  implode(',', $remove_cart_tracking_prod_arr).");";
            $this->delete(TABLE_CART_TRACKING, $where);
                     
            $remove_cart_tracking_prod_arr=$this->prepare_remove_product($remove_cart_tracking_prod_arr);
                            
        }
        return $remove_cart_tracking_prod_arr;
    }
    
    /* helps to making array which we need to remove from cart */
    public function prepare_remove_product($remove_cart_tracking_prod_arr=array()){
        $need_update_prod_arr=array();
        if(!empty($remove_cart_tracking_prod_arr)){
            $cart_prod_id_arr=$this->cart_items_arr;
            foreach($cart_prod_id_arr as $item=>$product_arr){
                if(in_array($product_arr['product_id'],$remove_cart_tracking_prod_arr) && in_array($product_arr['product_id'],$this->cart_tracking_auto_add_prod_id_arr) ){
                    $need_update_prod_arr[$product_arr['product_id']] = array(
                        'variant_id' => $product_arr['variant_id'],
                        'line_price' => $product_arr['line_price'],
                        'quantity' => 0,
                    );
                }
            }
        }
        return $need_update_prod_arr;
    }
    
    /* function used to remove offer product we also (reset)set cart tracking product id array because 
     * when customer remove offer product than we have not need to add againg this product 
     */
    public function is_need_remove_offer_prod($store_client_id,$offer_prod_id_array,$discount_type='1'){
        $need_to_is_cust_remove=array_diff($this->cart_tracking_prod_id_arr, $offer_prod_id_array);
        if (!empty($need_to_is_cust_remove)) {
            $fields = array('is_cust_remove' => '1');
            $where = "store_client_id = '$store_client_id' AND cart_token = '$this->cart_token' AND discount_type='$discount_type' AND product_id IN (".  implode(',', $need_to_is_cust_remove).")";
            $this->update(TABLE_CART_TRACKING, $fields, $where);
        }
        $this->set_cart_tracking_prod_id_arr($store_client_id);
        return $need_to_is_cust_remove;
    }
    
    /* function helps to display the discount on cart page */
    function get_discount_display_response() {
        $money_format = '';
        $main_total = 0;
        $auto_add_items_detail_arr = array();
        $auto_add_items_arr = $original_item_price_arr = $original_line_item_price_arr = $discounted_item_price_arr = $discounted_line_item_price_arr = array();
        $post = $this->get_user_cart();
        if (isset($post['shop']) && $post['shop'] != "") {
            $shop = $post['shop'];
            $shopinfo = $this->get_shop($shop);
            if (isset($shopinfo) && $shopinfo->num_rows > 0) {
                $shopinfo = $shopinfo->fetch_object();
                $store_client_id = $shopinfo->store_client_id;
                $money_format = $shopinfo->money_format;

                /* set all required (entity)value */
                $this->set_all_variable($store_client_id);
                
                $this->set_type1_offer_prod_id_arr($store_client_id);
                
                $cart_tracking_prod_id_arr = $this->get_discounted_prod_arr($store_client_id,'');
                
                $main_total = $this->full_cart_total;
                if ($cart_tracking_prod_id_arr->num_rows > 0) {
                    while ($track_prod = $cart_tracking_prod_id_arr->fetch_object()) {
                        $product_id = $track_prod->product_id;
                        $variant_id = $track_prod->variants_id;
                        $item_no = array_search($track_prod->product_id, $this->cart_prod_id_arr);
                        
                        $discount_type=$track_prod->discount_type;
                        $discount_per=$track_prod->discount_per;
                                                
                        if ($item_no !== FALSE && array_key_exists($track_prod->product_id, $this->type1_offer_prod_id_arr)) {
                            $cart_items_arr = $this->cart_items_arr[$item_no];

                            $auto_add_items_arr[] = $variant_id;
                            $auto_add_items_detail_arr[$variant_id] = array('discount_per'=>$discount_per);
                            
                            $original_item_price_arr[$variant_id] = ($cart_items_arr['price']);
                            $original_line_item_price_arr[$variant_id] = ($cart_items_arr['line_price']);
                            
                            $itme_price_discount = intval(($cart_items_arr['price'] * $discount_per) / 100);

                            $quantity=$cart_items_arr['quantity'];
                            if($quantity > 1 && $shop == "active-cart.myshopify.com"){
                                $line_itme_price_discount = intval(($cart_items_arr['line_price'] * $discount_per) / 100);
                                $discounted_item_price_arr[$variant_id] = $cart_items_arr['price'];
                                $discounted_line_item_price_arr[$variant_id] = ($cart_items_arr['line_price']) - $cart_items_arr['price'];
                                $main_total = $main_total - $cart_items_arr['price'];
                            }else{
                                $line_itme_price_discount = intval(($cart_items_arr['line_price'] * $discount_per) / 100);
                                $discounted_item_price_arr[$variant_id] = ($cart_items_arr['price'] - $itme_price_discount);
                                $discounted_line_item_price_arr[$variant_id] = $cart_items_arr['line_price'] - $line_itme_price_discount;
                                $main_total = $main_total - $line_itme_price_discount;
                            }
                            
                        }
                    }
                }
            }
           
            /* called for making block of discount display */
            $spend_offer = $this->get_spend_discount_html($main_total, $shopinfo);
            $spend_offer_html = $spend_offer['html'];
            $is_spend_offer = $spend_offer['is_spend_offer'];
            /* called for making block of discount display */
            return array(
                /* required for displaying discount */
                'auto_add_items' => $auto_add_items_arr,
                'auto_add_items_detail_arr' => $auto_add_items_detail_arr, 
                'original_item_price' => $original_item_price_arr,
                'discounted_item_price' => $discounted_item_price_arr,
                'original_line_item_price' => $original_line_item_price_arr,
                'discounted_line_item_price' => $discounted_line_item_price_arr,
                'main_total' => $main_total,
                'spend_offer_html' => $spend_offer_html,
                'is_spend_offer' => $is_spend_offer,
                'money_format' => htmlspecialchars_decode($money_format));
            
        }
    }
    
    /*******************************************************************************
    * DISCOUNT TYPE 2 (DISCOUNT ON SPEND) HTML BLOCK CODE
    *******************************************************************************/
    /**
     * Get api list
     */
    private function get_api_list($shop_token, $store_name, $api_name, $url_param_arr = array(), $subapi_name = '') {
        
        $api_list = shopify_call($shop_token, $store_name, SHOPIFY_API_VERSION."{$api_name}.json", $url_param_arr, 'GET');
        $api_list = json_decode($api_list['response'],TRUE);
        return $api_list;
    }
    
    private function get_t2_offer_prod_arr($store_client_id, $approx_cust_spend){
        $approx_cust_spend = $this->__number_format($approx_cust_spend / 100);
        $type2 = $this->query("SELECT o.`id`,o.`buyx`, gp.`product_id`, gp.`variant_id`  FROM `offers` AS o 
                               INNER JOIN  `gety_products` AS gp ON o.`id`=gp.`offer_id`
                               WHERE o.`status`='1' AND o.`store_client_id`='$store_client_id' AND o.`type`='2' AND o.`buyx` <= $approx_cust_spend ORDER BY buyx DESC , id DESC");
        /* contain all offer id */
        $type2_offer_id_arr = array();
        if ($type2->num_rows > 0) {
            while ($offer = $type2->fetch_assoc()) {
                $type2_offer_id_arr[]=$offer;
            }
        }
        return $type2_offer_id_arr;
    }
        
    private function get_spend_discount_html($main_total, $shopinfo) {
        $shop=$shopinfo->store_name;
        $store_client_id=$shopinfo->store_client_id;
        $shop_token=$shopinfo->token;
        $money_format = $shopinfo->money_format;
        $is_spend_offer = 0;
        $t2_prod_arr = $this->get_t2_offer_prod_arr($store_client_id, $main_total);
        $product_buyx_arr = array_column($t2_prod_arr, 'buyx');
        $product_buyx_max =  max($product_buyx_arr);

        $html='<div id="spendDiscountBlock" class="i-tix-auto-add"></div>'; 
        if (!empty($t2_prod_arr)) {
            $product_id_arr = array_column($t2_prod_arr, 'product_id');
            
            $product_response = array();
            if (!empty($product_id_arr)) {
                $url_param_arr = array('ids' => implode(',', $product_id_arr));
                $product_response = $this->get_api_list($shop_token, $shop, 'products', $url_param_arr);
                
                $api_product_response = $product_response['products'];
                
                $api_product_id_arr = array_column($api_product_response, 'id');
                
                if (!empty($product_response)) {
                    $html='';
                    $is_spend_offer++;
                    $radio_offer_id_arr=array();
                    
                    foreach ($t2_prod_arr as $key=>$offer_product) {
                        $api_prod_key = array_search($offer_product['product_id'], $api_product_id_arr);
                        
                        $api_product = $api_product_response[$api_prod_key];
                    
                        if(empty($radio_offer_id_arr) || !in_array($offer_product['id'], $radio_offer_id_arr)){
                            $radio_offer_id_arr[]=$offer_product['id'];
                            if($html!=''){
                                $html.='</div> <!-- end of single-spend-offer --> </div> <!-- list-group-items -->';
                            }
                            $is_checked='';
                            if($product_buyx_max == $offer_product['buyx']){
                                $is_checked='checked="checked"';
                            }
                            $add_class = ($product_buyx_max == $offer_product['buyx'])?'high_show_spend_value_offer':'hide_low_spend_value_offer';
                            $html.=
                            '<div class="list-group-items '.$add_class.'">
                                <div class="radio">
                                    <input type="radio" name="spend_discount" value="' . $offer_product['id'] . '" ' . $is_checked . '>
                                </div>
                                <div class="single-spend-offer">';
                        }
                        
                        $html.=
                        '<div class="offer-product-info">
                            <img src="' . $api_product['images']['0']['src'] . '" alt="' . $api_product['images']['0']['alt'] . ' width="150" height="150">
                            
                            <div class="spend-offer-price">
                                <div class="i-tix-product-title">
                                    <h3><a href="https://' . $shop . '/products/' . $api_product['handle'] . '" target="_blank">' . $api_product['title'] . '</a></h3> 
                                </div>
                                <div class="i-tix-price">
                                    <span class="line-through"> ' . $this->money_formate_replace(htmlspecialchars_decode($money_format),$api_product['variants']['0']['price']) . ' </span>
                                    <span style="display:block;"> ' . $this->money_formate_replace(htmlspecialchars_decode($money_format),'0.00') . ' </span>
                                </div>
                            </div>
                        </div>';
                    }
                    /* Get settings of store */
                    $get_css_arr = $this->get_settings($store_client_id);
                    
                    $add_hide_show_css = '';
                    if($get_css_arr->is_show_all_offer == 1){
                        $add_hide_show_css = "display:none !important;";
                    }
                    $html='
                        <div id="spendDiscountBlock" class="i-tix-auto-add">
                            <div class="offer-block list-group">' . $html . '</div> <!-- end of single-spend-offer--> </div> <!-- list-group-items -->
                                <div class="list-group-items none-of-above-class">
                                    <div class="radio">
                                        <input type="radio" name="spend_discount" value="0">
                                     </div>
                                    <div class="single-spend-offer">
                                        <div class="offer-product-info none-of-above-text">
                                            <h3> ' . $get_css_arr->none_of_above_text . ' </h3>
                                        </div>
                                    </div>
                                </div> <!-- end of list-group-items -->
                            </div> <!-- end of offer-block list-group -->
                        </div>
                        <style>
                            .i-tix-auto-add{
                                display:block;
                                padding: 10px;
                                float:left;
                                width:100%;
                                border: 1px solid #e8e9eb;
                                ' . $get_css_arr->font_family . '
                            }

                            .i-tix-auto-add .list-group-items{
                                display:flex; 
                                align-items:center;
                                border: 1px solid #e8e9eb;
                                box-shadow: 2px 3px 1px 0px #f1f1f1;
                                margin-bottom:10px;
                            }

                            .i-tix-auto-add .list-group-items:last-child{
                                margin:0;
                            }

                            /*.i-tix-auto-add .list-group-items input[type=\'radio\']{
                                width:5%;
                                position:relative;
                                top:70px;
                                left:10px;


                            }*/

                            .i-tix-auto-add .single-spend-offer{
                                display:block;
                                min-width: 95%;
                                margin-bottom:10px;
                                padding: 10px;
                                clear:left;
                            }
                            
                            .i-tix-auto-add .single-spend-offer:last-child, .i-tix-auto-add .single-spend-offer:first-child {
                                margin:0;
                                padding:0;
                            }

                            .i-tix-auto-add .offer-product-info{
                                padding:10px;
                                width: 100%;
                                display: flex;
                                align-items: center;
                            }
                            
                            .i-tix-auto-add .radio{
                                margin : 10px;
                            }
                            
                            .i-tix-auto-add .offer-product-info img{
                                margin-left : 10px;
                                float:left;
                                ' . $get_css_arr->image_css . '
                            }
                            
                            .i-tix-auto-add .offer-product-info h3{
                                float:left;
                                padding: 0 15px;
                                overflow: hidden;
                                font-weight: 100;
                                ' . $get_css_arr->product_title_css . '
                            }
                            
                            .i-tix-auto-add .offer-product-info.none-of-above-text h3{
                                margin-left: 50px;
                                margin-top: 20px;
                                ' . $get_css_arr->none_of_above_css . '
                            }
                            
                            .i-tix-auto-add .spend-offer-price{
                                display:table-cell;
                                position: relative;
                                width: 100%;
                                ' . $get_css_arr->discount_pirce_css . '
                            }
                            
                            .i-tix-auto-add .offer-product-info span.line-through{
                                text-decoration: line-through;
                                margin-right:5px;
                                ' . $get_css_arr->original_pirce_css . '
                            }
                            .i-tix-product-title{
                                float: left; 
                            }
                            .i-tix-auto-add .i-tix-price{
                                float: right; 
                            }
                            #spendDiscountBlock .list-group-items.hide_low_spend_value_offer{
                                '.$add_hide_show_css.'
                            }
                            @media only screen and (max-width: 641px) {
                                .i-tix-auto-add .i-tix-price{
                                    float: none; 
                                    padding: 0px 15px;
                                }
                            }
                          </style>
                        '; 
                }
            }
        }
        
        return array('html'=>$html,'is_spend_offer'=>$is_spend_offer);
    }

    function get_offer_product_dbr($store_client_id, $offer_ids, $dis_type = 2) {
        $where = "WHERE store_client_id = $store_client_id AND type = $dis_type AND offer_id IN ($offer_ids)";
        return $this->select(TABLE_GETY_PRODUCTS, $where);
    
    }
    /*******************************************************************************
    * END OF DISCOUNT TYPE 2 (DISCOUNT ON SPEND) HTML BLOCK CODE
    *******************************************************************************/
    
    /* get setting data */
    function get_settings($store_client_id) {

        $where = " WHERE  `store_client_id` = $store_client_id ;";
        $settings_db_resource = $this->select(TABLE_SETTINGS, $where);
        $setting = new stdClass();
        if ($settings_db_resource->num_rows > 0) {
            $setting = $settings_db_resource->fetch_object();

            if ($setting->font_family != '') {
                $setting->font_family = 'font-family : ' . $setting->font_family;
            } else {
                $setting->font_family = 'font-family : inherit';
            }

            /* product title css settings */
            $css_rule = unserialize($setting->product_title_css);

            $setting->product_title_css = 'color : ' . $css_rule['color'] . ';font-size : ' . $css_rule['font-size'] . 'px;';


            /* Product original price css settings */
            $css_rule = unserialize($setting->original_pirce_css);

            $setting->original_pirce_css = 'color : ' . $css_rule['color'] . ';font-size : ' . $css_rule['font-size'] . 'px;';

            /* Product discount price css settings */
            $css_rule = unserialize($setting->discount_pirce_css);

            $setting->discount_pirce_css = 'color : ' . $css_rule['color'] . ';font-size : ' . $css_rule['font-size'] . 'px;';

            /* Product Image css settings */
            $css_rule = unserialize($setting->image_css);

            $setting->image_css = 'width : ' . $css_rule['width'] . 'px;height : ' . $css_rule['height'] . 'px;';

            $css_rule = unserialize($setting->none_of_above_css);

            $setting->none_of_above_css = 'color : ' . $css_rule['color'] . ';font-size : ' . $css_rule['font-size'] . 'px;';
        }
        return $setting;
    }
    
    public function get_shopify_discount($amount, $rate) {
        //$discount = number_format($amount * ( $rate / 100),2,'.','');
        $discount = $amount * ( $rate / 100);
        $discount = $discount * pow(10, 2);
        $discount = floatval($discount);
        $discount = $discount / pow(10, 2);
        $final_discount = round($discount, 2);
        return $final_discount;
    }

}
