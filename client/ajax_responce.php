<?php
header("Access-Control-Allow-Origin: *");
/* include main config file file */
include_once ('../include/config.php');

/* include main client function file */
include_once ('functions.php');
$functions = new Client_functions();

/*get product*/
if(isset($_POST['ulproductlist'])) {
    $product_records = $functions->get_products_records($_POST);
    echo $product_records;

}

/* Add/Edit Offer */
if(isset($_POST['offer'])) {
    $offer_record = $functions->addeditOffer($_POST,$_POST['shop']);
    echo $offer_record;

}

/* get offer list with pagination */
if(isset($_POST['get_offer'])) {
    $offer_record = $functions->get_offer_load($_POST['shop'],$_POST['current_page']);
    echo $offer_record;
}

/* Edit time get selected product*/
if(isset($_POST['get_selected_product'])) {
    $offer_record = $functions->get_selected_product($_POST['shop'],$_POST['ids']);
    echo $offer_record;
}

/* offer status change */
if(isset($_POST['type']) && $_POST['type'] == 'status') {
    $log_records = $functions->offer_status_change($_POST);
    echo $log_records;
}

/* delete recorded of any tables */
if(isset($_POST['delete']) && $_POST['delete'] != '') {
    echo $functions->offer_delete($_POST);
}

/* delete recorded of any tables */
if(isset($_POST['tour_status']) && $_POST['tour_status'] != '') {
    echo $functions->tour_status($_POST);
}

/* save settings */
if (isset($_POST['addClientSetting'])) {
    $delete_customer_product = $functions->set_settings($_POST);
    echo $delete_customer_product;
}