<div class="Polaris-Banner Polaris-Banner--statusSuccess footer-review-bar" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner10Content" aria-labelledby="Banner10Heading" style="display: none;">
    <div class="Polaris-Banner__Heading">
        <p class="Polaris-Heading"><span>&#10084;</span> We Value Your Feedback!</p>
        <p>It helps us make Active Cart better and keeps us energized.  <a href="mailto:<?php echo SITE_ADMIN_EMAIL; ?>" target="_top">Let us know</a> how we’re doing.
            <br>
        </p>
    </div>
    <div class="Polaris-Banner__Content">
        <div class="Polaris-Banner__Actions">
            <div class="Polaris-ButtonGroup">
                <div class="Polaris-ButtonGroup__Item">
                    <a href="https://apps.shopify.com/activecart?reveal_new_review=true" target="_blank" class="Polaris-Button Polaris-Button--outline"><span class="Polaris-Button__Content"><span> Rate our app</span></span></a></div>
            </div>
        </div>
    </div>
</div>
<div class="Polaris-Layout__Section">
    <div class="Polaris-FooterHelp">
        <div class="Polaris-FooterHelp__Content">
            <div class="Polaris-FooterHelp__Text">
                <?php echo SITE_COPYRIGHT; ?>
            </div>
        </div>
    </div>
</div>
</div>
<div class="inline-flash-wrapper animated bounceInUp">
    <div class="inline-flash">
        <p class="inline-flash__message"></p>
    </div>
</div>
<style>
    .social {
        margin-top: 20px;
    }
    .social-text {
        display: inline-block;
        vertical-align: middle;
    }
    .social-botton {
        display: inline-block;
        padding-left: 10px;
    }
    .fb_iframe_widget, #twitter-widget-0{
        vertical-align: middle;
    }
    .footer-review-bar .Polaris-Banner__Heading{
        width: 100%;
    }
    .Polaris-Page__Footer--hasSeparator{
        padding: 1.6rem 0;
        border-top: 1px solid #dfe3e8;
    }
    .Polaris-Banner--statusSuccess.footer-review-bar{
        background: #FDFDFD;
        box-shadow: inset 0 3px 0 0 #62B1ED, inset 0 0 0 0 transparent, 0 0 0 1px rgba(63, 63, 68, 0.05), 0 1px 3px 0 rgba(63, 63, 68, 0.15);
    }
    .Polaris-Banner--statusSuccess.footer-review-bar:focus {
        background: #FDFDFD;
        box-shadow: inset 0 3px 0 0 #62B1ED, inset 0 0 0 0 transparent, 0 0 0 1px rgba(63, 63, 68, 0.05), 0 1px 3px 0 rgba(63, 63, 68, 0.15);
    }
    .footer-review-bar .Polaris-Button {
        background : #6C7BCF;
        color: #fff;
        width: 121px;
    }
    .Polaris-Heading span,.copyrightheart{
        color: red;
        font-size: 17px;
    }
    .footer-review-bar .Polaris-Button:active,
    .footer-review-bar .Polaris-Button:focus,
    .footer-review-bar .Polaris-Button:hover {
        color: #fff;
        opacity: 0.8;
    }
</style>
<script type="text/javascript" src="../assets/js/custom.js?v=1.11"></script>
</body>
</html>
