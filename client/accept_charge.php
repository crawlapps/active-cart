<?php

include_once ('../include/config.php');

/* include main client function file */
include_once ('functions.php');

/* include shopifyapps config file */
include_once '../shopifyapps/config.php';

/* include language translation file */
include_once '../include/en.php';

/* client function class variable */
$functions = new Client_functions();
if (isset($_REQUEST['charge_id']) && $_REQUEST['charge_id'] != "") {
    $charge_activate = $functions->charge_activate($_REQUEST['charge_id'], $_REQUEST['shop']);
    if ($charge_activate['flg'] == 1) {
        header('Location: https://' . $charge_activate['store_name'] . '/admin/apps/' . SHOPIFY_API_KEY.'?shop='.$_REQUEST['shop']);
    }else {
        header('Location: '.SITE_URL.'decline.php?shop='.$_REQUEST['shop']);
    }
    exit;
}