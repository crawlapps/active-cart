<?php

/* include general function */
include_once ('../include/functions.php');
include "../shopifyapps/shopify_call.php";

class Client_functions extends general_function {

    /**
     * @var object $db_connection The database connection
     */
    public $db_connection = null;

    /**
     * @var array collection of error messages
     */
    public $errors = array();

    /**
     * @var array collection of success / neutral messages
     */
    public $messages = array();

    /**
     * the function "__construct()" automatically starts whenever an object of this class is created,
     * you know, when you do "$login = new Login();"
     */
    public function __construct() {
        /*
         * Start database connection
         */
        if ($this->db_connection == null) {
            $db_connection = new DB_Class();
            $this->db_connection = $GLOBALS['conn'];
        }
    }

    /* Do not touch this code */

    public function getUserData($shop) {
        $user_store = $this->query("SELECT cs.*,c.email FROM " . TABLE_CLIENT_STORES . " as cs, " . TABLE_CLIENTS . " as c  WHERE cs.client_id = c.client_id AND cs.status=1 AND cs.store_name = '$shop'");
        $user_store = $user_store->fetch_object();
        $current_user = $user_store;
        $current_user->current_store = isset($user_store->store_name) ? $user_store->store_name : 'No Store';
        $current_user->store_id = isset($user_store->store_client_id) ? $user_store->store_client_id : 0;
        $current_user->charge_approve = $user_store->charge_approve;
        $current_user->downgrade_plan = $user_store->downgrade_plan;
        $current_user->email = $user_store->email;
        $this->store_id = $user_store->store_client_id;
        return $current_user;
    }
    
    /* get total offer type 1 of */
    public function get_user_offer1_cnt($store_client_id, $is_only_active = 0){
        if($is_only_active){
            $user_has_type1_offer_query=$this->query("SELECT COUNT(id)  FROM `".TABLE_OFFERS."` WHERE `store_client_id` = $store_client_id AND `status` = 1 AND `type` = 1");
        }else{
            $user_has_type1_offer_query=$this->query("SELECT COUNT(id)  FROM `".TABLE_OFFERS."` WHERE `store_client_id` = $store_client_id AND `type` = 1");
        }
        $count=$user_has_type1_offer_query->fetch_row();
        return $count[0];
    }
    /**
     * Get shopify api count
     */
    public function get_api_count($shop, $api_name, $title = '') {

        $shopinfo = $this->getUserData($shop);
        if ($title != '') {
            $apicount = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."{$api_name}/count.json", array('title' => $title), 'GET');
        } else {
            $apicount = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."{$api_name}/count.json", array(), 'GET');
        }
        $apicount = json_decode($apicount['response']);
        return $apicount;
    }

    /**
     * Get api list
     */
    public function get_api_list($shop, $api_name, $pageno = '1', $title = '', $id = '') {
        $shopinfo = $this->getUserData($shop);
        if ($id != "") {
            $api_list = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."{$api_name}.json", array('ids' => $id), 'GET');
        } else {
            if ($title != '') {
                $api_list = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."{$api_name}.json", array('title' => $title, 'limit' => 10, 'page' => $pageno), 'GET');
            } else {
                $api_list = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."{$api_name}.json", array('limit' => 10, 'page' => $pageno), 'GET');
            }
        }
        $api_list = json_decode($api_list['response']);
        return $api_list;
    }

    /**
     * Shopify recurring application charge
     */
    function recurring_application_charge($shop, $array) {
        $shopinfo = $this->getUserData($shop);
        $store_client_id = $shopinfo->store_client_id;
        $recurring_application_charge = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."recurring_application_charges.json", $array, 'POST');
        $recurring_application_charge = json_decode($recurring_application_charge['response']);
        return $recurring_application_charge;
    }

    /**
     * Shopify recurring application charge acitvate
     */
    function charge_activate($charge_id, $shop) {
        $flg = 0;
        $shopinfo = $this->getUserData($shop);
        $store_client_id = $shopinfo->store_client_id;
        $charge = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."recurring_application_charges/{$charge_id}.json", array(), 'GET');
        $charge = json_decode($charge['response']);
        if ($charge->recurring_application_charge->id == $charge_id && $charge->recurring_application_charge->status == "active") {
//            $charge_activate = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."recurring_application_charges/{$charge_id}/activate.json", array(), 'POST');
            $fields = array(
                'charge_approve' => '1',
                'charge_id' => $charge->recurring_application_charge->id,
            );
            $where = 'store_client_id = ' . $store_client_id;
            $this->update(TABLE_CLIENT_STORES, $fields, $where);
            $flg = 1;
            $shop = $shopinfo->store_name;
        }
        $info = array('flg' => $flg, 'store_name' => $shop);
        return $info;
    }

    /**
     * Shopify recurring application charge acitvate
     */
    function charge_deactivate($shop) {
        $flg = 0;
        $shopinfo = $this->getUserData($shop);
        $store_client_id = $shopinfo->store_client_id;
        $charge_id = $shopinfo->charge_id;
        //echo "/admin/recurring_application_charges/{$charge_id}.json";exit;
        $charge = shopify_call($shopinfo->token, $shopinfo->store_name, SHOPIFY_API_VERSION."recurring_application_charges/{$charge_id}.json", array(), 'DELETE');
        $charge = json_decode($charge['response']);
        $fields = array(
            'charge_approve' => '0',
            'downgrade_plan' => '1',
        );
        $where = 'store_client_id = ' . $store_client_id;
        $this->update(TABLE_CLIENT_STORES, $fields, $where);
        
        /* In active all offer except first type1 offer (buy product x get product y ) */
        $offer_rows_query = $this->query("SELECT COUNT(id) FROM `" . TABLE_OFFERS . "` WHERE store_client_id = $store_client_id;");
        $offer_rows = $offer_rows_query->fetch_row();
        $update_limit = $offer_rows[0];
        
        
        /* app user charge not approve so we deactive all offer except one */

        /* check if type1 offer (buy product x get product y ) */
        if($this->get_user_offer1_cnt($store_client_id)>0){
            $update_limit = $update_limit-1;
        }
        
        if($update_limit > 0){
            $where = "store_client_id = $store_client_id;";
            $fields = array('status' => 1);
            $this->update(TABLE_OFFERS, $fields, $where);
            
            $where = "store_client_id = $store_client_id ORDER BY type DESC,id DESC LIMIT $update_limit";
            $fields = array('status' => 0);
            $this->update(TABLE_OFFERS, $fields, $where);
            
            /* Remove Extra product from active one */
            if($this->get_user_offer1_cnt($store_client_id)>0){
                $get_offer_id_query=$this->select(TABLE_OFFERS," WHERE `store_client_id`='$store_client_id' AND `status` = '1' LIMIT 1");
                $get_offer_id = $get_offer_id_query->fetch_row();
                if($get_offer_id[0] != null){
                    $offer_id = $get_offer_id[0];
                    
                    $get_first_prod_id_query=$this->query("SELECT MIN(`id`) FROM `" . TABLE_GETY_PRODUCTS . "` WHERE `offer_id` = $offer_id ;");
                    $get_first_prod_id = $get_first_prod_id_query->fetch_row();
                    $first_prod_id = $get_first_prod_id[0];
                    
                    /* remove all other product except one which is first inserted(added) in type1 offer (buy product x get product y ) */
                    $where=" `offer_id` = $offer_id AND `id` > $first_prod_id";
                    $this->delete(TABLE_GETY_PRODUCTS, $where);
                }
            }
            /* Remove Extra product from active one END */
        }
        /* END of In active all offer except first type1 offer (buy product x get product y ) */
        
        $flg = 1;
        $shop = $shopinfo->store_name;
        $info = array('flg' => $flg, 'store_name' => $shop);
        return $info;
    }

    function get_products_records_old($post) {
        $data = array();
        $count = $this->get_api_count($post['shop'], 'products', $post['input_value']);
        $product_count = ceil($count->count / 10);
        $product_data = array();
        $product_list = $this->get_api_list($post['shop'], 'products', $post['current_page'], $post['input_value']);
        for ($i = 0; $i < count($product_list->products); $i++) {
            $product_array = array();
            $product_array['images_src'] = $images = $product_list->products[$i]->image == '' ? '' : $product_list->products[$i]->image->src;
            $product_array['images'] = $product_list->products[$i]->image == '' ? '<div class="image-icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M19 0H1C.448 0 0 .448 0 1v18c0 .552.448 1 1 1h18c.552 0 1-.448 1-1V1c0-.552-.448-1-1-1zM8 6c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.654 0 3-1.346 3-3S9.654 4 8 4 5 5.346 5 7s1.346 3 3 3zm-6 8v-2.434l3.972-2.383 2.473 1.65c.398.264.925.21 1.262-.126l4.367-4.367L18 13.48V18H2zM18 2v8.92l-3.375-2.7c-.398-.32-.973-.287-1.332.073l-4.42 4.42-2.318-1.545c-.322-.214-.74-.225-1.07-.025L2 13.233V2h16z" fill="#c4cdd5" fill-rule="evenodd"></path></svg></div>' : '<img src="' . $product_list->products[$i]->image->src . '" alt="' . $product_list->products[$i]->title . '" class="Polaris-Thumbnail__Image">';
            $product_array['image'] = $product_list->products[$i]->image == '' ? '' : $product_list->products[$i]->image->src;
            $product_array['title'] = (strlen($product_list->products[$i]->title) > 40 ) ? substr($product_list->products[$i]->title, 0, 40) . '...' : $product_list->products[$i]->title;
            $product_array['id'] = $product_list->products[$i]->id;
            $product_array['variant_id'] = $product_list->products[$i]->variants[0]->id;
            $product_array['handle'] = $product_list->products[$i]->handle;
            $product_array['price'] = $product_list->products[$i]->variants[0]->price;
            $product_array['type'] = isset($post['type']) ? $post['type'] : '';
            $product_array['get_input_name'] = isset($post['get_input_name']) ? $post['get_input_name'] : '';
            $product_data[] = $product_array;
        }
        echo json_encode(array('products' => $product_data, 'count' => $product_count, 'total_count' => $count->count));
    }

    /* function helps to check whether offer added product is already used by another offer type 
     * @$product_ids string product id seprator by comma
     *      */
    
    function product_already_in_used($store_client_id, $product_ids, $offer_type){
        $already_used_prod_query=$this->query("SELECT o.`id`,gp.`product_id`,o.`title`,
                (SELECT p.title FROM `product_list` AS p WHERE p.`product_id` = gp.`product_id` LIMIT 1) AS product_title
                FROM `".TABLE_GETY_PRODUCTS."` AS gp INNER JOIN `".TABLE_OFFERS."` AS o ON o.`id`=gp.`offer_id` WHERE 
                gp.store_client_id=$store_client_id AND o.`status` = 1 AND o.`type` = '$offer_type' AND gp.`product_id` IN($product_ids);");
        $used_prod_arr = array();
        if($already_used_prod_query->num_rows > 0){
            while($used_prod = $already_used_prod_query->fetch_assoc()){
                $used_prod_arr[]=$used_prod;
            }
        }
        return $used_prod_arr;
    }
    
    function addeditOffer($post, $shop) {
        $insert_id = $css_class = $message = "";
        $status = "401";
        $i = 0;
        $shop_details = $this->getUserData($shop);
        if ($post['title'] == "") {
            $message .= "<li>Title can't be blank</li>";
            $i++;
        }
        
        
        if ($post['type'] == 1) {
            
            if($shop_details->charge_approve == '0' && $this->get_user_offer1_cnt($shop_details->store_client_id) > 0 && (!isset($post['offer_id']) || $post['offer_id'] == "")){
                $message .= '<li>' . DONT_HAVE_PRO_PLAN_MSG . '</li>';
                $i++;
            }
            
            if ($post['buy_of_product_selected'] == "") {
                $message .= "<li>Buy can't be blank</li>";
                $i++;
            }
            if ($post['get_of_product_selected'] == "") {
                $message .= "<li>Get can't be blank</li>";
                $i++;
            }
            if ($post['at'] == "") {
                $message .= "<li>At can't be blank</li>";
                $i++;
            }
            if ($post['buy_of_product_selected'] != "" && $post['get_of_product_selected'] != "") {
                $product_selected = explode(',', $post['get_of_product_selected']);
                
                if($shop_details->charge_approve == '0' && count($product_selected) > 1){
                    $message .= '<li>' . DONT_HAVE_PRO_PLAN_ONLY_ONE_ADD_MSG . '</li>';
                    $i++;
                }
                /* check whether offer added product is already used by another offer type */
                $product_array = $title = array();
                $all_ready_used = $this->product_already_in_used($shop_details->store_client_id, $post['get_of_product_selected'],'2');
                if(!empty($all_ready_used)){
                    foreach($all_ready_used as $product){
                        $product_array[] = '.get_offer_product-' . $product['product_id'];
                        $message .= '<li>Product <b>' . $product['product_title'] . '</b> is already utilized in offer <b>' . $product['title'] . '</b>. Please Change/Remove below highlighted product.</li>';
                        $i++;
                    }
                }
                
                if (in_array($post['buy_of_product_selected'], $product_selected)) {
                    $message .= '<li>In "Buy Section" and "Get Section" is not allow same product.Please Change added product.</li>';
                    $i++;
                }
                
                if (isset($post['offer_id']) && $post['offer_id'] != "") {
                    $buy_list = $this->query('SELECT o.title FROM ' . TABLE_GETY_PRODUCTS . ' as gp, ' . TABLE_OFFERS . ' as o  WHERE gp.offer_id = o.id AND gp.store_client_id="' . $shop_details->store_client_id . '" AND gp.product_id = "' . $post['buy_of_product_selected'] . '" AND gp.offer_id != "' . $post['offer_id'] . '"');
                    //$where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" AND product_id = "' . $post['buy_of_product_selected'] . '" AND offer_id != "' . $post['offer_id'] . '"';
                } else {
                    $buy_list = $this->query('SELECT o.title FROM ' . TABLE_GETY_PRODUCTS . ' as gp, ' . TABLE_OFFERS . ' as o  WHERE gp.offer_id = o.id AND gp.store_client_id="' . $shop_details->store_client_id . '" AND gp.product_id = "' . $post['buy_of_product_selected'] . '"');
                    //$where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" AND product_id = "' . $post['buy_of_product_selected'] . '"';
                }
                //$buy_list = $this->select(TABLE_GETY_PRODUCTS, $where);
                if (isset($buy_list) && $buy_list->num_rows > 0) {
                    $buy_list = $buy_list->fetch_object();
                    $message .= '<li>The product chosen for the customer "Buy section" is already added to Active Cart campaign <b>' . $buy_list->title . '</b> in the customer "Get section". Please Change/Remove highlighted product.</li>';
                    $i++;
                }

                if (isset($post['offer_id']) && $post['offer_id'] != "") {
                    $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" AND id != "' . $post['offer_id'] . '"';
                } else {
                    $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" AND type = "1"';
                }
                
                $get_list = $this->select(TABLE_OFFERS, $where);
               
                if (isset($get_list) && $get_list->num_rows > 0) {
                    
                    while ($get_row = $get_list->fetch_object()) {
                        if (in_array($get_row->buyx, $product_selected)) {
//                        if ($get_row->buyx == $post['buy_of_product_selected']) {
                            $product_array[] = '.get_offer_product-' . $get_row->buyx;
                            $title[] = $get_row->title;
                        }
                    }
                    
                    if (!empty($product_array) && !empty($title)) {
                        $message .= '<li>The product chosen for the customer "Get section" has already been added to Active Cart campaign <b>' . implode(",", $title) . '</b> in the customer "Buy section". Please Change/Remove highlighted product.</li>';
                        $i++;
                    }
                    if (!empty($product_array)) {
                        $css_class = implode(',', $product_array);
                    }
                }
            }
        } else if ($post['type'] == 2) {
            if ($post['spend'] == "") {
                $message .= "<li>Spend can't be blank</li>";
                $i++;
            }
            if ($post['gety'] == "") {
                $message .= "<li>Get y can't be blank</li>";
                $i++;
            }
            if ($post['on_of_product_selected'] == "") {
                $message .= "<li>Get can't be blank</li>";
                $i++;
            }
            
            if($shop_details->charge_approve == '0'){
                $message .= '<li>' . DONT_HAVE_PRO_PLAN_MSG . '</li>';
                $i++;
            }
            
            if ($post['on_of_product_selected'] != "") {
                $product_selected = explode(',', $post['on_of_product_selected']);
                /* check whether offer added product is already used by another offer type */
                $all_ready_used = $this->product_already_in_used($shop_details->store_client_id, $post['on_of_product_selected'],'1');
                if(!empty($all_ready_used)){
                    foreach($all_ready_used as $product){
                        $message .= '<li>Product <b>' . $product['product_title'] . '</b> is already utilized in offer <b>' . $product['title'] . '</b></li>';
                        $i++;
                    }
                }
                
                if (isset($post['offer_id']) && $post['offer_id'] != "") {
                    $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" AND id != "' . $post['offer_id'] . '"';
                } else {
                    $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" AND type = "2"';
                }
                $get_list = $this->select(TABLE_OFFERS, $where);
                if (isset($get_list) && $get_list->num_rows > 0) {
                    $product_array = $title = array();
                    while ($get_row = $get_list->fetch_object()) {
                        //if (in_array($get_row->buyx, $product_selected)) {
                        if ($get_row->buyx == $post['spend']){
                            $product_array[] = '.on_offer_product-' . $get_row->buyx;
                            $title[] = $get_row->title;
                        }
                    }
                    //print_r($title);
                    if (!empty($product_array)) {
                        $message .= "<li>Same spend amount already created Active Cart offer of <b>" . implode(',', $title) . "</b> Campaign. Please Change spend amount.</li>";
                        $i++;
                        $css_class = implode(',', $product_array);
                    }
                }
            }
        }
        
        if ($message == '') {
            if ($post['type'] == 1) {
                $fields = array(
                    'store_client_id' => $shop_details->store_client_id,
                    'title' => $post['title'],
                    'type' => $post['type'],
                    'buyx' => $post['buy_of_product_selected'],
                    'at' => $post['at'],
                    'created' => DATE
                );
                
                if($shop_details->charge_approve == '0' && (!isset($post['offer_id']) || $post['offer_id'] == "")){
                    $fields['status'] = 0;
                }
                
                $product_selected = explode(',', $post['get_of_product_selected']);
                $product_variant_selected = explode(',', $post['get_of_product_variant_selected']);
                $product_price_selected = explode(',', $post['get_of_product_price_selected']);
            } else {
                $fields = array(
                    'store_client_id' => $shop_details->store_client_id,
                    'title' => $post['title'],
                    'type' => $post['type'],
                    'buyx' => $post['spend'],
                    'at' => $post['gety'],
                    'created' => DATE
                );
                if($shop_details->charge_approve == '0'){
                    $fields['status'] = 0;
                }
                $product_selected = explode(',', $post['on_of_product_selected']);
                $product_variant_selected = explode(',', $post['on_of_product_variant_selected']);
                $product_price_selected = explode(',', $post['on_of_product_price_selected']);
            }
            
            if (isset($post['offer_id']) && $post['offer_id'] != "") {
                $where = 'id = "' . $post['offer_id'] . '"';
                if ($this->update(TABLE_OFFERS, $fields, $where)) {
                    $where = 'offer_id = "' . $post['offer_id'] . '"';
                    $this->delete(TABLE_GETY_PRODUCTS, $where);
                    for ($j = 0; $j < count($product_selected); $j++) {
                        $fields = array(
                            'store_client_id' => $shop_details->store_client_id,
                            'type' => $post['type'],
                            'offer_id' => $post['offer_id'],
                            'product_id' => $product_selected[$j],
                            'variant_id' => $product_variant_selected[$j],
                            'product_price' => $product_price_selected[$j]
                        );
                        $this->insert(TABLE_GETY_PRODUCTS, $fields);
                    }
                    $status = 200;
                    $message = 'Record Successuly Updated.';
                } else {
                    $status = '400';
                }
            } else {
                if ($this->insert(TABLE_OFFERS, $fields)) {
                    $insert_id = $this->db_connection->insert_id;
                    for ($j = 0; $j < count($product_selected); $j++) {
                        $fields = array(
                            'store_client_id' => $shop_details->store_client_id,
                            'type' => $post['type'],
                            'offer_id' => $insert_id,
                            'product_id' => $product_selected[$j],
                            'variant_id' => $product_variant_selected[$j],
                            'product_price' => $product_price_selected[$j]
                        );
                        $this->insert(TABLE_GETY_PRODUCTS, $fields);
                    }
                    $status = 200;
                    $message = 'Record Successuly Added.';
                } else {
                    $status = '400';
                }
            }
            if ($post['type'] == 1) {
                $product_selected = implode(',', $product_selected) . "," . $post['buy_of_product_selected'];
            } else {
                $product_selected = implode(',', $product_selected);
            }

            $product_list = $this->get_api_list($shop, 'products', '', '', $product_selected);
            for ($j = 0; $j < count($product_list->products); $j++) {
                $fields = array(
                    'store_client_id' => $shop_details->store_client_id,
                    'product_id' => $product_list->products[$j]->id,
                    'title' => $product_list->products[$j]->title,
                    'image' => $product_list->products[$j]->image == '' ? '' : $product_list->products[$j]->image->src,
                    'created' => DATE
                );
                $this->insert(TABLE_PRODUCT_LIST, $fields);
            }
        }
        $is_show_all_offer = (isset($post['is_show_all_offer'])) ? 1 : 0;
        if ($post['type'] == 2) {
            $where = 'store_client_id = "' . $shop_details->store_client_id . '"';
            $this->update(TABLE_SETTINGS, array('is_show_all_offer' => $is_show_all_offer), $where);
        }

        echo json_encode(array('status' => $status, 'id' => $insert_id, 'message' => $message, 'count' => $i, 'css_class' => $css_class));
    }

    function get_offer($shop, $id = "") {
        $shop_details = $this->getUserData($shop);
        //$result = $this->select("SELECT * from " . TABLE_CLIENT_STORES . " where status = 1 AND store_name = '" . $store_name . "' LIMIT 0,1");
        if ($id == "") {
            $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '"';
        } else {
            $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" and id="' . $id . '"';
        }
        $offers = $this->select(TABLE_OFFERS, $where);
        return $offers;
    }

    function get_offer_load($shop, $page) {
        $shop_details = $this->getUserData($shop);
        $money_format = $shop_details->money_format;
        /*if ($shop_details->charge_approve) {
            if ($page == 1) {
                $limit = 0;
            } else {
                $limit = ($page * PAGE_PER) - PAGE_PER;
            }
            $offset = PAGE_PER;
        } else {
            $limit = 0;
            $offset = 2;
        }*/
        
        if ($page == 1) {
            $limit = 0;
        } else {
            $limit = ($page * PAGE_PER) - PAGE_PER;
        }
        $offset = PAGE_PER;
     
        $offer_rows_query = $this->query("SELECT COUNT(id) FROM `" . TABLE_OFFERS . "` WHERE store_client_id = $shop_details->store_client_id;");
        $offer_rows = $offer_rows_query->fetch_row();
        $total_page = ceil($offer_rows[0] / PAGE_PER);

        $where = 'WHERE store_client_id="' . $shop_details->store_client_id . '" ORDER BY status DESC,type,id DESC LIMIT ' . $limit . ',' . $offset;
        $offers = $this->select(TABLE_OFFERS, $where);
        $row = array();
        if (isset($offers) && $offers->num_rows > 0) {
            while ($offer = $offers->fetch_object()) {
                $offer_text = $get_y_product = array();
                if ($offer->buyx == '') {
                    if ($offer->type == 1) {
                        $offer_text['type_text'] = 'Buy X Get Y';
                    } else {
                        $offer_text['type_text'] = 'Spend X amount Get Y product';
                    }
                } else {
                    $get_list = $this->query('SELECT gp.* FROM ' . TABLE_GETY_PRODUCTS . ' as gp, ' . TABLE_OFFERS . ' as o  WHERE gp.offer_id = o.id AND gp.store_client_id="' . $shop_details->store_client_id . '" AND gp.offer_id = "' . $offer->id . '"');
                    while ($get_list_row = $get_list->fetch_object()) {
                        $where = 'WHERE product_id="' . $get_list_row->product_id . '"';
                        $get_product_details = $this->select(TABLE_PRODUCT_LIST, $where);
                        $get_product_detail = $get_product_details->fetch_object();
                        $get_y_product[] = (strlen($get_product_detail->title) > 20 ) ? '<span class="p_title" title="' . $get_product_detail->title . '">' . $get_product_detail->title . '</span>' : '<span class="p_title" title="' . $get_product_detail->title . '">' . $get_product_detail->title . '</span>';
                    }
                    if ($offer->type == 1) {
                        $where = 'WHERE product_id="' . $offer->buyx . '"';
                        $product_details = $this->select(TABLE_PRODUCT_LIST, $where);
                        $product_detail = $product_details->fetch_object();
                        $buy_x_product = (strlen($product_detail->title) > 20 ) ? '<span class="p_title" title="' . $product_detail->title . '">' . $product_detail->title . '</span>' : '<span class="p_title" title="' . $product_detail->title . '">' . $product_detail->title . '</span>';
                        $offer_text['type_text'] = "Buy Product : <b>" . $buy_x_product . "</b><br> Get Products : <b>" . implode(',', $get_y_product) . "</b> <br> at <b>" . $offer->at . "% Off</b>";
                    } else {
                        $offer_text['type_text'] = "Spend : <b>" . $this->money_formate_replace($money_format, $offer->buyx) . "</b><br> Get Products : <b>" . implode(',', $get_y_product) . " free</b>";
                    }
                }
//                if($shop_details->store_client_id == 383){
//                    $offer_text['type_text'] = 'hi';
//                }
                $row[] = (object) array_merge((array) $offer, $offer_text);
            }
        }
        $response = array('data' => $row, 'count' => $offers->num_rows, 'page' => $total_page, 'plan' => $shop_details->charge_approve);
        $return_data = json_encode($response);
        if ($return_data === false) {
            function my_json_encode($arr) {
                /* convmap since 0x80 char codes so it takes all multibyte codes (above ASCII 127). So such characters are being "hidden" from normal json_encoding */
                array_walk_recursive($arr, function (&$item, $key) {
                    if (is_string($item))
                        $item = mb_encode_numericentity($item, array(0x80, 0xffff, 0, 0xffff), 'UTF-8');
                });
                return mb_decode_numericentity(json_encode($arr), array(0x80, 0xffff, 0, 0xffff), 'UTF-8');
            }
            $return_data = my_json_encode($response);
        }
        echo $return_data;
       
    }

    function get_selected_product($shop, $ids) {
        $product_list = $this->get_api_list($shop, 'products', '', '', $ids);
        $product_data = array();
        for ($i = 0; $i < count($product_list->products); $i++) {
            $product_array = array();
            $product_array['images_src'] = $images = $product_list->products[$i]->image == '' ? '' : $product_list->products[$i]->image->src;
            $product_array['images'] = $product_list->products[$i]->image == '' ? '<div class="image-icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M19 0H1C.448 0 0 .448 0 1v18c0 .552.448 1 1 1h18c.552 0 1-.448 1-1V1c0-.552-.448-1-1-1zM8 6c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.654 0 3-1.346 3-3S9.654 4 8 4 5 5.346 5 7s1.346 3 3 3zm-6 8v-2.434l3.972-2.383 2.473 1.65c.398.264.925.21 1.262-.126l4.367-4.367L18 13.48V18H2zM18 2v8.92l-3.375-2.7c-.398-.32-.973-.287-1.332.073l-4.42 4.42-2.318-1.545c-.322-.214-.74-.225-1.07-.025L2 13.233V2h16z" fill="#c4cdd5" fill-rule="evenodd"></path></svg></div>' : '<img src="' . $product_list->products[$i]->image->src . '" alt="' . $product_list->products[$i]->title . '" class="Polaris-Thumbnail__Image">';
            $product_array['image'] = $product_list->products[$i]->image == '' ? '' : $product_list->products[$i]->image->src;
            $product_array['title'] = (strlen($product_list->products[$i]->title) > 40 ) ? substr($product_list->products[$i]->title, 0, 40) . '...' : $product_list->products[$i]->title;
            $product_array['id'] = $product_list->products[$i]->id;
            $product_array['variant_id'] = $product_list->products[$i]->variants[0]->id;
            $product_array['handle'] = $product_list->products[$i]->handle;
            $product_array['price'] = $product_list->products[$i]->variants[0]->price;
            $product_array['type'] = isset($post['type']) ? $post['type'] : '';
            $product_array['get_input_name'] = isset($post['get_input_name']) ? $post['get_input_name'] : '';
            $product_data[] = $product_array;
        }
        echo json_encode(array('products' => $product_data, 'count' => '', 'total_count' => ''));
    }
    /* check shop has privileges */
    public function is_shop_privileges($store_client_id, $offer_id, $status) {
        $is_charge_approve_query = $this->query("SELECT COUNT(store_client_id) AS is_charge_approve FROM `" . TABLE_CLIENT_STORES . "` WHERE `store_client_id` = $store_client_id AND `charge_approve` = '1' ;");
        $is_charge_approve = $is_charge_approve_query->fetch_row();
        $msg = TRUE;
        if ($is_charge_approve[0] == '0') {
            /* shop have not permission to update discount type 2 */
            $check_dis_type_query = $this->query("SELECT type FROM `" . TABLE_OFFERS . "` WHERE `store_client_id` = $store_client_id AND `id` = $offer_id ;");
            $discount_type = $check_dis_type_query->fetch_row();
            
            $msg = DONT_HAVE_PRO_PLAN_MSG;
            
            /*if ($discount_type[0] == '2') {
                $msg = DONT_HAVE_PRO_PLAN_MSG;
            } elseif ($status == '1') {
                /* here we make all statu as 0 for all discount_type 1  * /
                $where = 'store_client_id="' . $store_client_id . '"';
                $fields = array('status' => 0);
                $this->update(TABLE_OFFERS, $fields, $where);
            } */
        }
        return $msg;
    }

    public function offer_status_change($post) {
        $shop_details = $this->getUserData($post['shop']);
        $is_shop_privileges=TRUE;
        
        if($post['status'] == '1'){
            if($this->get_user_offer1_cnt($shop_details->store_client_id,1) > 0){
                $is_shop_privileges = $this->is_shop_privileges($shop_details->store_client_id, $post['offer_id'], $post['status']);
            }
        }
        
        if($is_shop_privileges === TRUE){
            /* check whether offer added product is already used by another offer type */
            $err_cnt = 0;
            if($post['status'] == '1'){
                $get_all_offer_prod_id_query=$this->query("SELECT GROUP_CONCAT(`product_id`) FROM `".TABLE_GETY_PRODUCTS."` WHERE `offer_id` = ".$post['offer_id']." GROUP BY `offer_id`");
                if($get_all_offer_prod_id_query->num_rows > 0){
                    $offer_type = '1';
                    if($post['offer_type'] == '1'){
                        $offer_type = '2';
                    }

                    $get_all_offer_prod_id = $get_all_offer_prod_id_query->fetch_row();
                    $get_all_offer_prod_ids = $get_all_offer_prod_id[0];
                    //$get_all_offer_prod_id_arr = explode(',', $get_all_offer_prod_id[0]);

                    $all_ready_used = $this->product_already_in_used($shop_details->store_client_id, $get_all_offer_prod_ids, $offer_type);
                    if(!empty($all_ready_used)){
                        $message = '';
                        
                        foreach($all_ready_used as $product){
                            $message .= '<li>Product <b>' . $product['product_title'] . '</b> is already utilized in offer <b>' . $product['title'] . '</b></li>';
                            $err_cnt++;
                        }
                        echo json_encode(array('code' => '400', 'messages_li' => $message, 'count'=>$err_cnt));
                        exit;
                    }
                }
            }
            /* END check whether offer added product is already used by another offer type */
        
            $where = 'Where id = "' . $post['offer_id'] . '" and store_client_id="' . $shop_details->store_client_id . '"';
            $offers = $this->select(TABLE_OFFERS, $where);
            if (isset($offers) && $offers->num_rows > 0 && $err_cnt == 0) {
                $where = 'id = "' . $post['offer_id'] . '" and store_client_id="' . $shop_details->store_client_id . '"';
                $fields = array('status' => $post['status']);
                $this->update(TABLE_OFFERS, $fields, $where);
                echo json_encode(array('code' => '200', 'message' => 'Offer status successfully change.'));
            } else {
                echo json_encode(array('code' => '400', 'message' => 'Sorry, Something Went Wrong: Please try again later.'));
            }
        }else{
            echo json_encode(array('code' => '400', 'message' => $is_shop_privileges));
        }
    }

    public function offer_delete($post) {
        $shop_details = $this->getUserData($post['shop']);
        $where = 'Where id = "' . $post['offer_id'] . '" and store_client_id="' . $shop_details->store_client_id . '"';
        $offers = $this->select(TABLE_OFFERS, $where);
        if (isset($offers) && $offers->num_rows > 0) {
            $where = 'id = "' . $post['offer_id'] . '" and store_client_id="' . $shop_details->store_client_id . '"';
            $this->delete(TABLE_OFFERS, $where);
            $where = 'offer_id = "' . $post['offer_id'] . '"';
            $this->delete(TABLE_GETY_PRODUCTS, $where);
            echo json_encode(array('code' => '200', 'message' => 'Offer deleted successfully.'));
        } else {
            echo json_encode(array('code' => '400', 'message' => ''));
        }
    }

    public function tour_status($post) {
        $shop_details = $this->getUserData($post['shop']);
        $where = 'store_client_id="' . $shop_details->store_client_id . '"';
        $fields = array('tour_status' => $post['tour_status']);
        $this->update(TABLE_CLIENT_STORES, $fields, $where);
        echo json_encode(array('code' => '200', 'message' => ''));
    }
    
    /* color hex validation */

    private function is_color_hex($number) {
        //return filter_var($number, FILTER_VALIDATE_FLOAT);
        return preg_match("/#([a-f0-9]{3}){1,2}\b/i", $number);
    }
    
    /* floating number validation also is_numeric used */

    private function is_floating_number($number) {
        //return filter_var($number, FILTER_VALIDATE_FLOAT);
        return preg_match("/^[0-9.]+$/i", $number);
    }
    

    /*
     * For Settings page
     */
    /* settngs data validation function */
    function validation_settings($post){
        $err_msg_arr = array();
        if (!empty($post)) {
            
            /* font_family validation 
            if (!isset($post['font_family']) || $post['font_family'] == '') {
                $err_msg_arr[] = FONT_FAMILY_REQUIRED_MSG;
            }
            */
            
            /* Prouduct title css validation */
            if (!isset($post['pt_text_color']) || $post['pt_text_color'] == '') {
                $err_msg_arr[] = PT_TEXT_COLOR_REQUIRED_MSG;
            }else if(!$this->is_color_hex($post['pt_text_color'])){
                $err_msg_arr[] = PT_TEXT_COLOR_FORMAT_MSG;
            }
            
            if (!isset($post['pt_font_size']) || $post['pt_font_size'] == '') {
                $err_msg_arr[] = PT_FONT_SIZE_REQUIRED_MSG;
            }elseif(!$this->is_floating_number($post['pt_font_size'])){
                $err_msg_arr[] = PT_FONT_SIZE_ONLYFLOAT_MSG;
            }
            
            /* original price css validation */
            if (!isset($post['ori_price_color']) || $post['ori_price_color'] == '') {
                $err_msg_arr[] = ORI_PRICE_COLOR_REQUIRED_MSG;
            }else if(!$this->is_color_hex($post['ori_price_color'])){
                $err_msg_arr[] = ORI_PRICE_COLOR_FORMAT_MSG;
            }
            
            if (!isset($post['ori_price_font_size']) || $post['ori_price_font_size'] == '') {
                $err_msg_arr[] = ORI_PRICE_FONT_SIZE_REQUIRED_MSG;
            }elseif(!$this->is_floating_number($post['ori_price_font_size'])){
                $err_msg_arr[] = ORI_PRICE_FONT_SIZE_ONLYFLOAT_MSG;
            }
            
            /* discount price css validation */
            if (!isset($post['dis_price_color']) || $post['dis_price_color'] == '') {
                $err_msg_arr[] = DIS_PRICE_COLOR_REQUIRED_MSG;
            }else if(!$this->is_color_hex($post['dis_price_color'])){
                $err_msg_arr[] = DIS_PRICE_COLOR_FORMAT_MSG;
            }
            
            if (!isset($post['dis_price_font_size']) || $post['dis_price_font_size'] == '') {
                $err_msg_arr[] = DIS_PRICE_FONT_SIZE_REQUIRED_MSG;
            }elseif(!$this->is_floating_number($post['dis_price_font_size'])){
                $err_msg_arr[] = DIS_PRICE_FONT_SIZE_ONLYFLOAT_MSG;
            }
            
            /* product image css validation */
            if (!isset($post['image_width']) || $post['image_width'] == '') {
                $err_msg_arr[] = IMAGE_WIDTH_REQUIRED_MSG;
            }elseif(!$this->is_floating_number($post['image_width'])){
                $err_msg_arr[] = IMAGE_WIDTH_ONLYFLOAT_MSG;
            }
            
            if (!isset($post['image_height']) || $post['image_height'] == '') {
                $err_msg_arr[] = IMAGE_HEIGHT_REQUIRED_MSG;
            }elseif(!$this->is_floating_number($post['image_height'])){
                $err_msg_arr[] = IMAGE_HEIGHT_ONLYFLOAT_MSG;
            }
            
            /* None of above validation */
            if (!isset($post['none_of_above_text']) || $post['none_of_above_text'] == '') {
                $err_msg_arr[] = NOA_MESSAGE_REQUIRED_MSG;
            }
            
            if (!isset($post['none_of_color']) || $post['none_of_color'] == '') {
                $err_msg_arr[] = NOA_COLOR_REQUIRED_MSG;
            }else if(!$this->is_color_hex($post['none_of_color'])){
                $err_msg_arr[] = NOA_COLOR_FORMAT_MSG;
            }
            
            if (!isset($post['none_of_font_size']) || $post['none_of_font_size'] == '') {
                $err_msg_arr[] = NOA_FONT_SIZE_REQUIRED_MSG;
            }elseif(!$this->is_floating_number($post['none_of_font_size'])){
                $err_msg_arr[] = NOA_FONT_SIZE_ONLYFLOAT_MSG;
            }
            
            
        } else {
            $err_msg_arr[] = POST_NOT_SUBMITED_ERR_MSG;
        }
        
        return $err_msg_arr;
    }
    
    /* save settings */
    function set_settings($post) {
        $response = array('result' => 'fail', 'msg' => SOMETHING_WENT_WRONG_MSG);
        if (isset($post['shop']) && $post['shop'] != '') {
            $shopinfo = $this->getUserData($post['shop']);
            $err_msg_arr = $this->validation_settings($post);
            if (count($err_msg_arr) > 0) {
                /* handle validation error */
                $response['err_msg_arr'] = $err_msg_arr;
                $total_error = count($err_msg_arr);
                if ($total_error == 1) {
                    $response['msg_heading'] = '<p class="Polaris-Heading">There is ' . $total_error . ' error to save shipping method:</p>';
                } else {
                    $response['msg_heading'] = '<p class="Polaris-Heading">There are ' . $total_error . ' errors to save shipping method:</p>';
                }
                $response['msg_content'] = '<ul><li>' . implode('</li><li>', $err_msg_arr) . '</li></ul>';
            } else {

                $product_title_css = serialize( array(
                    'color' => $post['pt_text_color'],
                    'font-size' => $post['pt_font_size']
                ) );
                
                $original_pirce_css = serialize( array(
                    'color' => $post['ori_price_color'],
                    'font-size' => $post['ori_price_font_size']
                ) );
                
                $discount_pirce_css = serialize( array(
                    'color' => $post['dis_price_color'],
                    'font-size' => $post['dis_price_font_size']
                ) );
                
                $image_css = serialize( array(
                    'width' => $post['image_width'],
                    'height' => $post['image_height']
                ) );
                
                $none_of_above_css = serialize( array(
                    'color' => $post['none_of_color'],
                    'font-size' => $post['none_of_font_size']
                ) );
                


                $fields = array(
                    '`store_client_id`' => $shopinfo->store_client_id,
                    '`font_family`' => mysqli_real_escape_string($this->db_connection, $post['font_family']),
                    '`product_title_css`' => $product_title_css,
                    '`original_pirce_css`' => $original_pirce_css,
                    '`discount_pirce_css`' => $discount_pirce_css,
                    '`image_css`' => $image_css,
                    '`none_of_above_text`' => mysqli_real_escape_string($this->db_connection, $post['none_of_above_text']),
                    '`none_of_above_css`' => $none_of_above_css,
                    '`created_on`' => date('Y-m-d H:i:s'),
                    '`updated_on`' => date('Y-m-d H:i:s')
                    
                );
                
                $last_id = $this->insert_on_duplicate_update(TABLE_SETTINGS, $fields);
                if ($last_id) {
                    $response['result'] = 'success';
                    $response['msg'] = SETTINGS_CREATED_SUCCESS_MSG;
                    $response['last_id'] = $last_id;
                }

                //$cwp_id = $this->insert(TABLE_CUSTOMER_WISE_PRODUCT, $fields);
            }
        } /* cechcking for shop post or not end */
        echo json_encode($response);
    }

    /* get setting data */
    function get_settings($shop){
        $shopinfo = $this->getUserData($shop);
        $where = " WHERE  `store_client_id` = $shopinfo->store_client_id ;" ;
        $settings_db_resource=$this->select(TABLE_SETTINGS, $where);
        $return_settings_arr=array();
        if($settings_db_resource->num_rows>0){
            $setting=$settings_db_resource->fetch_object();
                       
            $return_settings_arr['font_family']=$setting->font_family;
            
            /* product title css settings */
            $css_rule = unserialize($setting->product_title_css);
            
            $return_settings_arr['pt_text_color']=$css_rule['color'];
            $return_settings_arr['pt_font_size']=$css_rule['font-size'];
            
            /* Product original price css settings */
            $css_rule = unserialize($setting->original_pirce_css);
            
            $return_settings_arr['ori_price_color']=$css_rule['color'];
            $return_settings_arr['ori_price_font_size']=$css_rule['font-size'];
            
            /* Product discount price css settings */
            $css_rule = unserialize($setting->discount_pirce_css);
            
            $return_settings_arr['dis_price_color']=$css_rule['color'];
            $return_settings_arr['dis_price_font_size']=$css_rule['font-size'];
            
            /* Product Image css settings */
            $css_rule = unserialize($setting->image_css);
            
            $return_settings_arr['image_width']=$css_rule['width'];
            $return_settings_arr['image_height']=$css_rule['height'];
            
            /* none of above text and css */
            $return_settings_arr['none_of_above_text']=$setting->none_of_above_text;
            
            $css_rule = unserialize($setting->none_of_above_css);
            
            $return_settings_arr['none_of_color']=$css_rule['color'];
            $return_settings_arr['none_of_font_size']=$css_rule['font-size'];

            $return_settings_arr['is_show_all_offer'] = $setting->is_show_all_offer; 
            
        }
        return $return_settings_arr;
    }
    
    /* get font-family */

    public function get_font_family() {
        $result = $this->db_connection->query("SELECT * FROM " . TABLE_FONT_FAMILY . " ORDER BY name ASC");
        return $result;
    }
    
    function get_products_records($post) {
        $data = array();
        $api_name = $post['api_name'];
        $limit = isset($post['limit']) ? $post['limit'] : 10;
        $pageno = isset($_POST['current_page']) ? $_POST['current_page'] : '1';
        $shop_name = $post['shop'];
        
//        $count = $this->get_api_count($post['shop'], 'products', $post['input_value']);
//        $total_page = ceil($count->count / 10);
        
        $api_data_list = array();
        $url_param_arr = array();
        
        $search_keyword = (isset($post['input_value']) && $post['input_value'] != '') ? $post['input_value'] : '';
        $page_query = (isset($post['page_query']) && !empty($post['page_query'])) ? $post['page_query'] : 'after';
        $prod_cursor = (isset($post['page_cursor']) && $post['page_cursor'] != '') ? $post['page_cursor'] :null;
        $prod_cursor = ($pageno == 1 && $page_query == 'after') ? null : $prod_cursor;
        $api_data_arr = array("data_file_name" => "pagination-".$api_name."-".$page_query.".txt");
        
        $url_param_arr = array("limit" => (int) $limit,
                "cursor" => $prod_cursor
            );
        $url_param_arr['query'] = ($search_keyword != '') ? "title:$search_keyword*" : '';
        $api_data_list = $this->get_graphql_api_list($api_data_arr, $url_param_arr, 'POST', 0, $shop_name);
        /* 1 => Throttled error found, 0 => Throttled error not found  */
        $error_status = 0;
        if(isset($api_data_list['errors']) && !empty($api_data_list['errors'])) {
            $error_status = 1;
        }
        
        $product_data = array();
        if(isset($api_data_list['data']['products']['edges']) && !empty($api_data_list['data']['products']['edges'])){
            $api_data = $api_data_list['data']['products']['edges'];
            foreach ($api_data as $key => $product) {
                $product_id = substr($product['node']['id'], strrpos($product['node']['id'], '/') + 1);
                $variant_id = substr($product['node']['variants']['edges']['0']['node']['id'], strrpos($product['node']['variants']['edges']['0']['node']['id'], '/') + 1);
                $variant_price = $product['node']['variants']['edges']['0']['node']['price'];
                $product_array = array();
                $product_array['images_src'] = $images = $product['node']['featuredImage'] == '' ? '' : $product['node']['featuredImage']['originalSrc'];
                $product_array['images'] = $product['node']['featuredImage'] == '' ? '<div class="image-icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M19 0H1C.448 0 0 .448 0 1v18c0 .552.448 1 1 1h18c.552 0 1-.448 1-1V1c0-.552-.448-1-1-1zM8 6c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.654 0 3-1.346 3-3S9.654 4 8 4 5 5.346 5 7s1.346 3 3 3zm-6 8v-2.434l3.972-2.383 2.473 1.65c.398.264.925.21 1.262-.126l4.367-4.367L18 13.48V18H2zM18 2v8.92l-3.375-2.7c-.398-.32-.973-.287-1.332.073l-4.42 4.42-2.318-1.545c-.322-.214-.74-.225-1.07-.025L2 13.233V2h16z" fill="#c4cdd5" fill-rule="evenodd"></path></svg></div>' : '<img src="' . $product['node']['featuredImage']['originalSrc'] . '" alt="' . $product['node']['title'] . '" class="Polaris-Thumbnail__Image">';
                $product_array['image'] = $product['node']['featuredImage'] == '' ? '' : $product['node']['featuredImage']['originalSrc'];
                $product_array['title'] = (strlen($product['node']['title']) > 40 ) ? substr($product['node']['title'], 0, 40) . '...' : $product['node']['title'];
                $product_array['text'] = $product_array['title'];
                $product_array['id'] = $product_id;
                $product_array['variant_id'] =  $variant_id;
                $product_array['handle'] = $product['node']['handle'];
                $product_array['price'] = $variant_price;
                $product_array['type'] = isset($post['type']) ? $post['type'] : '';
                $product_array['get_input_name'] = isset($post['get_input_name']) ? $post['get_input_name'] : '';
                $product_data[] = $product_array;
            }
        }
 
        $pagination = array();
        if (!empty($product_data) && ($api_data_list['data']['products']['pageInfo']['hasNextPage'] == 1 || $api_data_list['data']['products']['pageInfo']['hasPreviousPage'] == 1) ) {// data found
            $last_ele = end($api_data_list['data']['products']['edges']);
            $pagination['last_ele_cursor'] = $last_ele['cursor'];
            $pagination['first_ele_cursor'] = $api_data_list['data']['products']['edges'][0]['cursor'];
            $pagination['page_info'] = $api_data_list['data']['products']['pageInfo'];
        }
        echo json_encode(array('products' => $product_data, 'pagination' => $pagination));
//        echo json_encode(array('products' => $product_data, 'count' => $total_page, 'total_count' => $count->count, 'pagination' => $pagination));
        
//        $count = $this->get_api_count($post['shop'], 'products', $post['input_value']);
//        $product_count = ceil($count->count / 10);
//        $product_data = array();
//        $product_list = $this->get_api_list($post['shop'], 'products', $post['current_page'], $post['input_value']);
//        for ($i = 0; $i < count($product_list->products); $i++) {
//            $product_array = array();
//            $product_array['images_src'] = $images = $product_list->products[$i]->image == '' ? '' : $product_list->products[$i]->image->src;
//            $product_array['images'] = $product_list->products[$i]->image == '' ? '<div class="image-icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M19 0H1C.448 0 0 .448 0 1v18c0 .552.448 1 1 1h18c.552 0 1-.448 1-1V1c0-.552-.448-1-1-1zM8 6c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.654 0 3-1.346 3-3S9.654 4 8 4 5 5.346 5 7s1.346 3 3 3zm-6 8v-2.434l3.972-2.383 2.473 1.65c.398.264.925.21 1.262-.126l4.367-4.367L18 13.48V18H2zM18 2v8.92l-3.375-2.7c-.398-.32-.973-.287-1.332.073l-4.42 4.42-2.318-1.545c-.322-.214-.74-.225-1.07-.025L2 13.233V2h16z" fill="#c4cdd5" fill-rule="evenodd"></path></svg></div>' : '<img src="' . $product_list->products[$i]->image->src . '" alt="' . $product_list->products[$i]->title . '" class="Polaris-Thumbnail__Image">';
//            $product_array['image'] = $product_list->products[$i]->image == '' ? '' : $product_list->products[$i]->image->src;
//            $product_array['title'] = (strlen($product_list->products[$i]->title) > 40 ) ? substr($product_list->products[$i]->title, 0, 40) . '...' : $product_list->products[$i]->title;
//            $product_array['id'] = $product_list->products[$i]->id;
//            $product_array['variant_id'] = $product_list->products[$i]->variants[0]->id;
//            $product_array['handle'] = $product_list->products[$i]->handle;
//            $product_array['price'] = $product_list->products[$i]->variants[0]->price;
//            $product_array['type'] = isset($post['type']) ? $post['type'] : '';
//            $product_array['get_input_name'] = isset($post['get_input_name']) ? $post['get_input_name'] : '';
//            $product_data[] = $product_array;
//        }
//        echo json_encode(array('products' => $product_data, 'count' => $product_count, 'total_count' => $count->count));
    }
    
    public function get_graphql_api_list($api_main_url_arr = array(), $url_param_arr = array(), $method = 'POST', $is_object = 1, $shop_name = '') {
        $result = array("errors"=>SOMETHING_WENT_WRONG_MSG);
        /* Get shop info object */
        $shop = $this->getUserData($shop_name);
        /* Make api url */
        $api_main_url = SHOPIFY_API_VERSION .'graphql.json';
        /* call api for fetch api data */
        $request_headers = array("Content-Type: application/json; charset=utf-8", 'Expect:');
        $query_data = '';
        if(!empty($api_main_url_arr)){
            $path = ABS_PATH.'/graphql_request_template/'.$api_main_url_arr['data_file_name']; 
            if (file_exists($path)) {
                $query_data = file_get_contents($path); 
                $query_array = array("query"=>$query_data);
                
                if(!empty($url_param_arr)){
                    $query_array["variables"] = $url_param_arr;
                }
                $prepare_param_arr = json_encode($query_array);                                
                $api_data_list = shopify_call($shop->token, $shop->store_name, $api_main_url, $prepare_param_arr, $method, $request_headers);
                
                /* return api response */
                if ($is_object) {
                    $result = json_decode($api_data_list['response']);
                } else {
                    $result = json_decode($api_data_list['response'], TRUE);
                }
            }else{
                $result['errors'] = FILE_NOT_EXISTS_MSG;
            }
        }      
        return $result;
    }
}
