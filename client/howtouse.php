<?php include_once('header.php'); ?>
<div class="Polaris-Page">
    <div class="Polaris-Page__Header Polaris-Page__Header--hasBreadcrumbs Polaris-Page__Header--hasSecondaryActions Polaris-Page__Header--hasSeparator">
        <div class="Polaris-Page__MainContent">
            <div class="Polaris-Page__TitleAndActions">
                <div class="Polaris-Page__Title">
                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge">How to Use?</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="Polaris-Page__Content">
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 1 :</strong>
                <p>Screen: Dashboard</p>
                <p>Welcome to Active Cart! We have started you off with some example campaigns</p>
                <img src="../assets/img/step1.png">
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 2 :</strong> Title
                <p>Screen: Dashboard</p>
                <p>Copy; Name your campaign! This can help distinguish each of your campaigns and is for Internal Use Only!</p>
                <img src="../assets/img/step2.png">
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 3 :</strong> Type
                <p>Screen: Dashboard</p>
                <p>The type of campaign you will be running will show here</p>
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 4 :</strong> Status
                <p>Screen: Dashboard</p>
                <p>Is your campaign live? Distinguish between Published or Live campaigns and ones that are Unpublished or Not Live.</p>
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 5 :</strong> Actions
                <p>Screen: Dashboard</p>
                <p>Want to Publish or Unpublish your campaign? Use the eye icon called “Status”. Desire to make changes to your campaign? You can by clicking the “Edit” icon on the right. Let’s try it.</p>
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 6 :</strong> Offer Type
                <p>Screen: Campaign Screen</p>
                <p>Select Between what kind of offer you’d like to provide.</p>
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 7 :</strong> Details
                <p>Screen: Campaign Screen
                <p>Add in the appropriate details of your campaign. Get Creative, the options can be limitless!  TIP: Think from a customer point of view. What do they have to do in order to get the end result?</p>
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 8 :</strong> Dashboard
                <p>Screen: Campaign Screen</p>
                <p>Head back to the dashboard to see your beautiful list of money making campaigns</p>
            </div>
        </div>
        <div class="Polaris-Card">
            <div class="Polaris-Card__Section">
                <strong>Step 9 :</strong> Support/FAQ
                <p>Screen: Dashboard</p>
                <p>Have a question? Take a look into the Support/FAQ page first! We frequently update</p>
            </div>
        </div>
    </div>

    <div class="Polaris-Banner Polaris-Banner--statusInfo" tabindex="0" role="status" aria-live="polite" aria-describedby="Banner6Content" aria-labelledby="Banner6Heading">
        <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorTealDark Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><circle cx="10" cy="10" r="9" fill="currentColor"></circle><path d="M10 0C4.486 0 0 4.486 0 10s4.486 10 10 10 10-4.486 10-10S15.514 0 10 0m0 18c-4.411 0-8-3.589-8-8s3.589-8 8-8 8 3.589 8 8-3.589 8-8 8m1-5v-3a1 1 0 0 0-1-1H9a1 1 0 1 0 0 2v3a1 1 0 0 0 1 1h1a1 1 0 1 0 0-2m-1-5.9a1.1 1.1 0 1 0 0-2.2 1.1 1.1 0 0 0 0 2.2"></path></g></svg></span></div>
        <div>
            <div class="Polaris-Banner__Heading" id="Banner6Heading">
                <p class="Polaris-Heading">Need any other help?</p>
            </div>
            <div class="Polaris-Banner__Content" id="Banner6Content">
                <p>We are always here to help you. Please <a class="Polaris-Link" href="mailto:activecartapp@gmail.com" target="_top">email us</a>.</p>
            </div>
        </div>
    </div>

    <?php include_once('footer.php'); ?>