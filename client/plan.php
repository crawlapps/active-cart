<?php
include_once ('../include/config.php');
include_once '../shopifyapps/config.php';
/* include main client function file */
include_once ('functions.php');
$functions = new Client_functions();
if (isset($_GET['plan']) && $_GET['plan'] == 1) {
    $shop = $_GET['shop'];
    if($shop == "active-cart.myshopify.com"){
        $array = array("recurring_application_charge" => array('name' => SITE_NAME, 'price' => '8.99', 'return_url' => SITE_URL . 'client/accept_charge.php?shop=' . $shop,"trial_days" => "7","test" => true));
    }else{
        $array = array("recurring_application_charge" => array('name' => SITE_NAME, 'price' => '8.99', 'return_url' => SITE_URL . 'client/accept_charge.php?shop=' . $shop,"trial_days" => "7"));
    }
    $recurring_application_charge = $functions->recurring_application_charge($shop, $array);
    $redirect_url = $recurring_application_charge->recurring_application_charge->confirmation_url;
    header('Location: ' . $redirect_url . '');
}else if(isset($_GET['plan']) && $_GET['plan'] == 2){
    $shop = $_GET['shop'];
    $charge_deactivate = $functions->charge_deactivate($shop);
    header('Location: https://' . $shop . '/admin/apps/' . SHOPIFY_API_KEY.'?shop='.$shop);
    exit;
}
?>
