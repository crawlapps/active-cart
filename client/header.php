<?php
/* include main config file file */
include_once ('../include/config.php');

/* include main client function file */
include_once ('functions.php');

/* include shopifyapps config file */
include_once '../shopifyapps/config.php';

/* client function class variable */
$functions = new Client_functions();
$shop = $_REQUEST['shop'];
$current_user = $functions->getUserData($shop);
$offer_type1_cnt = $functions->get_user_offer1_cnt($current_user->store_id);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title><?php echo SITE_NAME; ?> | <?php echo $current_user->current_store; ?></title>
        <!-- CSS -->
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/custom_client.css" />
        <link href="../assets/css/bootstrap-tour.min.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/polaris.css" rel="stylesheet">
        <link rel="stylesheet" href="../assets/css/spectrum.css" rel="stylesheet">

        <!-- JS -->
        <script type="text/javascript">
            var shop = '<?php echo $shop; ?>';
            var tour_status = '<?php echo $current_user->tour_status; ?>';
            var charge_approve = '<?php echo $current_user->charge_approve; ?>';
        </script>
        <script src="../assets/js/jquery-2.1.1.js"></script>
        <script src="../assets/js/jquery-ui.min.js"></script>
        <script src="../assets/js/bootstrap.min.js"></script>
        <script src="../assets/js/spectrum.js"></script>
        <script src="../assets/js/bootstrap-tour.min.js"></script>
        <link rel="stylesheet" href="../assets/css/select2.min.css" />
        <script type="text/javascript" src="../assets/js/select2.full.min.js"></script>
        <?php include_once('../shopifyapps/redirect_charge.php'); ?>
        <script type="text/javascript">
            window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","resetIdentity","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
            heap.load("3906523294");
        </script>
    </head>
    <body>