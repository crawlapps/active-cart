<?php
include_once('header.php');
?>
<div class="Polaris-Page">

    <div class="Polaris-Page__Content">
        <div class="Polaris-Banner Polaris-Banner--statusCritical Polaris-Banner--withinPage" tabindex="0" role="alert" aria-live="polite" aria-labelledby="Banner11Heading" aria-describedby="Banner11Content" style="margin-bottom: 20px;">
            <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorRedDark Polaris-Icon--isColored Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20" focusable="false" aria-hidden="true"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M2 10c0-1.846.635-3.543 1.688-4.897l11.209 11.209A7.954 7.954 0 0 1 10 18c-4.411 0-8-3.589-8-8m14.312 4.897L5.103 3.688A7.954 7.954 0 0 1 10 2c4.411 0 8 3.589 8 8a7.952 7.952 0 0 1-1.688 4.897M0 10c0 5.514 4.486 10 10 10s10-4.486 10-10S15.514 0 10 0 0 4.486 0 10"></path></g></svg></span></div>
            <div>
                <div class="Polaris-Banner__Content" id="Banner11Content">
                    <p>Using a Drawer / Popup cart type of theme? Go to Themes > Customize Theme > Theme settings > Cart page and set cart type to Page (or ask your theme developer how to disable the popup cart). App work only in full cart page. Contact us on live support (icon in bottom right corner) or <a href="mailto:<?php echo SITE_ADMIN_EMAIL; ?>" target="_top">click here</a> to email us.</p>
                </div>
            </div>
        </div>
        <div class="Polaris-Banner Polaris-Banner--statusCritical error-show" style="display: none;" tabindex="0" role="alert" aria-live="polite" aria-describedby="Banner7Content" aria-labelledby="Banner7Heading">
            <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorRedDark Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M2 10c0-1.846.635-3.543 1.688-4.897l11.209 11.209A7.954 7.954 0 0 1 10 18c-4.411 0-8-3.589-8-8m14.312 4.897L5.103 3.688A7.954 7.954 0 0 1 10 2c4.411 0 8 3.589 8 8a7.952 7.952 0 0 1-1.688 4.897M0 10c0 5.514 4.486 10 10 10s10-4.486 10-10S15.514 0 10 0 0 4.486 0 10"></path></g></svg></span></div>
            <div>
                <div class="Polaris-Banner__Heading" id="Banner7Heading">
                    <p class="Polaris-Heading"></p>
                </div>
                <div class="Polaris-Banner__Content" id="Banner7Content">
                    <ul style="padding: 0 0 0 20px;">
                        <li>Title can't be blank</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <?php if ($current_user->charge_approve == 0) { ?>
                            <div class="Polaris-Card">
                                <div class="Polaris-Card__Section">
                                    <div class="Polaris-SettingAction">
                                        <div class="Polaris-SettingAction__Setting">
                                            <div class="Polaris-Stack">
                                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                    <div class="Polaris-AccountConnection__Content">
                                                        <div>Plan Information</div>
                                                        <div><span class="Polaris-TextStyle--variationSubdued">Free Plan</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-SettingAction__Action">
                                            <a target="_top" href="<?php echo SITE_CLIENT_URL; ?>plan.php?shop=<?php echo $shop; ?>&plan=1"  class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="redirect">Upgrade Pro Plan</span></span></a>
                                        </div>
                                    </div>
                                    <div class="Polaris-AccountConnection__TermsOfService">
                                        <p>By clicking <strong>Upgrade Pro Plan</strong>, you get all function of App's</p>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="Polaris-Card">
                                <div class="Polaris-Card__Section">
                                    <div class="Polaris-SettingAction">
                                        <div class="Polaris-SettingAction__Setting">
                                            <div class="Polaris-Stack">
                                                <div class="Polaris-Stack__Item Polaris-Stack__Item--fill">
                                                    <div class="Polaris-AccountConnection__Content">
                                                        <div>Plan Information</div>
                                                        <div><span class="Polaris-TextStyle--variationSubdued">Pro Plan</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-SettingAction__Action">
                                            <a target="_top" href="<?php echo SITE_CLIENT_URL; ?>plan.php?shop=<?php echo $shop; ?>&plan=2"  class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="redirect">Downgrade Plan</span></span></a>
                                        </div>
                                    </div>
                                    <div class="Polaris-AccountConnection__TermsOfService">
                                        <p>By clicking <strong>Downgrade Free Plan</strong>, you get limited function of App's</p>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="Polaris-Page__Header Polaris-Page__Header--hasBreadcrumbs">
                            <div class="Polaris-Page__MainContent">
                                <div class="Polaris-Page__TitleAndActions">
                                    <h1 class="Polaris-DisplayText Polaris-DisplayText--sizeLarge"><?php echo SITE_NAME; ?> List</h1>
                                </div>
                                <div class="Polaris-Page__PrimaryAction">
                                    <?php if ($current_user->charge_approve || $offer_type1_cnt=='0') { ?>
                                        <a class="Polaris-Button Polaris-Button--primary" href="special_offer.php?shop=<?php echo $shop; ?>"><span class="Polaris-Button__Content"><span class="redirect">Create <?php echo SITE_NAME; ?> Offer</span></span></a>
                                    <?php } else { ?>
                                        <button type="button" class="Polaris-Button Polaris-Button--primary" data-toggle="modal" data-target="#charge_modal"><span class="Polaris-Button__Content"><span>Create <?php echo SITE_NAME; ?> Offer</span></span></button>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="Polaris-Page__Content">
                            <div class="Polaris-Card" style="overflow: visible;">
                                <div class="Polaris-Card__Section table-result" style="overflow: visible;">
                                    <table class="results table-responsive">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th style="width: 350px;">Type</th>
                                                <th>Status</th>
                                                <th style="width:220px;">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="view-offers">
                                            <tr class="Polaris-ResourceList__ItemWrapper">
                                                <td class="load-offers" colspan="4"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="pagination" style="display: none;">
                                    <div class="Polaris-Page__Pagination">
                                        <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                                            <div id="pagination-offers" class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">

                                            </div>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shopify modal inmodal" id="charge_modal" data-backdrop="false" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" class="shopify-close-btn close close-product"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-4.293 4.293-4.293-4.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l4.293 4.293-4.293 4.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l4.293-4.293 4.293 4.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-4.293-4.293z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                    <h4 class="modal-title">Upgrade Pro Plan </h4>
                </div>
                <div class="modal-body">
                    <div class="Polaris-Card">
                        <div class="Polaris-Card__Section">
                            <div class="Polaris-FormLayout">
                                <div role="group">
                                    <div class="Polaris-FormLayout__Item">
                                        <p>Do you want to run more than one campaign? Click <strong>Yes</strong> Upgrade to Pro Today</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer pagination">
                    <div class="Polaris-Page__Pagination">
                        <div class="Polaris-ButtonGroup" style="float: right;">
                            <input type="hidden" id="select_product_buy" name="select_product_buy">
                            <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button" data-dismiss="modal"><span class="Polaris-Button__Content"><span>Cancel</span></span></button></div>
                            <div class="Polaris-ButtonGroup__Item">
                                <a target="_top" href="<?php echo SITE_CLIENT_URL; ?>plan.php?shop=<?php echo $shop; ?>&plan=1" class="Polaris-Button Polaris-Button--primary"><span class="Polaris-Button__Content"><span class="redirect">Yes</span></span></a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>
    <script>
        get_offer_load('1');
        function startTour() {
            var tour = new Tour({
                storage: false,
                backdrop: true,
                template: "<div class='popover tour'>\n\
                            <div class='arrow'></div>\n\
                            <h3 class='popover-title'></h3>\n\
                            <div class='popover-content'></div>\n\
                            <div class='popover-navigation'>\n\
                                <button class='btn btn-default' data-role='prev'>« Prev</button>\n\
                                <button class='btn btn-default' data-role='next'>Next »</button>\n\
                                </div></div>",
                onEnd: function () {
                    //document.cookie = "edit=1";
                    setCookie('edit', '1', '1');
                },
                steps: [{
                        element: ".table-result",
                        title: "Dashboard",
                        content: "Welcome to Active Cart! We have started you off with some example campaigns",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0,
                        onNext: function () {

                        }
                    }, {
                        element: ".table-responsive tr th:nth-child(1)",
                        title: "Title",
                        content: "Name your campaign! This can help distinguish each of your campaigns and is for Internal Use Only!",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }, {
                        element: ".table-responsive tr th:nth-child(2)",
                        title: "Type",
                        content: "The type of campaign you will be running will show here",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }, {
                        element: ".table-responsive tr th:nth-child(3)",
                        title: "Status",
                        content: "Is your campaign live? Distinguish between Published or Live campaigns and ones that are Unpublished or Not Live.",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }, {
                        element: ".table-responsive tr th:nth-child(4)",
                        title: "Action",
                        content: "Want to Publish or Unpublish your campaign? Use the eye icon called “Status”. Desire to make changes to your campaign? You can by clicking the “Edit” icon on the right. Let’s try it.",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }, {
                        element: ".table-responsive .first_offer0 td:last-child",
                        title: "Action",
                        content: "Want to Publish or Unpublish your campaign? Use the eye icon called “Status”. Desire to make changes to your campaign? You can by clicking the “Edit” icon on the right. Let’s try it.",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0,
                        onNext: function () {
                            //document.cookie = "edit=1";
                            setCookie('edit', '1', '1');
                            var hrefValue = $('.first_offer0 .Editbutton a').attr('href');
                            window.location.href = hrefValue;
                            return (new $.Deferred()).promise();
                        }
                    }, {
                        element: ".table-responsive .first_offer1",
                        title: "Action",
                        content: "",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0,
                        onNext: function () {

                        }
                    }
                ]});
            // Initialize the tour
            tour.init();
            tour.restart();
        }
        if (tour_status == '0' && getCookie('dashboard') == '1') {
            setTimeout(function () {
                startSecondTour();
            }, 100);
        }
        function startSecondTour() {
            var SecondTour = new Tour({
                storage: false,
                backdrop: true,
                template: "<div class='popover tour'>\n\
                                <div class='arrow'></div>\n\
                                <h3 class='popover-title'></h3>\n\
                                <div class='popover-content'></div>\n\
                                <div class='popover-navigation'>\n\
                                    <button class='btn btn-default' data-role='prev'>« Prev</button>\n\
                                    <button class='btn btn-default' data-role='next'>Next »</button>\n\
                                    <button class='btn btn-default' data-role='end' on-end-end='end(tour)'>End tour</button></div></div>",
                onEnd: function () {
                    $.ajax({
                        url: "ajax_responce.php",
                        type: "post",
                        data: {'tour_status': '1', 'shop': shop},
                        success: function (data) {
                            setCookie('dashboard','','-1');
                        }
                    });
                },
                steps: [{
                        element: ".first_offer0 .Statusbutton",
                        title: "Campaign status",
                        content: "Change Campaign status Published/Unpublished.",
                        placement: "bottom",
                        backdropContainer: 'body',
                        backdropPadding: 0,
                        onPrev: function () {
                            return (new $.Deferred()).promise();
                        }
                    }, {
                        element: ".first_offer0 .delete",
                        title: "Campaign delete",
                        content: "Campaign deleted permanently.",
                        placement: "bottom",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }
                ]});
            // Initialize the tour
            SecondTour.init();
            SecondTour.restart();
        }
    </script>