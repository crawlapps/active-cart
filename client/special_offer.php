<?php
include_once('header.php');
$title = $buyx = $percentage = $offer_id = "";
$offer_product_ids = $offer_product_variant_ids = $offer_product_price_ids = array();
$type = 0;
if (isset($_GET['id']) && $_GET['id'] != "") {
    $offer_details = $functions->get_offer($shop, $_GET['id']);
    if (isset($offer_details) && $offer_details->num_rows > 0) {
        $offer_detail = $offer_details->fetch_object();
        //print_r($offer_detail);  
        $title = $offer_detail->title;
        $type = $offer_detail->type;
        $buyx = $offer_detail->buyx;
        //$buyx  = $offer_detail->buyx;
        $percentage = $offer_detail->at;
        $offer_id = $offer_detail->id;
        $where = 'WHERE offer_id = "' . $offer_id . '"';
        $offer_products = $functions->select(TABLE_GETY_PRODUCTS, $where);
        if (isset($offer_products) && $offer_products->num_rows > 0) {
            while ($offer_product = $offer_products->fetch_object()) {
                $offer_product_ids[] = $offer_product->product_id;
                $offer_product_variant_ids[] = $offer_product->variant_id;
                $offer_product_price_ids[] = $offer_product->product_price;
            }
        }
        if (!empty($offer_product_ids)) {
            $offer_product_ids = implode(',', $offer_product_ids);
            $offer_product_variant_ids = implode(',', $offer_product_variant_ids);
            $offer_product_price_ids = implode(',', $offer_product_price_ids);
        }
    }
}
$return_settings_arr=$functions->get_settings($shop);
?>
<div class="Polaris-Page special_offer-page">
    <div class="Polaris-Page__Content">
        <div class="Polaris-Banner Polaris-Banner--statusCritical error-show" style="display: none;" tabindex="0" role="alert" aria-live="polite" aria-describedby="Banner7Content" aria-labelledby="Banner7Heading">
            <div class="Polaris-Banner__Ribbon"><span class="Polaris-Icon Polaris-Icon--colorRedDark Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><circle fill="currentColor" cx="10" cy="10" r="9"></circle><path d="M2 10c0-1.846.635-3.543 1.688-4.897l11.209 11.209A7.954 7.954 0 0 1 10 18c-4.411 0-8-3.589-8-8m14.312 4.897L5.103 3.688A7.954 7.954 0 0 1 10 2c4.411 0 8 3.589 8 8a7.952 7.952 0 0 1-1.688 4.897M0 10c0 5.514 4.486 10 10 10s10-4.486 10-10S15.514 0 10 0 0 4.486 0 10"></path></g></svg></span></div>
            <div>
                <div class="Polaris-Banner__Heading" id="Banner7Heading">
                    <p class="Polaris-Heading"></p>
                </div>
                <div class="Polaris-Banner__Content" id="Banner7Content">
                    <ul style="padding: 0 0 0 20px;">
                        <li>Title can't be blank</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="Polaris-Layout">
            <div class="Polaris-Layout__AnnotatedSection">
                <div class="Polaris-Layout__AnnotationWrapper">
                    <div class="Polaris-Layout__AnnotationContent">
                        <div class="Polaris-Page__Content">
                            <form name="addOffer" id="addOffer" action="index.php" method="post">
                                <div class="Polaris-Card">
                                    <div class="Polaris-Card__Section">
                                        <div class="Polaris-FormLayout">
                                            <div role="group" class="Polaris-FormLayout--condensed">
                                                <div class="Polaris-FormLayout__Items">
                                                    <div class="Polaris-FormLayout__Item ">
                                                        <div class="title-tour">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label id="TextField1Label" for="TextField1" class="Polaris-Label__Text">Title of Your Campaign ( Internal use only )</label></div>
                                                            </div>
                                                            <div class="Polaris-TextField"><input placeholder="Please enter title" id="title" name="title" class="Polaris-TextField__Input" value="<?php echo $title; ?>">
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-FormLayout__Item">
                                                        <div class="type-tour">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label id="TextField1Label" for="TextField1" class="Polaris-Label__Text">Offer type</label></div>
                                                            </div>
                                                            <div class="Polaris-Select">
                                                                <select name="type" id="type" class="Polaris-Select__Input" aria-invalid="false" >
                                                                    <option value="1" <?php echo ($type == 1 || $type == 0) ? 'selected' : ''; ?>>Buy X Get Y</option>
                                                                    <option value="2" <?php echo ($type == 2) ? 'selected' : ''; ?> <?php echo ($current_user->charge_approve) ? '' : 'disabled="disabled"'; ?>>Spend X amount Get Y Free</option>
                                                                </select>
                                                                <div class="Polaris-Select__Icon"><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M13 8l-3-3-3 3h6zm-.1 4L10 14.9 7.1 12h5.8z" fill-rule="evenodd"></path></svg></span></div>
                                                                <div class="Polaris-Select__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--start buy x get y-->
                                <div id="buy_x" class="Polaris-Card" style="display: <?php echo ($type == 1 || $type == 0) ? 'block' : 'none'; ?>">
                                    <div class="Polaris-Card__Header">
                                        <h2 class="Polaris-Heading">Details</h2>
                                    </div>
                                    <div class="Polaris-Card__Section">
                                        <div class="Polaris-FormLayout">
                                            <div class="Polaris-FormLayout__Item">
                                                <div class="buy-tour">
                                                    <div class="Polaris-Labelled__LabelWrapper">
                                                        <div class="Polaris-Label"><label class="Polaris-Label__Text">Buy</label></div>
                                                    </div> 
                                                    <div class="Polaris-Connected">
                                                        <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                            <div class="Polaris-TextField">
                                                                <div class="Polaris-TextField__Prefix" id="TextField-Browse-product-upsell"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                <input type="text" id="buy_of_product_select2" name="buy_of_product" class="Polaris-TextField__Input browse_buy_product_search" data-type="buy" aria-invalid="false" placeholder="Search products" autocomplete="off">
                                                                <div class="Polaris-TextField__Backdrop"></div>
                                                            </div>
                                                        </div>
                                                        <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                            <div class="Polaris-Labelled--hidden">
                                                                <button type="button" class="Polaris-Button"  data-toggle="modal" data-target="#product_buy_modal"><span class="Polaris-Button__Content"><span>Browse</span></span></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                                </div>
                                            </div>
                                            <div class="Polaris-FormLayout__Item">
                                                <ul id="ul-selected-product-list-buy" class="Polaris-ResourceList">
                                                </ul>
                                            </div>
                                            <div role="group" class="Polaris-FormLayout--condensed">
                                                <div class="Polaris-FormLayout__Items">
                                                    <div class="Polaris-FormLayout__Item">
                                                        <div class="get-tour">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label class="Polaris-Label__Text">Get</label></div>
                                                            </div> 
                                                            <div class="Polaris-Connected">
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                    <div class="Polaris-TextField"><div class="Polaris-TextField__Prefix" id="TextField-Browse-product-upsell"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                        <input  multiple="multiple" type="text" id="get_of_product_select2" name ="" value="" class="Polaris-TextField__Input browse_get_product_search" data-type="get" aria-invalid="false" placeholder="Search products" autocomplete="off">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                    <div class="Polaris-Labelled--hidden">
                                                                        <button type="button" class="Polaris-Button" data-toggle="modal" data-target="#product_modal"><span class="Polaris-Button__Content"><span>Browse</span></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-FormLayout__Item">
                                                        <div class="at-tour">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label class="Polaris-Label__Text">At</label></div>
                                                            </div>
                                                            <div class="Polaris-Connected">
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--focused Polaris-Connected__Item--primary">
                                                                    <div class="Polaris-TextField Polaris-TextField--hasValue"><input placeholder="Please enter percentage" type="number" id="at" name="at" value="<?php echo ($type == 1) ? $percentage : ''; ?>" class="Polaris-TextField__Input">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                    <div class="Polaris-Labelled--hidden">
                                                                        <button type="button" class="Polaris-Button browse-product-upsell"><span class="Polaris-Button__Content"><span>% Off</span></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="Polaris-Labelled__HelpText">100 means free</div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-FormLayout__Item">
                                                    <ul id="ul-selected-product-list-get" class="Polaris-ResourceList">
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--start buy x get y-->

                                <!--start spend amount get y-->
                                <div id="get_y" class="Polaris-Card" style="display: <?php echo ($type == 2) ? 'block' : 'none'; ?>">
                                    <div class="Polaris-Card__Header">
                                        <h2 class="Polaris-Heading">Details</h2>
                                    </div>
                                    <div class="Polaris-Card__Section">
                                        <div class="Polaris-FormLayout">
                                            <div role="group" class="Polaris-FormLayout--condensed">
                                                <div class="Polaris-FormLayout__Items">
                                                    <div class="Polaris-FormLayout__Item" style="margin-top: -0.8rem;">
                                                        <div class="">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label id="TextField6Label" for="TextField6" class="Polaris-Label__Text">Spend</label></div>
                                                            </div>
                                                            <div class="Polaris-Connected">
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--focused Polaris-Connected__Item--primary">
                                                                    <div class="Polaris-TextField Polaris-TextField--hasValue"><input placeholder="Please enter amount" type="number" id="spend" name="spend" value="<?php echo ($type == 2) ? $buyx : ''; ?>" " class="Polaris-TextField__Input">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                    <div class="Polaris-Labelled--hidden">
                                                                        <div class="Polaris-Labelled--hidden">
                                                                            <button type="button" class="Polaris-Button browse-product-upsell"><span class="Polaris-Button__Content"><span>$</span></span></button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-FormLayout__Item" style="display: none">
                                                        <div class="">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label class="Polaris-Label__Text">Get y</label></div>
                                                            </div>
                                                            <div class="Polaris-Connected">
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--focused Polaris-Connected__Item--primary">
                                                                    <div class="Polaris-TextField Polaris-TextField--hasValue"><input name="gety" placeholder="Please enter percentage" type="number" id="gety" value="100" class="Polaris-TextField__Input">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                    <div class="Polaris-Labelled--hidden">
                                                                        <button type="button" class="Polaris-Button browse-product-upsell"><span class="Polaris-Button__Content"><span>% Off</span></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="Polaris-FormLayout__Item">
                                                        <div class="">
                                                            <div class="Polaris-Labelled__LabelWrapper">
                                                                <div class="Polaris-Label"><label class="Polaris-Label__Text">Get</label></div>
                                                            </div> 
                                                            <div class="Polaris-Connected">
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--primary">
                                                                    <div class="Polaris-TextField"><div class="Polaris-TextField__Prefix" id="TextField-Browse-product-upsell"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                        <input type="text" name="on_of_product_select2" id="on_of_product_select2" value="" class="Polaris-TextField__Input"  aria-invalid="false" placeholder="Search products" autocomplete="off">
                                                                        <div class="Polaris-TextField__Backdrop"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="Polaris-Connected__Item Polaris-Connected__Item--connection">
                                                                    <div class="Polaris-Labelled--hidden">
                                                                        <button type="button" data-toggle="modal" data-target="#product_on_modal" class="Polaris-Button browse-product-upsell"><span class="Polaris-Button__Content"><span>Browse</span></span></button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="Polaris-FormLayout__Item">
                                                <ul id="ul-selected-product-list-on" class="Polaris-ResourceList"></ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="Polaris-Card" style="border-top: 1px solid #dfe3e8;">
                                        <div class="Polaris-Card__Header">
                                            <h2 class="Polaris-Heading">Global Spend X Get Y Setting</h2>
                                        </div>
                                        <div class="Polaris-Card__Section">
                                            <div class="Polaris-FormLayout">
                                                <div role="group" class="Polaris-FormLayout--condensed">
                                                    <div class="Polaris-FormLayout__Items">
                                                        <div class="Polaris-FormLayout__Item">
                                                            <div style="--top-bar-background:#00848e; --top-bar-color:#f9fafb; --top-bar-background-lighter:#1d9ba4;"><label class="Polaris-Choice" for="is_show_all_offer"><span class="Polaris-Choice__Control"><span class="Polaris-Checkbox"><input id="is_show_all_offer" <?php echo ($return_settings_arr['is_show_all_offer'] == 1)?'checked="checked"':'';?>  type="checkbox" name="is_show_all_offer" class="Polaris-Checkbox__Input" aria-invalid="false" role="checkbox" aria-checked="false" value=""><span class="Polaris-Checkbox__Backdrop"></span><span class="Polaris-Checkbox__Icon"><span class="Polaris-Icon"><svg viewBox="0 0 20 20" class="Polaris-Icon__Svg" focusable="false" aria-hidden="true">
                                                                                    <path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path>
                                                                                    </svg></span></span></span></span><span class="Polaris-Choice__Label">When creating a "tiered" campaign, only show the last spend campaign. This means one campaign will be rewarded to your customer depending on total added to cart instead of all Spend X Get Y campaigns.  (Recommend)</span></label></div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="Polaris-PageActions">
                                    <div class="Polaris-Stack Polaris-Stack--spacingTight Polaris-Stack--distributionEqualSpacing">
                                        <div class="Polaris-Stack__Item">
                                            <div class="Polaris-ButtonGroup">
                                                <div class="Polaris-ButtonGroup__Item">
                                                    <a href="<?php echo SITE_CLIENT_URL; ?>?shop=<?php echo $shop; ?>" class="Polaris-Button"><span class="Polaris-Button__Content"><span>Cancel</span></span></a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="Polaris-Stack__Item save-tour">
                                            <?php 
                                            $is_btn_disabled = $btn_disabled_class = '';
                                            if(($current_user->charge_approve == '0' && $offer_type1_cnt > 0 && !isset($_GET['id'])) || (isset($_GET['id']) && $current_user->charge_approve == '0' && $type != 1) ){
                                                $is_btn_disabled = 'disabled=""';
                                                $btn_disabled_class = 'Polaris-Button--disabled';
                                            }
                                            
                                            ?>
                                            <button type="submit" class="Polaris-Button Polaris-Button--primary offer_save <?php echo $btn_disabled_class; ?>" <?php echo $is_btn_disabled; ?>>
                                                <span class="Polaris-Button__Content"><span>Save</span></span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="get_of_product_selected" id="get_of_product_selected" value="<?php echo($type == 1 && !empty($offer_product_ids)) ? $offer_product_ids : ''; ?>">
                                <input type="hidden" name="get_of_product_variant_selected" id="get_of_product_variant_selected" value="<?php echo($type == 1 && !empty($offer_product_variant_ids)) ? $offer_product_variant_ids : ''; ?>">
                                <input type="hidden" name="get_of_product_price_selected" id="get_of_product_price_selected" value="<?php echo($type == 1 && !empty($offer_product_price_ids)) ? $offer_product_price_ids : ''; ?>">
                                <input type="hidden" name="buy_of_product_selected" id="buy_of_product_selected" value="<?php echo($type == 1 && !empty($buyx)) ? $buyx : ''; ?>">
                                <input type="hidden" name="buy_of_product_variant_selected" id="buy_of_product_variant_selected" value="<?php echo($type == 1 && !empty($buyx)) ? $buyx : ''; ?>">
                                <input type="hidden" name="on_of_product_selected" id="on_of_product_selected" value="<?php echo($type == 2 && !empty($offer_product_ids)) ? $offer_product_ids : ''; ?>">
                                <input type="hidden" name="on_of_product_variant_selected" id="on_of_product_variant_selected" value="<?php echo($type == 2 && !empty($offer_product_variant_ids)) ? $offer_product_variant_ids : ''; ?>">
                                <input type="hidden" name="on_of_product_price_selected" id="on_of_product_price_selected" value="<?php echo($type == 2 && !empty($offer_product_price_ids)) ? $offer_product_price_ids : ''; ?>">
                                <input type="hidden" name="offer_id" id="offer_id" value="<?php echo $offer_id; ?>">
                                <!--end spend amount get y-->
                            </form>

                            <div class="shopify modal inmodal" id="product_buy_modal" data-backdrop="false" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" class="shopify-close-btn close close-product"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-4.293 4.293-4.293-4.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l4.293 4.293-4.293 4.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l4.293-4.293 4.293 4.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-4.293-4.293z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                                            <h4 class="modal-title">Add Buy Prodcuts </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="Polaris-Card">
                                                <div class="Polaris-Card__Section">
                                                    <div class="Polaris-FormLayout">
                                                        <div role="group" class="">
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-TextField"><div class="Polaris-TextField__Prefix" id="TextField-Browse-collection"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                    <input type="text" id="add_buy_products" name="add_buy_products" class="Polaris-TextField__Input" aria-invalid="false"  onkeyup="product_records(this.value, 1, 'buy', '', 'after', 'products')" placeholder="Search products" autocomplete="off">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                                <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Card__Section Polaris-Card__Section-list">
                                                    <div class="product-loader loader"></div>
                                                    <form method="post" id="product-data">
                                                        <ul id="ul-buy-list" class="Polaris-ResourceList">
                                                        </ul>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer pagination">
                                            <div class="Polaris-Page__Pagination">
                                                <div>
                                                    <div style="float: left;">
                                                        <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                                                            <div id="modal_pagination_buy" class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            </div>
                                                    </div>
                                                    </nav>
                                                    <div class="Polaris-ButtonGroup" style="float: right;">
                                                        <input type="hidden" id="select_product_buy" name="select_product_buy">
                                                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button close-product"><span class="Polaris-Button__Content"><span>Cancel</span></span></button></div>
                                                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--primary Polaris-Button--disabled add_products add_products_buy" data-type="buy"><span class="Polaris-Button__Content"><span>Add</span></span></button></div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="shopify modal inmodal" id="product_modal" data-backdrop="false" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" class="shopify-close-btn close close-product"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-4.293 4.293-4.293-4.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l4.293 4.293-4.293 4.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l4.293-4.293 4.293 4.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-4.293-4.293z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                                            <h4 class="modal-title">Add Get Of Prodcuts </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="Polaris-Card">
                                                <div class="Polaris-Card__Section">
                                                    <div class="Polaris-FormLayout">
                                                        <div role="group" class="">
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-TextField"><div class="Polaris-TextField__Prefix" id="TextField-Browse-collection"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                    <input type="text" id="add_get_products" name="add_get_products" class="Polaris-TextField__Input" aria-invalid="false"  onkeyup="product_records(this.value, 1, 'get', '', 'after', 'products')" placeholder="Search products" autocomplete="off">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                                <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Card__Section Polaris-Card__Section-list">
                                                    <div class="product-loader loader"></div>
                                                    <form method="post" id="product-data">
                                                        <ul id="ul-get-list" class="Polaris-ResourceList">
                                                        </ul>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer pagination">
                                            <div class="Polaris-Page__Pagination">
                                                <div>
                                                    <div style="float: left;">
                                                        <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                                                            <div id="modal_pagination_get" class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            </div>
                                                    </div>
                                                    </nav>
                                                    <div class="Polaris-ButtonGroup" style="float: right;">
                                                        <input type="hidden" id="select_product_get" name="select_product_get">
                                                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button close-product"><span class="Polaris-Button__Content"><span>Cancel</span></span></button></div>
                                                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--primary Polaris-Button--disabled add_products add_products_get" data-type="get"><span class="Polaris-Button__Content"><span>Add</span></span></button></div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="shopify modal inmodal" id="product_on_modal" data-backdrop="false" tabindex="-1" role="dialog"  aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" class="shopify-close-btn close close-product"><span class="remove-icon Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293c.391-.391.391-1.023 0-1.414s-1.023-.391-1.414 0l-4.293 4.293-4.293-4.293c-.391-.391-1.023-.391-1.414 0s-.391 1.023 0 1.414l4.293 4.293-4.293 4.293c-.391.391-.391 1.023 0 1.414.195.195.451.293.707.293.256 0 .512-.098.707-.293l4.293-4.293 4.293 4.293c.195.195.451.293.707.293.256 0 .512-.098.707-.293.391-.391.391-1.023 0-1.414l-4.293-4.293z" fill="#637381" fill-rule="evenodd"></path></svg></span></button>
                                            <h4 class="modal-title">Add Get Of Prodcuts </h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="Polaris-Card">
                                                <div class="Polaris-Card__Section">
                                                    <div class="Polaris-FormLayout">
                                                        <div role="group" class="">
                                                            <div class="Polaris-FormLayout__Item">
                                                                <div class="Polaris-TextField"><div class="Polaris-TextField__Prefix" id="TextField-Browse-collection"><span class="Polaris-Icon Polaris-Icon--hasBackdrop" aria-label=""><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M8 12c-2.21 0-4-1.79-4-4s1.79-4 4-4 4 1.79 4 4-1.79 4-4 4zm9.707 4.293l-4.82-4.82C13.585 10.493 14 9.296 14 8c0-3.313-2.687-6-6-6S2 4.687 2 8s2.687 6 6 6c1.296 0 2.492-.415 3.473-1.113l4.82 4.82c.195.195.45.293.707.293s.512-.098.707-.293c.39-.39.39-1.023 0-1.414z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span></div>
                                                                    <input type="text" id="add_on_products" name="add_on_products" class="Polaris-TextField__Input" aria-invalid="false"  onkeyup="product_records(this.value, 1, 'on', '', 'after', 'products')" placeholder="Search products" autocomplete="off">
                                                                    <div class="Polaris-TextField__Backdrop"></div>
                                                                </div>
                                                                <div class="Polaris-Labelled__HelpText">Type at least 3 characters</div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="Polaris-Card__Section Polaris-Card__Section-list">
                                                    <div class="product-loader loader"></div>
                                                    <form method="post" id="product-data">
                                                        <ul id="ul-on-list" class="Polaris-ResourceList">
                                                        </ul>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer pagination">
                                            <div class="Polaris-Page__Pagination">
                                                <div>
                                                    <div style="float: left;">
                                                        <nav class="Polaris-Pagination Polaris-Pagination--plain" aria-label="Pagination">
                                                            <div id="modal_pagination_on" class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented">
                                                            </div>
                                                    </div>
                                                    </nav>
                                                    <div class="Polaris-ButtonGroup" style="float: right;">
                                                        <input type="hidden" id="select_product_on" name="select_product_on">
                                                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button close-product"><span class="Polaris-Button__Content"><span>Cancel</span></span></button></div>
                                                        <div class="Polaris-ButtonGroup__Item"><button type="button" class="Polaris-Button Polaris-Button--primary Polaris-Button--disabled add_products add_products_on" data-type="on"><span class="Polaris-Button__Content"><span>Add</span></span></button></div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include_once('footer.php'); ?>
    <?php if ($type == 1 && $buyx != "") { ?>
        <script>
            load_selected_product('buy', '<?php echo $buyx; ?>');
            load_selected_product('get', '<?php echo $offer_product_ids; ?>');
        </script>
    <?php } elseif ($type == 2 && !empty($offer_product_ids)) {
        ?>
        <script>
            load_selected_product('on', '<?php echo $offer_product_ids; ?>');
        </script>
        <?php
    }
    ?>
    <script>
        product_records('', 1, 'buy', '', '', 'products');
        product_records('', 1, 'get', '', '', 'products');
        product_records('', 1, 'on', '', '', 'products');
    </script>
    <?php if ($current_user->tour_status == 0) { ?>
        <script>
            //StartTourSpecial();
        </script>
    <?php } ?>
    <script>
        if (tour_status == 0 || getCookie('edit') == 1) {
            StartTourSpecial();
        }
        function StartTourSpecial() {
            var tour = new Tour({
                storage: false,
                backdrop: true,
                template: "<div class='popover tour'>\n\
                                <div class='arrow'></div>\n\
                                <h3 class='popover-title'></h3>\n\
                                <div class='popover-content'></div>\n\
                                <div class='popover-navigation'>\n\
                                    <button class='btn btn-default' data-role='prev'>« Prev</button>\n\
                                    <button class='btn btn-default' data-role='next'>Next »</button>\n\
                                    <button class='btn btn-default' data-role='end' on-end-end='end(tour)'>End tour</button></div></div>",
                onEnd: function () {
                    $.ajax({
                        url: "ajax_responce.php",
                        type: "post",
                        data: {'tour_status': '1', 'shop': shop},
                        success: function (data) {
                            setCookie('edit','','-1');
                            //document.cookie = "dashboard=1";
                            setCookie('dashboard', '1', '1');
                        }
                    });
                },
                steps: [{
                        element: ".type-tour",
                        title: "Offer Type",
                        content: "Select Between what kind of offer you'd like to provide.",
                        placement: "bottom",
                        backdropContainer: 'body',
                        backdropPadding: 0,
                        onPrev: function () {
                            window.location.href = 'index.php?shop=<?php echo $shop; ?>';
                            return (new $.Deferred()).promise();
                        }
                    }, {
                        element: "#buy_x",
                        title: "Details",
                        content: "Add in the appropriate details of your campaign. Get Creative, the options can be limitless!  TIP: Think from a customer point of view. What do they have to do in order to get the end result?",
                        placement: "bottom",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }, {
                        element: ".save-tour",
                        title: "Save",
                        content: "Save your Campaign.",
                        placement: "top",
                        backdropContainer: 'body',
                        backdropPadding: 0,
                        onNext: function () {
                            setCookie('edit','','-1');
                            //document.cookie = "dashboard=1";
                            setCookie('dashboard', '1', '1');
                            window.location.href = 'index.php?shop=' + shop;
                            return (new $.Deferred()).promise();
                        },
                        onPrev: function () {
                            setCookie('edit','','-1');
                            //document.cookie = "dashboard=1";
                            setCookie('dashboard', '1', '1');
                        }
                    }, {
                        element: ".first_offer1 .Editbutton",
                        title: "Edit campaign",
                        content: "Edit the appropriate details of your campaign with what kind of offer you'd like to provide.",
                        placement: "bottom",
                        backdropContainer: 'body',
                        backdropPadding: 0
                    }
                ]});
            // Initialize the tour
            tour.init();
            tour.restart();
        }
    </script>
