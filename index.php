<?php

include_once 'include/config.php';
include_once 'include/en.php';
include_once 'include/functions.php';
require_once('include/Registration.php');
require 'shopifyapps/config.php';
require 'shopifyapps/shopify.php';
include "shopifyapps/shopify_call.php";


if ($_REQUEST['shop'] != "") {
    $db_connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
    if (mysqli_connect_errno()) {
        echo "Failed : connect to MySQL: " . mysqli_connect_error();
        die;
    }
    if (isset($_GET['code'])) {
        $shopifyClient = new ShopifyClient($_GET['shop'], "", SHOPIFY_API_KEY, SHOPIFY_SECRET);
        $token = $shopifyClient->getAccessToken($_GET['code']);
        $shop = $_GET['shop'];
        $store_row = $db_connection->query("SELECT * FROM " . TABLE_CLIENT_STORES . " WHERE store_name = '$shop' and status='1'");
        if (isset($store_row) && $store_row->num_rows > 0) {
            header('Location: ' . SITE_CLIENT_URL . '?shop=' . $shop);
        } else {
            $shopuinfo = shopify_call($token, $shop, SHOPIFY_API_VERSION."shop.json", array(), 'GET');
            /* Get response */
            $shopuinfo = json_decode($shopuinfo['response']);
            /* register uninstall call back url */

            /* Register hook */
            /* webhook for uninstall app */
            $general_function = new general_function();

            $path = SHOPIFY_API_VERSION.'webhooks.json';
            /* If your shopify app is public */
            $password = md5(SHOPIFY_SECRET . $token);
            $baseurl = "https://" . SHOPIFY_API_KEY . ":" . $password . "@" . $shop . "/";
            $url = $baseurl . ltrim($path, '/');

            $params = '{"webhook": {"topic":"app/uninstalled",
                                "address":"' . SITE_PATH . '/webhook/uninstall.php?store=' . $shop . '&inapp=1",
                                "format":"json"
				}}';
            $responce = $general_function->register_webhook($url, $params, $token);

            /* Product update */
            $params = '{"webhook": {"topic":"products/update",
                                "address":"' . SITE_PATH . '/webhook/product-update.php?store=' . $shop . '&inapp=1",
                                "format":"json"
				}}';
            $responce = $general_function->register_webhook($url, $params, $token);
            /* Product delete */
            $params = '{"webhook": {"topic":"products/delete",
                                "address":"' . SITE_PATH . '/webhook/product-delete.php?store=' . $shop . '&inapp=1",
                                "format":"json"
				}}';
            $responce = $general_function->register_webhook($url, $params, $token);

            /* Shop Update */
            $params = '{"webhook": {"topic":"shop/update",
                                "address":"' . SITE_PATH . '/webhook/shop-update.php?store=' . $shop . '&inapp=1",
                                "format":"json"
				}}';
            $responce = $general_function->register_webhook($url, $params, $token);

            /* Themes Publish */
            $params = '{"webhook": {"topic":"themes/publish",
                                "address":"' . SITE_PATH . '/webhook/themes-publish.php?store=' . $shop . '&inapp=1",
                                "format":"json"
				}}';
            $responce = $general_function->register_webhook($url, $params, $token);

            /* so this single line handles the entire registration process. */
            $registration = new Registration();
            $shop_details = array();
            $shop_details = array(
                'user_email' => $shopuinfo->shop->email,
                'shop' => $shop,
                'token' => $token,
                'shop_plan' => $shopuinfo->shop->plan_name,
                'shop_owner' => $shopuinfo->shop->shop_owner,
                'address1' => $shopuinfo->shop->address1,
                'address2' => $shopuinfo->shop->address2,
                'city' => $shopuinfo->shop->city,
                'country_name' => $shopuinfo->shop->country_name,
                'currency' => $shopuinfo->shop->currency,
                'money_format' => htmlspecialchars(strip_tags($shopuinfo->shop->money_format), ENT_QUOTES, "ISO-8859-1"),
                'name' => htmlspecialchars($shopuinfo->shop->name, ENT_QUOTES, "ISO-8859-1"),
                'phone' => $shopuinfo->shop->phone,
                'province' => $shopuinfo->shop->province,
                'zip' => $shopuinfo->shop->zip,
                'app_name' => 'Shopify App ' . SITE_NAME
            );
            $result = $registration->registerNewUserShopify($shop_details);
            $theme_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes.json", array('role' => 'main'), 'GET');
            $theme_get = json_decode($theme_get['response']);
            $theme_id = $theme_get->themes[0]->id;
                
            /* for add class in cart tamplate */
            $theme_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=layout/theme.liquid&theme_id={$theme_id}", array(), 'GET');
        if ($theme_file_get['response'] != '') {
            $theme_file_get = json_decode($theme_file_get['response']);
            $data = $theme_file_get->asset->value;
            $position_snippets = strpos($data, "{{ '" . SITE_PATH . "/assets/js/activecart.js' | script_tag }}");
            if ($position_snippets === false) {
                $data = str_replace("</body>", "\r\n<!-- Active Cart Code Start -->\r\n{{ '" . SITE_PATH . "/assets/js/activecart.js' | script_tag }}\r\n<!-- Active Cart Code End --></body>", $data);
                $theme_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'layout/theme.liquid', 'value' => $data)), 'PUT');
            }
        }

        $cart_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=templates/cart.liquid&theme_id={$theme_id}", array(), 'GET');
        if ($cart_file_get['response'] != '') {
            $count_templates_cart = 0;
            $cart_file_get = json_decode($cart_file_get['response']);
            $item_price = '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>';
            $variables_item = array('{{ item.price | money }}' => $item_price,
                '{{ item.price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.price | money_with_currency }}' => $item_price,
                '{{ item.original_price | money }}' => $item_price,
                '{{ item.original_price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.original_price | money_with_currency }}' => $item_price
            );
            $asset_value = $cart_file_get->asset->value;
            $asset_value_bkp = $cart_file_get->asset->value;
            foreach ($variables_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $item_line_price = '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>';
            $variables_line_item = array('{{ item.line_price | money }}' => $item_line_price,
                '{{ item.line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.line_price | money_with_currency }}' => $item_line_price,
                '{{ item.original_line_price | money }}' => $item_line_price,
                '{{ item.original_line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.original_line_price | money_with_currency }}' => $item_line_price
            );
            foreach ($variables_line_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $cart_price = '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>';
            $variables_cart = array('{{ cart.total_price | money }}' => $cart_price,
                '{{ cart.total_price | money_without_trailing_zeros }}' => $cart_price,
                '{{ cart.total_price | money_with_currency }}' => $cart_price
            );
            foreach ($variables_cart as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            if ($count_templates_cart > 0) {
                $cart_file_get_bkp = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=templates/cart-auto-add-bkp.liquid&theme_id={$theme_id}", array(), 'GET');
                if ($cart_file_get_bkp['response'] == '') {
                    shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'templates/cart-auto-add-bkp.liquid', 'value' => $asset_value_bkp)), 'PUT');
                }
                shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'templates/cart.liquid', 'value' => $asset_value)), 'PUT');
            }
        }
        $cart_file_get = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=sections/cart-template.liquid&theme_id={$theme_id}", array(), 'GET');
        if ($cart_file_get['response'] != '') {
            $count_sections_cart = 0;
            $cart_file_get = json_decode($cart_file_get['response']);
            $item_price = '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>';
            $variables_item = array('{{ item.price | money }}' => $item_price,
                '{{ item.price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.price | money_with_currency }}' => $item_price,
                '{{ item.original_price | money }}' => $item_price,
                '{{ item.original_price | money_without_trailing_zeros }}' => $item_price,
                '{{ item.original_price | money_with_currency }}' => $item_price
            );
            $asset_value = $cart_file_get->asset->value;
            $asset_value_bkp = $cart_file_get->asset->value;
            foreach ($variables_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $item_line_price = '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>';
            $variables_line_item = array('{{ item.line_price | money }}' => $item_line_price,
                '{{ item.line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.line_price | money_with_currency }}' => $item_line_price,
                '{{ item.original_line_price | money }}' => $item_line_price,
                '{{ item.original_line_price | money_without_trailing_zeros }}' => $item_line_price,
                '{{ item.original_line_price | money_with_currency }}' => $item_line_price
            );
            foreach ($variables_line_item as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-item-line-price" data-key="{{item.product.id}}" data-variant-key="{{item.variant.id}}">{{ item.line_price | money }}</span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_templates_cart++;
                    }
                }
            }
            $cart_price = '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>';
            $variables_cart = array('{{ cart.total_price | money }}' => $cart_price,
                '{{ cart.total_price | money_without_trailing_zeros }}' => $cart_price,
                '{{ cart.total_price | money_with_currency }}' => $cart_price
            );
            foreach ($variables_cart as $key => $value) {
                $position_snippets = strpos($asset_value, '<span class="auto-cart-original-total">{{ cart.total_price | money }}</span><span class="auto-cart-total"></span>');
                if ($position_snippets === false) {
                    $asset_value = str_replace($key, $value, $asset_value, $count);
                    if ($count > 0) {
                        $count_sections_cart++;
                    }
                }
            }
            
            if ($count_sections_cart > 0) {
                $cart_file_get_bkp = shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json?asset[key]=sections/cart-template-auto-add-bkp.liquid&theme_id={$theme_id}", array(), 'GET');
                if ($cart_file_get_bkp['response'] == '') {
                    shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'sections/cart-template-auto-add-bkp.liquid', 'value' => $asset_value_bkp)), 'PUT');
                }
                shopify_call($token, $shop, SHOPIFY_API_VERSION."themes/{$theme_id}/assets.json", array('asset' => array('key' => 'sections/cart-template.liquid', 'value' => $asset_value)), 'PUT');
            }
        }

            header('Location: https://' . $shop . '/admin/apps/' . SHOPIFY_API_KEY);
            exit;
        }
    } else {
        $shop = isset($_POST['shop']) ? $_POST['shop'] : $_GET['shop'];
        $store_row = $db_connection->query("SELECT * FROM " . TABLE_CLIENT_STORES . " WHERE store_name = '$shop' and status='1'");
        /* Check store is active or not */
        if (isset($store_row) && $store_row->num_rows > 0) {
            $store_row = $store_row->fetch_object();
            $result_row = $db_connection->query("SELECT * FROM " . TABLE_CLIENTS . " WHERE client_id = $store_row->client_id");
            $result_row = $result_row->fetch_object();
            $db_connection->query("UPDATE " . TABLE_CLIENTS . " SET store_client_id = $store_row->store_client_id, status = 1 where client_id = $store_row->client_id");
            header('Location: ' . SITE_CLIENT_URL . '?shop=' . $shop);
        } else {
            $install_url = "https://" . $shop . "/admin/oauth/authorize?client_id=" . SHOPIFY_API_KEY . "&scope=" . urlencode(SHOPIFY_SCOPE) . "&redirect_uri=" . urlencode(SITE_PATH) . "&output=embed";
            header("Location: " . $install_url);
            exit;
        }
    }
}
?>
