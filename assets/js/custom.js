"use strict";
/* show loader */
function loading_show(class_name) {
    $('.' + class_name).addClass("Polaris-Button--loading").html('<span class="Polaris-Button__Content"><span class="Polaris-Button__Spinner"><svg viewBox="0 0 20 20" class="Polaris-Spinner Polaris-Spinner--colorInkLightest Polaris-Spinner--sizeSmall" aria-label="Loading" role="status"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z"></path></svg></span><span></span></span>').fadeIn('fast').attr('disabled', 'disabled');
}

/* hide loader */
function loading_hide(class_name, button_name) {
    $('.' + class_name).removeClass("Polaris-Button--loading").html('<span class="Polaris-Button__Content"><span>' + button_name + '</span></span>').removeAttr("disabled");
}

$.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results == null) {
        return null;
    }
    else {
        return results[1] || 0;
    }
}
$(document).ready(function () {

    /* Add/edit settings */
    $(document).on("submit", "#addClientSettingFrm", function (e) {
        e.preventDefault();
        var frmData = $(this).serialize();
        frmData += '&' + $.param({"addClientSetting": '', 'shop': shop});
        
        $.ajax({
            url: "ajax_responce.php",
            type: "post",
            dataType: "json",
            data: frmData,
            beforeSend: function () {
                loading_show('save_loader_show');
            },
            success: function (response) {
                if (response['result'] == 'success') {
                    $("#errorMsgBlock").hide();
                    flashNotice(response['msg']);
                } else if (response['msg_content'] != undefined && response['msg_heading'] != undefined) {
                    $("#Banner3Content").html(response['msg_content']);
                    $("#Banner3Heading").html(response['msg_heading']);
                    $("#errorMsgBlock").show();
                    $("html, body").animate({scrollTop: 0}, "slow");
                } else {
                    flashNotice(response['msg']);
                }
                loading_hide('save_loader_show', 'Save');
            }
        });
    });

    $('#type').change(function () {
        var value = $(this).val();
        if (value == '1') {
            $('#buy_x').show();
            $('#get_y').hide();
        } else {
            $('#get_y').show();
            $('#buy_x').hide();
        }
        $('.error-show').hide();
    });
    $('.close-product').click(function () {
        $('#product_modal').modal('hide');
        $('#product_buy_modal').modal('hide');
        $('#product_on_modal').modal('hide');
    });
    $('.add_products').click(function () {
        selectItem('ul-selected-product-list-' + $(this).data('type'), 'offer_product', 'product_modal', $(this).data('type'));
    });
    
    var page_cursor = '';
    $('#buy_of_product_select2,#get_of_product_select2,#on_of_product_select2').on('select2:open', function (e) {
        page_cursor = null;
    });
    
    $("#buy_of_product_select2").select2({
        placeholder: "Search for a repository",
        minimumInputLength: 3,
        ajax: {
            url: "ajax_responce.php",
            dataType: 'json',
            type: "post",
            quietMillis: 10,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    input_value: term, //search term
                    current_page: page, // page number
                    shop: shop,
                    ulproductlist: 'true',
                    get_input_name: 'offer_product',
                    type: 'buy',
                    api_name: 'products',
                    page_query: 'after',
                    page_cursor: page_cursor,
                };
            },
            results: function (data, page) {
                var loadMore = false;
                if(data['pagination'] != '') {
                    loadMore = (data['pagination']['page_info']['hasNextPage'] == true) ? true : false;
                }
                page_cursor = '';
                /* whether or not there are more results available */ 
                if(loadMore == true) {
                    page_cursor = data['pagination']['last_ele_cursor'];
                }
                // notice we return the value of more so Select2 knows if more results can be loaded
                return {results: data.products, more: loadMore };
            }
        },
        formatResult: repoFormatResult, // omitted for brevity, see the source of this page
        formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) {
            return m;
        } // we do not want to escape markup since we are displaying html in results
    });
    
    $("#get_of_product_select2").select2({
        placeholder: "Search product",
        minimumInputLength: 3,
        ajax: {
            url: "ajax_responce.php",
            dataType: 'json',
            type: "post",
            quietMillis: 10,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    input_value: term, //search term
                    current_page: page, // page number
                    shop: shop,
                    ulproductlist: 'true',
                    get_input_name: 'offer_product',
                    type: 'get',
                    api_name: 'products',
                    page_query: 'after',
                    page_cursor: page_cursor,
                };
            },
            results: function (data, page) {
                var loadMore = false;
                if(data['pagination'] != '') {
                    loadMore = (data['pagination']['page_info']['hasNextPage'] == true) ? true : false;
                }
                page_cursor = '';
                /* whether or not there are more results available */ 
                if(loadMore == true) {
                    page_cursor = data['pagination']['last_ele_cursor'];
                }
                // notice we return the value of more so Select2 knows if more results can be loaded
                return {results: data.products, more: loadMore };
            }
        },
        formatResult: repoFormatResult, // omitted for brevity, see the source of this page
        formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) {
            return m;
        } // we do not want to escape markup since we are displaying html in results
    });
    $("#on_of_product_select2").select2({
        placeholder: "Search for a repository",
        minimumInputLength: 3,
        ajax: {
            url: "ajax_responce.php",
            dataType: 'json',
            type: "post",
            quietMillis: 10,
            data: function (term, page) { // page is the one-based page number tracked by Select2
                return {
                    input_value: term, //search term
                    current_page: page, // page number
                    shop: shop,
                    ulproductlist: 'true',
                    get_input_name: 'offer_product',
                    type: 'on',
                    api_name: 'products',
                    page_query: 'after',
                    page_cursor: page_cursor,
                };
            },
            results: function (data, page) {
                var loadMore = false;
                if(data['pagination'] != '') {
                    loadMore = (data['pagination']['page_info']['hasNextPage'] == true) ? true : false;
                }
                page_cursor = '';
                /* whether or not there are more results available */ 
                if(loadMore == true) {
                    page_cursor = data['pagination']['last_ele_cursor'];
                }
                // notice we return the value of more so Select2 knows if more results can be loaded
                return {results: data.products, more: loadMore };
            }
        },
        formatResult: repoFormatResult, // omitted for brevity, see the source of this page
        formatSelection: repoFormatSelection, // omitted for brevity, see the source of this page
        dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
        escapeMarkup: function (m) {
            return m;
        } // we do not want to escape markup since we are displaying html in results
    });
    $('#addOffer').submit(function (e) {
        loading_show('offer_save');
        e.preventDefault();
        $.ajax({
            url: "ajax_responce.php",
            type: "post",
            data: $(this).serialize() + '&offer=true&shop=' + shop,
            success: function (result) {
                var edit = getCookie('edit');
                setCookie('edit','','-1');
                loading_hide('offer_save', 'Save');
                var result = $.parseJSON(result);
                if (result.status == 200) {
                    $('.error-show').hide();
                    if (result.id != "") {
                        $('#offer_id').val(result.id);
                    }
                    flashNotice(result.message, result.status);
                    if(edit == 1) {
                        setCookie('dashboard', '1', '1');
                        window.top.location.href = "index.php?shop=" + shop;
                    } else {
                        window.location.href = "index.php?shop=" + shop;
                    }
                    return (new $.Deferred()).promise();
                } else if (result.status == 400) {
                    flashNotice('Sorry, Something Went Wrong: Please try again later.', result.status);
                } else if (result.status == 401) {
                    if (result.count != undefined && result.message != undefined) {
                        showTopErroBlock(result.count, result.message, result.css_class);
                    }
                }
            }
        });
    });

    $(document).on("click", '.status_change', function () {
        var status = $(this).attr('data-status');
        var offer_id = $(this).attr('data-offer-id');
        var offer_type = $(this).attr('data-offer-type');
        if (offer_id != "") {
            loading_show('offer_' + offer_id + ' .status_change');
            $.ajax({
                url: "ajax_responce.php",
                type: "post",
                data: {'type': 'status', 'status': status, 'offer_id': offer_id, 'shop': shop, offer_type: offer_type},
                success: function (response) {
                    var objResponse = $.parseJSON(response);
                    if (objResponse.code == 200) {
                        if (status == '1') {
                            loading_hide('offer_' + offer_id + ' .status_change', '<a class="history-link" href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628C17.836 9.398 15.61 4 9.998 4S2.163 9.4 2.07 9.628c-.094.24-.094.505 0 .744C2.162 10.602 4.387 16 10 16s7.836-5.4 7.928-5.628c.094-.24.094-.505 0-.744zM9.998 14a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-6c-1.103 0-2 .896-2 2s.897 2 2 2a2 2 0 1 0 0-4z" fill="#212b35" fill-rule="evenodd"></path></svg></span></a>');
                            $('.offer_' + offer_id + ' .statusText').html('<span class="Polaris-Badge Polaris-Badge--statusSuccess"><span class="Polaris-VisuallyHidden">Success</span>Published</span>');
                            $('.offer_' + offer_id + ' .status_change').attr('data-status', '0');
                        } else {
                            loading_hide('offer_' + offer_id + ' .status_change', '<a class="history-link" href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M10 12a2 2 0 0 0 2-2c0-.178-.03-.348-.074-.512l5.78-5.78a1 1 0 1 0-1.413-1.415l-2.61 2.61A7.757 7.757 0 0 0 10 4C4.388 4 2.163 9.4 2.07 9.628a1.017 1.017 0 0 0 0 .744c.055.133.836 2.01 2.583 3.56l-2.36 2.36a1 1 0 1 0 1.414 1.415l5.78-5.78c.165.042.335.073.513.073zm-4-2a4 4 0 0 1 4-4c.742 0 1.432.208 2.025.56l-1.513 1.514A2.004 2.004 0 0 0 10 8a2 2 0 0 0-2 2c0 .178.03.347.074.51L6.56 12.026A3.96 3.96 0 0 1 6 10zm10.144-3.144l-2.252 2.252c.065.288.107.585.107.893a4 4 0 0 1-4 4c-.308 0-.604-.04-.892-.107l-1.682 1.68a7.903 7.903 0 0 0 2.573.428c5.612 0 7.836-5.398 7.928-5.628a1.004 1.004 0 0 0 0-.743c-.044-.112-.596-1.438-1.784-2.774z" fill="#212b35" fill-rule="evenodd"></path></svg></span></a>');
                            $('.offer_' + offer_id + ' .statusText').html('<span class="Polaris-Badge Polaris-Badge--statusWarning"><span class="Polaris-VisuallyHidden">Success</span>Unpublished</span>');
                            $('.offer_' + offer_id + ' .status_change').attr('data-status', '1');
                        }
                        flashNotice(objResponse.message);
                    } else if (objResponse.code == 400) {
                        if (objResponse.message == '' || objResponse.message == undefined) {
                            objResponse.message = 'Sorry, Something Went Wrong: Please try again later.';
                        } else {
                            loading_hide('offer_' + offer_id + ' .status_change', '<a class="history-link" href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M10 12a2 2 0 0 0 2-2c0-.178-.03-.348-.074-.512l5.78-5.78a1 1 0 1 0-1.413-1.415l-2.61 2.61A7.757 7.757 0 0 0 10 4C4.388 4 2.163 9.4 2.07 9.628a1.017 1.017 0 0 0 0 .744c.055.133.836 2.01 2.583 3.56l-2.36 2.36a1 1 0 1 0 1.414 1.415l5.78-5.78c.165.042.335.073.513.073zm-4-2a4 4 0 0 1 4-4c.742 0 1.432.208 2.025.56l-1.513 1.514A2.004 2.004 0 0 0 10 8a2 2 0 0 0-2 2c0 .178.03.347.074.51L6.56 12.026A3.96 3.96 0 0 1 6 10zm10.144-3.144l-2.252 2.252c.065.288.107.585.107.893a4 4 0 0 1-4 4c-.308 0-.604-.04-.892-.107l-1.682 1.68a7.903 7.903 0 0 0 2.573.428c5.612 0 7.836-5.398 7.928-5.628a1.004 1.004 0 0 0 0-.743c-.044-.112-.596-1.438-1.784-2.774z" fill="#212b35" fill-rule="evenodd"></path></svg></span></a>');
                            $('.offer_' + offer_id + ' .statusText').html('<span class="Polaris-Badge Polaris-Badge--statusWarning"><span class="Polaris-VisuallyHidden">Success</span>Unpublished</span>');
                        }
                        if (objResponse.count != undefined && objResponse.messages_li != undefined) {

                            loading_hide('offer_' + offer_id + ' .status_change', '<a class="history-link" href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M10 12a2 2 0 0 0 2-2c0-.178-.03-.348-.074-.512l5.78-5.78a1 1 0 1 0-1.413-1.415l-2.61 2.61A7.757 7.757 0 0 0 10 4C4.388 4 2.163 9.4 2.07 9.628a1.017 1.017 0 0 0 0 .744c.055.133.836 2.01 2.583 3.56l-2.36 2.36a1 1 0 1 0 1.414 1.415l5.78-5.78c.165.042.335.073.513.073zm-4-2a4 4 0 0 1 4-4c.742 0 1.432.208 2.025.56l-1.513 1.514A2.004 2.004 0 0 0 10 8a2 2 0 0 0-2 2c0 .178.03.347.074.51L6.56 12.026A3.96 3.96 0 0 1 6 10zm10.144-3.144l-2.252 2.252c.065.288.107.585.107.893a4 4 0 0 1-4 4c-.308 0-.604-.04-.892-.107l-1.682 1.68a7.903 7.903 0 0 0 2.573.428c5.612 0 7.836-5.398 7.928-5.628a1.004 1.004 0 0 0 0-.743c-.044-.112-.596-1.438-1.784-2.774z" fill="#212b35" fill-rule="evenodd"></path></svg></span></a>');
                            $('.offer_' + offer_id + ' .statusText').html('<span class="Polaris-Badge Polaris-Badge--statusWarning"><span class="Polaris-VisuallyHidden">Success</span>Unpublished</span>');

                            showTopErroBlock(objResponse.count, objResponse.messages_li);
                        } else {
                            flashNotice(objResponse.message, objResponse.code);
                        }
                    }
                }
            });
        }
    });
    $(document).on("click", '.delete', function () {
        var offer_id = $(this).attr('data-offer-id');
        if (offer_id != "") {
            loading_show('offer_' + offer_id + ' .delete');
            $.ajax({
                url: "ajax_responce.php",
                type: 'POST',
                data: {'offer_id': offer_id, 'shop': shop, 'delete': 'true'},
                success: function (result) {
                    var objResponse = $.parseJSON(result);
                    if (objResponse.code == 200) {
                        flashNotice(objResponse.message);
                        get_offer_load('1');
                    } else if (objResponse.code == 400) {
                        flashNotice('Sorry, Something Went Wrong: Please try again later.', objResponse.code);
                    }
                }
            });
        }
    });

    /*
     * For color picker 
     */
    $(".spectrum_color").spectrum({
        showButtons: false
    });

    $(".spectrum_color").on('move.spectrum', function (e, color) {
        var id = $(this).data('id');
        var hexVal = color.toHexString();
        $("[data-id='" + id + "']").val(hexVal);
    });

});
function showTopErroBlock(msgCnt, messages, css_class) {
    $('.error-show').show();
    var spell = '';
    if (msgCnt == 1) {
        spell = 'is ' + msgCnt + ' error:';
    } else {
        spell = 'are ' + msgCnt + ' errors:';
    }
    $('.error-show p').html('There ' + spell);
    $('.error-show ul li').html(messages);
    if (getCookie('edit') == 1) {
        tour.end();
    }
    if (css_class != undefined && css_class != '') {
        $(css_class).css({'color': '#ff0000'});
    }
//    $(window).scrollTop(0);
    $("html, body").animate({scrollTop: 0}, "slow");
}

function repoFormatResult(repo) {
    var markup = '<div id="ul-selected-product-list-buy" class="Polaris-ResourceList">' +
            '<li class="Polaris-ResourceList__ItemWrapper>' +
            '<div class="Polaris-ResourceList__Item Polaris-ResourceList__Item--link Polaris-ResourceList__Item--persistActions Polaris-ResourceList__Item--mediaThumbnail Polaris-ResourceList__Item--sizeMedium">' +
            '<div class="selected Polaris-ResourceList__Container">' +
            '<div class="Polaris-ResourceList__Media">' +
            '<span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall">'
            + repo.images +
            '</span>' +
            '</div>' +
            '<div class="Polaris-ResourceList__Content">' +
            '<div class="Polaris-ResourceList__Attributes">' +
            '<p class="Polaris-ResourceList__AttributeOne">' + repo.title + '</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</li>' +
            '</div>';
    return markup;
}

function repoFormatSelection(repo) {
    addSelectItem(repo);
    $('.select2-chosen').html('Search products');
    return '';//repo.title;
}

function ButtonActiveDeactive(class_name, button_class) {
    var value = '';
    $("." + class_name + ":checked").each(function () {
        value = $(this).val();
    });
    if (value != '') {
        $('.' + button_class).removeClass("Polaris-Button--disabled");
    } else {
        $('.' + button_class).addClass("Polaris-Button--disabled");
    }
}
var selectedproductarray = [];
var selectedproductbuyarray = [];
var selectedproductonarray = [];
function productSelected(val, type) {
    //alert(val);
    //alert(type);
    var data = val.split(",");
    if (data[1] == 'offer_product_' + type) {
        var product_id = data[0];
        if (type == "get" || type == "on") {
            var select_product = $("#select_product_" + type).val();
            var name = $('#checkbox_offer_product_' + type + data[0]).data('name');
            var image = $('#checkbox_offer_product_' + type + data[0]).data('image');
            var variant_id = $('#checkbox_offer_product_' + type + data[0]).data('variant-id');
            var price = $('#checkbox_offer_product_' + type + data[0]).data('price');
            if ($('#checkbox_offer_product_' + type + data[0]).is(':checked')) {
                if (select_product == "") {
                    $('#select_product_' + type).val(data[0]);
                    if (type == 'get') {
                        selectedproductarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                    } else {
                        selectedproductonarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                    }
                } else {
                    if (type == 'get') {
                        selectedproductarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                    } else {
                        selectedproductonarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                    }
                    $('#select_product_' + type).val(select_product + ',' + data[0]);
                }
            } else {
                select_product = select_product.split(",");
                for (var i = 0; i < select_product.length; i++) {
                    if (select_product[i] == data[0]) {
                        if (type == 'get') {
                            delete selectedproductarray[data[0]];
                        } else {
                            delete selectedproductonarray[data[0]];
                        }
                        delete selectedproductarray[data[0]];
                        select_product.splice($.inArray(data[0], select_product), 1);
                    }
                }
                $('#select_product_' + type).val(select_product.join());
            }

        } else {
            var name = $('#radio_offer_product_' + type + data[0]).data('name');
            var image = $('#radio_offer_product_' + type + data[0]).data('image');
            var variant_id = $('#radio_offer_product_' + type + data[0]).data('variant-id');
            var price = $('#radio_offer_product_' + type + data[0]).data('price');
            selectedproductbuyarray = [];
            selectedproductbuyarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
            $('#select_product_' + type).val(data[0]);
        }
        ButtonActiveDeactive(type + '_product', 'add_products_' + type);
    }
    //console.log(selectedproductarray);
}
function productSelected_fromselect2(val, type, name, image, variant_id, price) {
    var data = val.split(",");
    if (data[1] == 'offer_product_' + type) {
        var product_id = data[0];
        if (type == "get" || type == "on") {
            var select_product = $("#select_product_" + type).val();
            /*var name = $('#checkbox_offer_product_' + type + data[0]).data('name');
             var image = $('#checkbox_offer_product_' + type + data[0]).data('image');*/
            if (select_product == "") {
                $('#select_product_' + type).val(data[0]);
                if (type == 'get') {
                    selectedproductarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                } else {
                    selectedproductonarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                }
            } else {
                if (type == 'get') {
                    selectedproductarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                } else {
                    selectedproductonarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
                }
                $('#select_product_' + type).val(select_product + ',' + data[0]);
            }
        } else {
            var name = $('#radio_offer_product_' + type + data[0]).data('name');
            var image = $('#radio_offer_product_' + type + data[0]).data('image');
            selectedproductbuyarray = [];
            selectedproductbuyarray[product_id] = {title: name, image: image, variant_id: variant_id, price: price};
            $('#select_product_' + type).val(data[0]);
        }
        ButtonActiveDeactive(type + '_product', 'add_products_' + type);
    }
}

var k = 0;
function selectItem(list_name, get_input_name, modal_name, type) {
    var html = '';
    var function_value = '';
    var select_product = $("#select_product_" + type).val();
    //var select_product_variant = $('#' + type + '_of_product_variant_selected').val();
    var variant_ids = [];
    var product_price = [];
    select_product = select_product.split(",");
    if (type == 'buy') {
        var temp = selectedproductbuyarray;
    } else if (type == 'get') {
        var temp = selectedproductarray;
    } else {
        var temp = selectedproductonarray;
    }
    for (var i = 0; i < select_product.length; i++) {
        var id = select_product[i];
        var variant_id = temp[id]['variant_id'];
        var price = temp[id]['price'];
        //variant_ids = temp[id]['variant_id'].join();
        var product_id = id;
        var product_image = temp[id]['image'];
        if (product_image != '') {
            var image = '<img src="' + product_image + '" alt="' + product_image + '" class="Polaris-Thumbnail__Image">';
        } else {
            var image = '<div class="image-icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M19 0H1C.448 0 0 .448 0 1v18c0 .552.448 1 1 1h18c.552 0 1-.448 1-1V1c0-.552-.448-1-1-1zM8 6c.55 0 1 .45 1 1s-.45 1-1 1-1-.45-1-1 .45-1 1-1zm0 4c1.654 0 3-1.346 3-3S9.654 4 8 4 5 5.346 5 7s1.346 3 3 3zm-6 8v-2.434l3.972-2.383 2.473 1.65c.398.264.925.21 1.262-.126l4.367-4.367L18 13.48V18H2zM18 2v8.92l-3.375-2.7c-.398-.32-.973-.287-1.332.073l-4.42 4.42-2.318-1.545c-.322-.214-.74-.225-1.07-.025L2 13.233V2h16z" fill="#c4cdd5" fill-rule="evenodd"></path></svg></div>';
        }
        var product_title = temp[id]['title'];
        function_value = "'" + type + "_" + get_input_name + '-' + product_id + "'";
        html += '<li class="Polaris-ResourceList__ItemWrapper ' + type + '_' + get_input_name + '-' + product_id + '">' +
                '<div class="Polaris-ResourceList__Item Polaris-ResourceList__Item--link Polaris-ResourceList__Item--persistActions Polaris-ResourceList__Item--mediaThumbnail Polaris-ResourceList__Item--sizeMedium">' +
                '<div class="selected Polaris-ResourceList__Container" id="ResourceListItem' + product_id + '">' +
                '<div class="Polaris-ResourceList__Media">' +
                '<span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall">' + image + '</span>' +
                '</div>' +
                '<div class="Polaris-ResourceList__Content">' +
                '<div class="Polaris-ResourceList__Attributes">' +
                '<p class="Polaris-ResourceList__AttributeOne">' + product_title + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="Polaris-ResourceList">' +
                '<span class="Polaris-Icon Polaris-Icon--hasBackdrop" onclick="selectRemove(' + function_value + ',\'' + type + '\',\'' + variant_id + '\',\'' + price + '\')"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span>' +
                '</div>' +
                '</div>' +
                '</div>';
        '</li>';
        variant_ids.push(temp[id]['variant_id']);
        product_price.push(temp[id]['price']);
    }
    $('#' + type + '_of_product_selected').val(select_product.join(","));
    $('#' + type + '_of_product_variant_selected').val(variant_ids.join(","));
    $('#' + type + '_of_product_price_selected').val(product_price.join(","));
    if (type == 'get' || type == 'on') {
        $('#' + list_name).html(html);
        $('#' + modal_name).modal('hide');
        $('#product_' + type + '_modal').modal('hide');
    } else {
        $('#' + list_name).html(html);
        $('#product_buy_modal').modal('hide');
    }
}
function addSelectItem(product) {
    var product_id = product.id;
    var variant_id = product.variant_id;
    var product_title = product.title;
    var price = product.price;
    var image = product.image;
    var images = product.images;
    var get_input_name = product.get_input_name;
    var type = product.type;
    var function_value = "'" + type + "_" + get_input_name + '-' + product_id + "'";
    var list_name = 'ul-selected-product-list-' + type;
    if (type == 'buy') {
        var temp = selectedproductbuyarray;
    } else if (type == 'get') {
        var temp = selectedproductarray;
    } else {
        var temp = selectedproductonarray;
    }
    if (temp[product_id] == undefined) {
        var html = '<li class="Polaris-ResourceList__ItemWrapper ' + type + '_' + get_input_name + '-' + product_id + '">' +
                '<div class="Polaris-ResourceList__Item Polaris-ResourceList__Item--link Polaris-ResourceList__Item--persistActions Polaris-ResourceList__Item--mediaThumbnail Polaris-ResourceList__Item--sizeMedium">' +
                '<div class="selected Polaris-ResourceList__Container" id="ResourceListItem' + product_id + '">' +
                '<div class="Polaris-ResourceList__Media">' +
                '<span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall">' + images + '</span>' +
                '</div>' +
                '<div class="Polaris-ResourceList__Content">' +
                '<div class="Polaris-ResourceList__Attributes">' +
                '<p class="Polaris-ResourceList__AttributeOne">' + product_title + '</p>' +
                '</div>' +
                '</div>' +
                '<div class="Polaris-ResourceList">' +
                '<span class="Polaris-Icon Polaris-Icon--hasBackdrop" onclick="selectRemove(' + function_value + ',\'' + type + '\',\'' + variant_id + '\',\'' + price + '\')"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span>' +
                '</div>' +
                '</div>' +
                '</div>';
        if (type == 'get' || type == 'on') {
            var select_product = $('#' + type + '_of_product_selected').val();
            if (select_product == "") {
                $('#' + type + '_of_product_selected').val(product_id);
            } else {
                $('#' + type + '_of_product_selected').val(select_product + ',' + product_id);
            }
            var select_product_variant = $('#' + type + '_of_product_variant_selected').val();
            if (select_product_variant == "") {
                $('#' + type + '_of_product_variant_selected').val(variant_id);
            } else {
                $('#' + type + '_of_product_variant_selected').val(select_product_variant + ',' + variant_id);
            }
            var select_price_variant = $('#' + type + '_of_product_price_selected').val();
            if (select_price_variant == "") {
                $('#' + type + '_of_product_price_selected').val(price);
            } else {
                $('#' + type + '_of_product_price_selected').val(select_price_variant + ',' + price);
            }
        } else {
            $('#' + type + '_of_product_selected').val(product_id);
            $('#' + type + '_of_product_variant_selected').val(variant_id);
        }
        if (type == 'buy') {
            $('.buy_product').prop('checked', false);
            $('#radio_offer_product_' + type + product_id).prop('checked', true);
        } else {
            $('#checkbox_offer_product_' + type + product_id).prop('checked', true);
        }
        //console.log(selectedproductonarray);
        productSelected_fromselect2(product_id + ',offer_product_' + type, type, product_title, image, variant_id, price);

        if (type == 'get' || type == 'on') {
            $('#' + list_name).append(html);
        } else {
            $('#' + list_name).html(html);
        }
    }
}
function selectRemove(id, type, variant_id, price) {
    var data = id.split("-");
    var select_product = $("#select_product_" + type).val();
    select_product = select_product.split(",");
    for (var i = 0; i < select_product.length; i++) {
        if (select_product[i] == data[1]) {
            select_product.splice($.inArray(data[1], select_product), 1);
        }
    }
    $("#select_product_" + type).val(select_product.join());
    $('#' + type + '_of_product_selected').val(select_product.join());

    var select_product_variant = $('#' + type + '_of_product_variant_selected').val();
    if (type != "buy") {
        select_product_variant = select_product_variant.split(",");
    }
    for (var i = 0; i < select_product_variant.length; i++) {
        if (select_product_variant[i] == variant_id) {
            select_product_variant.splice($.inArray(variant_id, select_product_variant), 1);
        }
    }
    if (type != "buy") {
        $('#' + type + '_of_product_variant_selected').val(select_product_variant.join());
    }

    var select_product_price = $('#' + type + '_of_product_price_selected').val();
    if (type != "buy") {
        select_product_price = select_product_price.split(",");
        for (var i = 0; i < select_product_price.length; i++) {
            if (select_product_price[i] == price) {
                select_product_price.splice($.inArray(price, select_product_price), 1);
            }
        }
        $('#' + type + '_of_product_price_selected').val(select_product_price.join());
    }

    $('.' + data[0] + '-' + data[1]).remove();
    if (type == 'buy') {
        $('#radio_offer_product_' + type + data[1]).prop('checked', false);
    } else {
        $('#checkbox_offer_product_' + type + data[1]).prop('checked', false);
    }
    //$("#checkbox_" + data[0] + "_" + type + data[1]).prop("checked", false);
    if (data[0] == 'offer_product') {
        ButtonActiveDeactive('buy_product', 'add_products_' + type);
    }
}

var product_list;
var count;
var xhrlt;
var xhrbuyx;
var xhron;
function get_product_old(val, page, type) {
    if (xhrlt && xhrlt.readyState != 4) {
        xhrlt.abort();
    }
    $("#ul-" + type + "-list").html('<div class="product-loader loader"><svg viewBox="0 0 44 44" class="Polaris-Spinner Polaris-Spinner--colorTeal Polaris-Spinner--sizeLarge" role="status"><path d="M15.542 1.487A21.507 21.507 0 0 0 .5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 0 0-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 1 0-.9-2.863z"></path></svg></div>');
    xhrlt = $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {'input_value': val, 'shop': shop, 'ulproductlist': true, 'current_page': page},
        //async:true,
        success: function (response) {
            //alert(response);
            var objResponse = $.parseJSON(response);
            product_list = objResponse.products;
            count = objResponse.count;
            popup('ul-' + type + '-list', 'offer_product_get', product_list, type + '_product', 'add_products', count, page, type);
        }

    });
}

function get_product(val, page, type, page_cursor, page_query, api_name) {
    if (xhrlt && xhrlt.readyState != 4) {
        xhrlt.abort();
    }
    $("#ul-" + type + "-list").html('<div class="product-loader loader"><svg viewBox="0 0 44 44" class="Polaris-Spinner Polaris-Spinner--colorTeal Polaris-Spinner--sizeLarge" role="status"><path d="M15.542 1.487A21.507 21.507 0 0 0 .5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 0 0-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 1 0-.9-2.863z"></path></svg></div>');
    xhrlt = $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {
            input_value: val,
            shop: shop,
            ulproductlist: true,
            current_page: page,
            page_cursor: page_cursor,
            page_query: page_query,
            api_name: api_name
        },
        success: function (response) {
            var objResponse = $.parseJSON(response);
            product_list = objResponse.products;
            var pagination = objResponse.pagination;
            popup('ul-' + type + '-list', 'offer_product_get', product_list, type + '_product', 'add_products', page, type, pagination);
        }
    });
}

function get_product_forbuyx_old(val, page, type) {
    if (xhrbuyx && xhrbuyx.readyState != 4) {
        xhrbuyx.abort();
    }
    $("#ul-" + type + "-list").html('<div class="product-loader loader"><svg viewBox="0 0 44 44" class="Polaris-Spinner Polaris-Spinner--colorTeal Polaris-Spinner--sizeLarge" role="status"><path d="M15.542 1.487A21.507 21.507 0 0 0 .5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 0 0-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 1 0-.9-2.863z"></path></svg></div>');
    xhrbuyx = $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {'input_value': val, 'shop': shop, 'ulproductlist': true, 'current_page': page},
        //async:true,
        success: function (response) {
            //alert(response);
            var objResponse = $.parseJSON(response);
            product_list = objResponse.products;
            count = objResponse.count;
            popup('ul-' + type + '-list', 'offer_product_buy', product_list, type + '_product', 'add_products', count, page, type);
        }

    });
}

function get_product_forbuyx(val, page, type, page_cursor, page_query, api_name) {
    page_query = (typeof page_query != 'undefined') ? page_query : 'after';
    if (xhrbuyx && xhrbuyx.readyState != 4) {
        xhrbuyx.abort();
    }
    $("#ul-" + type + "-list").html('<div class="product-loader loader"><svg viewBox="0 0 44 44" class="Polaris-Spinner Polaris-Spinner--colorTeal Polaris-Spinner--sizeLarge" role="status"><path d="M15.542 1.487A21.507 21.507 0 0 0 .5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 0 0-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 1 0-.9-2.863z"></path></svg></div>');
    xhrbuyx = $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {
            input_value: val,
            shop: shop,
            ulproductlist: true,
            current_page: page,
            page_cursor: page_cursor,
            page_query: page_query,
            api_name: api_name
        },
        success: function (response) {
            var objResponse = $.parseJSON(response);
            product_list = objResponse.products;
            var pagination = objResponse.pagination;
            popup('ul-' + type + '-list', 'offer_product_buy', product_list, type + '_product', 'add_products', page, type, pagination);
        }

    });
}

function get_producton(val, page, type, page_cursor, page_query, api_name) {
    page_query = (typeof page_query != 'undefined') ? page_query : 'after';
    if (xhron && xhron.readyState != 4) {
        xhron.abort();
    }
    $("#ul-" + type + "-list").html('<div class="product-loader loader"><svg viewBox="0 0 44 44" class="Polaris-Spinner Polaris-Spinner--colorTeal Polaris-Spinner--sizeLarge" role="status"><path d="M15.542 1.487A21.507 21.507 0 0 0 .5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 0 0-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 1 0-.9-2.863z"></path></svg></div>');
    xhron = $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {
            input_value: val,
            shop: shop,
            ulproductlist: true,
            current_page: page,
            page_cursor: page_cursor,
            page_query: page_query,
            api_name: api_name
        },
        success: function (response) {
            var objResponse = $.parseJSON(response);
            product_list = objResponse.products;
            var pagination = objResponse.pagination;
            popup('ul-' + type + '-list', 'offer_product_on', product_list, type + '_product', 'add_products', page, type, pagination);
        }
    });
}

function product_records(val, page, type, page_cursor, page_query, api_name) {
    if (val.length >= 3 || val == "") {
        if (type == 'buy') {
            get_product_forbuyx(val, page, type, page_cursor, page_query, api_name);
        } else if (type == 'get') {
            get_product(val, page, type, page_cursor, page_query, api_name);
        } else {
            get_producton(val, page, type, page_cursor, page_query, api_name);
        }
    }
}

function popup(id, input_name, list, class_name, click_event, page, type, pagination) {
    var html = '';

    /* for selecting product */
    var select_product = $("#select_product_" + type).val();
    if (select_product != '') {
        select_product = ',' + $("#select_product_" + type).val();
    }

    /* for selected product */
    select_product = $('#' + type + '_of_product_selected').val() + select_product;
    select_product = select_product.split(',');
    if (list == '') {
        html += '<li class="Polaris-ResourceList__ItemWrapper">' +
                '<p class="Polaris-ResourceList__AttributeOne Records-Not-Found">Product records not found</p>' +
                '</li>';
    } else {
        list.forEach(function (item) {
            var checked = '';
            $('#' + id).html('');
            var product_id = String(item['id']);
            if (jQuery.inArray(product_id, select_product) != '-1') {
                checked = 'checked';
            }
            html += '<li class="Polaris-ResourceList__ItemWrapper">' +
                    '<div class="Polaris-ResourceList__Item Polaris-ResourceList__Item--link Polaris-ResourceList__Item--persistActions Polaris-ResourceList__Item--mediaThumbnail Polaris-ResourceList__Item--sizeMedium">' +
                    '<div class="list-container Polaris-ResourceList__Container">';
            if (type == 'buy') {
                html += '<div class="radiobtn">' +
                        '<label class="Polaris-Choice" for="radio_' + input_name + item['id'] + '">' +
                        '<span id="radio-hide-' + input_name + item['id'] + '" class="Polaris-Choice__Control">' +
                        '<div class="Polaris-RadioButton">' +
                        '<input data-variant-id="' + item['variant_id'] + '" data-image="' + item['image'] + '" data-name="' + item['title'] + '" data-price="' + item['price'] + '" id="radio_' + input_name + item['id'] + '" name="' + input_name + '" type="radio" value="' + item['id'] + ',' + input_name + '" class="Polaris-RadioButton__Input ' + class_name + '" onclick="productSelected(this.value,\'' + type + '\')"  aria-describedby="disabledHelpText" ' + checked + '>' +
                        '<div class="Polaris-RadioButton__Backdrop"></div>' +
                        '<div class="Polaris-RadioButton__Icon"></div>' +
                        '</div>' +
                        '</span>';
            } else {
                html += '<div id="checkbox">' +
                        '<label class="Polaris-Choice Polaris-Choice--labelHidden" for="checkbox_' + input_name + item['id'] + '">' +
                        '<span id="checkbox-hide-' + input_name + item['id'] + '" class="Polaris-Choice__Control">' +
                        '<div class="Polaris-Checkbox">' +
                        '<input data-variant-id="' + item['variant_id'] + '" data-image="' + item['image'] + '" data-name="' + item['title'] + '" data-price="' + item['price'] + '" type="checkbox" name="' + input_name + '" id="checkbox_' + input_name + item['id'] + '" value="' + item['id'] + ',' + input_name + '" class="Polaris-Checkbox__Input ' + class_name + '" onclick="productSelected(this.value,\'' + type + '\')" aria-invalid="false" ' + checked + '>' +
                        '<div class="Polaris-Checkbox__Backdrop"></div>' +
                        '<div class="Polaris-Checkbox__Icon">' +
                        '<span class="Polaris-Icon">' +
                        '<svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><g fill-rule="evenodd"><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path><path d="M8.315 13.859l-3.182-3.417a.506.506 0 0 1 0-.684l.643-.683a.437.437 0 0 1 .642 0l2.22 2.393 4.942-5.327a.437.437 0 0 1 .643 0l.643.684a.504.504 0 0 1 0 .683l-5.91 6.35a.437.437 0 0 1-.642 0"></path></g></svg>' +
                        '</span>' +
                        '</div>' +
                        '</div>' +
                        '</span>' +
                        '<span class="Polaris-Choice__Label"></span>';
            }
            html += '<div class="Polaris-ResourceList__Media">' +
                    '<span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall">' + item['images'] + '</span>' +
                    '</div>' +
                    '<div class="Polaris-ResourceList__Content">' +
                    '<div id="demo" class="Polaris-ResourceList__Attributes">' +
                    '<p class="Polaris-ResourceList__AttributeOne dep_buttons">' + item['title'] + '</p>' +
                    '</div>' +
                    '</div>' +
                    '<div id="label-show-' + input_name + item['id'] + '" class="alredy-addded Polaris-ResourceList">' +
                    '<span class="Polaris-Button__Content"><span>Already added</span></span>' +
                    '</div>' +
                    '</label>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<input  type="hidden" id="product_id_' + item['id'] + '" value="' + item['id'] + '">' +
                    '<input  type="hidden" id="product_image_' + item['id'] + '" value="' + item['images_src'] + '">' +
                    '<input  type="hidden" id="product_title_' + item['id'] + '" value="' + item['title'] + '">' +
                    '</li>';
        });
    }
    $('#' + id).html(html);
    var modal_pagination = '';
    var prevoius = parseInt(page) - 1;
    var next = parseInt(page) + 1;
    var pagination_prevoius = "'" + prevoius + ',' + input_name + "'";
    var pagination_next = "'" + next + ',' + input_name + "'";
    if (list != '' && pagination != '') {
        var next_button_color = "Polaris-Button--disabled";
        var nexpPageCursor = '';
        if(pagination['page_info']['hasNextPage'] == 1) {
            next_button_color = "";
            nexpPageCursor = pagination['last_ele_cursor'];
        } 
        
        var previous_button_color = "Polaris-Button--disabled";
        var prevPageCursor = '';
        if (pagination['page_info']['hasPreviousPage'] == 1) {
            previous_button_color = '';
            prevPageCursor = pagination['first_ele_cursor'];
        }
        modal_pagination = '<div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" onclick="modal_pagination(' + pagination_prevoius + ',\'' + type + '\', \'' + prevPageCursor + '\', \'before\', \'products\')" class="Polaris-Button tip ' + previous_button_color + '" data-hover="Previous"><span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2"  fill-rule="evenodd"></path></svg></span></span></span></a></div>' +
                '<div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)"  onclick="modal_pagination(' + pagination_next + ',\'' + type + '\', \'' + nexpPageCursor + '\', \'after\', \'products\')" class="Polaris-Button tip ' + next_button_color + '" data-hover="Next"><span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414"  fill-rule="evenodd"></path></svg></span></span></span></a></div>';
    }
    $("#modal_pagination_" + type).html(modal_pagination);
}
function modal_pagination(page, type, page_cursor, page_query, api_name) {
    var data = page.split(",");
    var title = $("#add_"+ type +"_products").val();
    product_records(title, data[0], type, page_cursor, page_query, api_name);
}
function flashNotice($message, status) {
    if (status == 400) {
        $(".inline-flash").addClass('error');
    }
    $(".inline-flash__message").html($message);
    $('.inline-flash-wrapper').addClass('inline-flash-wrapper--is-visible');
    setTimeout(function () {
        $('.inline-flash-wrapper').removeClass('inline-flash-wrapper--is-visible');
        $(".inline-flash").removeClass('error');
    }, 3000);
}

function get_offer_load(page) {
    $('#view-offers').html('<tr class="Polaris-ResourceList__ItemWrapper"><td class="load-offers" colspan="4"></td></tr>');
    loader('.load-offers');
    $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {'get_offer': true, 'shop': shop, 'current_page': page},
        //async:true,
        success: function (result) {
            var result = $.parseJSON(result);
            var html = '';
            var j = 0;
            var id;
            if (result.count == 0) {
                html += '<tr class="Polaris-ResourceList__ItemWrapper">' +
                        '<td colspan="4">No record found.</td>';
                '</tr>';
            } else {
                var isBtnDisabled = '', btnDisabledClass = '', deleteBtn = 0;
                result.data.forEach(function (offer) {
                    if ((j != 0 && result.plan == 0) || (result.plan == 0 && offer.type == 2)) {
                        isBtnDisabled = 'disabled=""';
                        btnDisabledClass = 'Polaris-Button--disabled';
                    }

                    if (j == 0) {
                        id = offer.id;
                    }
                    html += '<tr class="Polaris-ResourceList__ItemWrapper offer_' + offer.id + ' first_offer' + j + '">' +
                            '<td ><p class="Polaris-ResourceList__AttributeOne">' + offer.title + '</p></td>';
                    html += '<td>' + offer.type_text + '</td>';
                    if (offer.status == 1) {
                        html += '<td class="statusText"><span class="Polaris-Badge Polaris-Badge--statusSuccess">Published</span></td>';
                    } else {
                        html += '<td class="statusText"><span class="Polaris-Badge Polaris-Badge--statusWarning">Unpublished</span></td>';
                    }
                    html += '<td>' +
                            '<div class="Polaris-ButtonGroup Polaris-ButtonGroup--segmented highlight-text">';
                    /* Change Status Button */
                    if (offer.status == 1) {
                        html += '<div class="Polaris-ButtonGroup__Item highlight-text Statusbutton ' + btnDisabledClass + '" ' + isBtnDisabled + '><span class="Polaris-Button Polaris-Button--sizeSlim tip status_change" data-hover="Status" data-status="0" data-offer-type=' + offer.type + ' data-offer-id=' + offer.id + '><a class="history-link" href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.928 9.628C17.836 9.398 15.61 4 9.998 4S2.163 9.4 2.07 9.628c-.094.24-.094.505 0 .744C2.162 10.602 4.387 16 10 16s7.836-5.4 7.928-5.628c.094-.24.094-.505 0-.744zM9.998 14a4 4 0 1 1 0-8 4 4 0 0 1 0 8zm0-6c-1.103 0-2 .896-2 2s.897 2 2 2a2 2 0 1 0 0-4z" fill="#212b35" fill-rule="evenodd"></path></svg></a></span></div>';
                    } else {
                        html += '<div class="Polaris-ButtonGroup__Item highlight-text Statusbutton ' + btnDisabledClass + '" ' + isBtnDisabled + '><span class="Polaris-Button Polaris-Button--sizeSlim tip status_change" data-hover="Status" data-status="1" data-offer-type=' + offer.type + ' data-offer-id=' + offer.id + '><a class="history-link" href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M10 12a2 2 0 0 0 2-2c0-.178-.03-.348-.074-.512l5.78-5.78a1 1 0 1 0-1.413-1.415l-2.61 2.61A7.757 7.757 0 0 0 10 4C4.388 4 2.163 9.4 2.07 9.628a1.017 1.017 0 0 0 0 .744c.055.133.836 2.01 2.583 3.56l-2.36 2.36a1 1 0 1 0 1.414 1.415l5.78-5.78c.165.042.335.073.513.073zm-4-2a4 4 0 0 1 4-4c.742 0 1.432.208 2.025.56l-1.513 1.514A2.004 2.004 0 0 0 10 8a2 2 0 0 0-2 2c0 .178.03.347.074.51L6.56 12.026A3.96 3.96 0 0 1 6 10zm10.144-3.144l-2.252 2.252c.065.288.107.585.107.893a4 4 0 0 1-4 4c-.308 0-.604-.04-.892-.107l-1.682 1.68a7.903 7.903 0 0 0 2.573.428c5.612 0 7.836-5.398 7.928-5.628a1.004 1.004 0 0 0 0-.743c-.044-.112-.596-1.438-1.784-2.774z" fill="#212b35" fill-rule="evenodd"></path></svg></a></span></div>';
                    }
                    /* Change Status Button END */

                    /* Edit Button */
                    html += '<div class="Polaris-ButtonGroup__Item highlight-text Editbutton ' + btnDisabledClass + '" ' + isBtnDisabled + '><span class="Polaris-Button Polaris-Button--sizeSlim tip" data-hover="Edit"><a class="history-link" href="special_offer.php?id=' + offer.id + '&shop=' + shop + '"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 4.293l-4-4C13.52.105 13.265 0 13 0H3c-.552 0-1 .448-1 1v18c0 .552.448 1 1 1h14c.552 0 1-.448 1-1V5c0-.265-.105-.52-.293-.707zM16 18H4V2h8.586L16 5.414V18zm-3-5H7c-.552 0-1 .448-1 1s.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1zm-7-3c0 .552.448 1 1 1h6c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1zm1-3h1c.552 0 1-.448 1-1s-.448-1-1-1H7c-.552 0-1 .448-1 1s.448 1 1 1z" fill="#000" fill-rule="evenodd"></path></svg></a></span></div>';

                    /* Delete Button */
                    if (j != 0 || result.plan != 0 || offer.type != 1) {
                        html += '<div class="Polaris-ButtonGroup__Item highlight-text ' + btnDisabledClass + '" ' + isBtnDisabled + '><span class="Polaris-Button Polaris-Button--sizeSlim tip delete" data-hover="Delete" data-offer-id=' + offer.id + '><a class="history-link"  href="javascript:void(0)"><span class="Polaris-custom-icon Polaris-Icon Polaris-Icon--hasBackdrop"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M16 6a1 1 0 1 1 0 2h-1v9a1 1 0 0 1-1 1H6a1 1 0 0 1-1-1V8H4a1 1 0 1 1 0-2h12zM9 4a1 1 0 1 1 0-2h2a1 1 0 1 1 0 2H9zm2 12h2V8h-2v8zm-4 0h2V8H7v8z" fill="#000" fill-rule="evenodd"></path></svg></a></span></div>';
                    }

                    html += '</div>' +
                            '</td>' +
                            '</tr>';
                    j = j + 1;

                });
            }
            var prevoius = parseInt(page) - 1;
            var next = parseInt(page) + 1;
            var pagination = '';
            if (result.plan == 1) {
                if (result.page > 1) {
                    $('.pagination').show();
                    if (page == 1) {
                        var previous_button_color = "Polaris-Button--disabled";
                    } else {
                        var previous_button_color = "";
                    }
                    if (page < result.page) {
                        var next_button_color = "";
                    } else {
                        var next_button_color = "Polaris-Button--disabled";
                    }
                    pagination = '<div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)" onclick="get_offer_load(\'' + prevoius + '\')" class="Polaris-Button tip ' + previous_button_color + '" data-hover="Previous"><span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17 9H5.414l3.293-3.293a.999.999 0 1 0-1.414-1.414l-5 5a.999.999 0 0 0 0 1.414l5 5a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L5.414 11H17a1 1 0 1 0 0-2"  fill-rule="evenodd"></path></svg></span></span></span></a></div>' +
                            '<div class="Polaris-ButtonGroup__Item"><a href="javascript:void(0)"  onclick="get_offer_load(\'' + next + '\')" class="Polaris-Button tip ' + next_button_color + '" data-hover="Next"><span class="Polaris-Button__Content"><span><span class="Polaris-Icon"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M17.707 9.293l-5-5a.999.999 0 1 0-1.414 1.414L14.586 9H3a1 1 0 1 0 0 2h11.586l-3.293 3.293a.999.999 0 1 0 1.414 1.414l5-5a.999.999 0 0 0 0-1.414"  fill-rule="evenodd"></path></svg></span></span></span></a></div>';
                }
                $('#pagination-offers').html(pagination);
            }
            $('#view-offers').html(html);
            setTimeout(function () {
                if (tour_status == '0' && getCookie('dashboard') != '1') {
                    var tour = new Tour({
                        storage: false,
                        backdrop: true,
                        template: "<div class='popover tour'>\n\
                            <div class='arrow'></div>\n\
                            <h3 class='popover-title'></h3>\n\
                            <div class='popover-content'></div>\n\
                            <div class='popover-navigation'>\n\
                                <button class='btn btn-default' data-role='prev'>« Prev</button>\n\
                                <button class='btn btn-default' data-role='next'>Next »</button>\n\
                                </div></div>",
                        onEnd: function () {
                            //document.cookie = "edit=1";
                            setCookie('edit', '1', '1');
                        },
                        steps: [{
                                element: ".first_offer0 .Editbutton",
                                title: "Edit campaign",
                                content: "Edit the appropriate details of your campaign with what kind of offer you'd like to provide.",
                                placement: "bottom",
                                backdropContainer: 'body',
                                backdropPadding: 0,
                                onNext: function () {
                                    //document.cookie = "edit=1";
                                    setCookie('edit', '1', '1');
                                    window.top.location.href = 'special_offer.php?shop=' + shop + '&id=' + id;
                                    return (new $.Deferred()).promise();
                                }
                            }, {
                                element: ".first_offer1 .Editbutton",
                                title: "Edit campaign",
                                content: "Edit the appropriate details of your campaign with what kind of offer you'd like to provide.",
                                placement: "bottom",
                                backdropContainer: 'body',
                                backdropPadding: 0
                            }
                        ]});
                    // Initialize the tour
                    tour.init();
                    tour.restart();
                }

            }, 100);
        }
    });

}
function loader(name) {
    $(name).html('<div class="product-loader loader"><svg viewBox="0 0 44 44" class="Polaris-Spinner Polaris-Spinner--colorTeal Polaris-Spinner--sizeLarge" role="status"><path d="M15.542 1.487A21.507 21.507 0 0 0 .5 22c0 11.874 9.626 21.5 21.5 21.5 9.847 0 18.364-6.675 20.809-16.072a1.5 1.5 0 0 0-2.904-.756C37.803 34.755 30.473 40.5 22 40.5 11.783 40.5 3.5 32.217 3.5 22c0-8.137 5.3-15.247 12.942-17.65a1.5 1.5 0 1 0-.9-2.863z"></path></svg></div>');
}

function load_selected_product(type, ids) {
    loader('#ul-selected-product-list-' + type);
    var type = type;
    var get_input_name = 'offer_product'
    $.ajax({
        url: "ajax_responce.php",
        type: "post",
        data: {'get_selected_product': true, 'shop': shop, 'ids': ids},
        //async:true,
        success: function (result) {
            var result = $.parseJSON(result);
            var html = '';
            result.products.forEach(function (products) {
                var function_value = "'" + type + "_" + get_input_name + '-' + products.id + "'";
                html += '<li class="Polaris-ResourceList__ItemWrapper ' + type + '_' + get_input_name + '-' + products.id + '">' +
                        '<div class="Polaris-ResourceList__Item Polaris-ResourceList__Item--link Polaris-ResourceList__Item--persistActions Polaris-ResourceList__Item--mediaThumbnail Polaris-ResourceList__Item--sizeMedium">' +
                        '<div class="selected Polaris-ResourceList__Container" id="ResourceListItem' + products.id + '">' +
                        '<div class="Polaris-ResourceList__Media">' +
                        '<span class="Polaris-Thumbnail Polaris-Thumbnail--sizeSmall">'
                        + products.images +
                        '</span>' +
                        '</div>' +
                        '<div class="Polaris-ResourceList__Content">' +
                        '<div class="Polaris-ResourceList__Attributes">' +
                        '<p class="Polaris-ResourceList__AttributeOne">' + products.title + '</p>' +
                        '</div>' +
                        '</div>' +
                        '<div class="Polaris-ResourceList">' +
                        '<span class="Polaris-Icon Polaris-Icon--hasBackdrop" onclick="selectRemove(' + function_value + ',\'' + type + '\',\'' + products.variant_id + '\',\'' + products.price + '\')"><svg class="Polaris-Icon__Svg" viewBox="0 0 20 20"><path d="M11.414 10l4.293-4.293a.999.999 0 1 0-1.414-1.414L10 8.586 5.707 4.293a.999.999 0 1 0-1.414 1.414L8.586 10l-4.293 4.293a.999.999 0 1 0 1.414 1.414L10 11.414l4.293 4.293a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414L11.414 10z" fill="#95a7b7" fill-rule="evenodd"></path></svg></span>' +
                        '</div>' +
                        '</div>' +
                        '</li>';
                if (type == 'buy') {
                    $('.buy_product').prop('checked', false);
                    $('#radio_offer_product_' + type + products.id).prop('checked', true);
                } else {
                    $('#checkbox_offer_product_' + type + products.id).prop('checked', true);
                }
                //console.log(selectedproductonarray);
                productSelected_fromselect2(products.id + ',offer_product_' + type, type, products.title, products.image, products.variant_id, products.price);
            });
            $('#ul-selected-product-list-' + type).html(html);
        }
    });
}

function getCookie(name)
{
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++)
    {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == name)
        {
            return unescape(y);
        }
    }
}
function eraseCookie(name) {
//    document.cookie = name + '=; Max-Age=-99999999;';
document.cookie = name + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    //document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/; SameSite=None; Secure";
}
