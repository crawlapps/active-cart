var auto_addurl = "https://www.activecartapp.com/activecart/activecart.php";
var shop = Shopify.shop;
var flagACtivecart = false;
if (shop == "crawlapps-info.myshopify.com") {
   var auto_addurl = "http://localhost/private-apps/activecart/activecart.php";
}
if (typeof $ == 'undefined') {
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState) {  //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" ||
                    script.readyState == "complete") {
                flagACtivecart = true;
                script.onreadystatechange = null;
                callbackAutoAdd();
            }
        };
    } else {  //Others
        script.onload = function () {
            flagACtivecart = true;
            callbackAutoAdd();
        };
    }
    script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js';
    document.getElementsByTagName("head")[0].appendChild(script);
} else {
    var $ = jQuery.noConflict();
    flagACtivecart = true;
    callbackAutoAdd();
}
function callbackAutoAdd() {
    var shop = Shopify.shop;
    var path = __st.pageurl;
    var cart = path.search("/cart");
    if (cart > 0) {
        window.auto = {
            shop: shop, cart: null, 'main_cart_total': null,
        }
        function StartCart() {
            jQuery.ajax({
                cache: false,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                type: "POST",
                url: "cart.js",
                success: function (res) {
                    //if (res.item_count > 0) {
                    window.auto.cart = res;
                    StartApp();
                    //}
                }
            });
        }
        function StartApp() {
            var action = 'autoadd';
            var url_param = getParameterByName("auto-add-display-discount");
            if (url_param == 1) {
                action = 'auto-add-display-discount';
            }
            jQuery.ajax({
                url: auto_addurl + '?action=' + action,
                type: "POST",
                dataType: "json",
                data: JSON.stringify(window.auto),
                success: function (data) {
                    if (data.cart_reload == 1) {
                        var cart_items = data.cart_items;
                        var i;
                        var items = "";
                        for (var pid in cart_items) { //for (i = 0; i < cart_items.length; i++) {
                            cart_items[pid]['quantity'] = (cart_items[pid]['quantity'] != undefined) ? cart_items[pid]['quantity'] : 1;
                            if (items == "") {
                                items += 'updates[' + cart_items[pid]['variant_id'] + ']=' + cart_items[pid]['quantity'];
                            } else {
                                items += '&updates[' + cart_items[pid]['variant_id'] + ']=' + cart_items[pid]['quantity'];
                            }
                        }

                        jQuery.ajax({
                            url: "/cart/update.js",
                            type: "POST",
                            dataType: "json",
                            data: items,
                            success: function () {
                                var url = addParameterToURL('auto-add-display-discount=1');
                                window.location.href = url;
                                //location.reload();
                            },
                            error: function () {
                            }
                        });

                    } else {
                        var auto_add_items = data.auto_add_items;
                        window.auto.auto_add_items = auto_add_items;
                        window.auto.auto_add_items_detail_arr = data.auto_add_items_detail_arr;
                        var money_format = data.money_format;
                        var j;

                        if (parseInt(data.is_spend_offer) > 0) {
                            /* spen offer block */
                            $("#spendDiscountBlock").remove();
                            $("form[action='/cart']").prepend(data.spend_offer_html);
                            /* end spen offer block */
                        }

                        if (auto_add_items.length != '0') {
                            for (j = 0; j < auto_add_items.length; j++) {
                                var original_price = autoShopifyformatMoney(data.original_item_price[auto_add_items[j]], money_format);
                                var discounted_price = autoShopifyformatMoney(data.discounted_item_price[auto_add_items[j]], money_format);

                                var original_line_price = autoShopifyformatMoney(data.original_line_item_price[auto_add_items[j]], money_format);
                                var discounted_line_price = autoShopifyformatMoney(data.discounted_line_item_price[auto_add_items[j]], money_format);

                                var id = auto_add_items[j];
                                $(".auto-cart-item-price[data-variant-key='" + id + "']").html("<span class=\"original_price\">" + original_price + "</span>" + "<span class='discounted_price'>" + discounted_price + "</span>");
                                $(".auto-cart-item-line-price[data-variant-key='" + id + "']").html("<span class=\"original_price\">" + original_line_price + "</span>" + "<span class='discounted_price'>" + discounted_line_price + "</span>");
                            }
                            $(".auto-cart-original-total ,.original_price").css({"text-decoration": "line-through"});
                            $(".discounted_price ,.auto-cart-total").css({"display": "block"});
                            $(".auto-cart-total").html(autoShopifyformatMoney(data.main_total, money_format));

                            var sourceURL = window.location.href;
                            var newUrl = removeParam('auto-add-display-discount', sourceURL);
                            window.history.pushState('', '', newUrl);
                        }
                    }
                }
            });
        }
        function autoShopifyformatMoney(cents, format) {
            if (typeof cents == "undefined" || cents == null) {
                return ""
            }
            if (typeof cents == "string" && cents.length == 0) {
                return ""
            }

            var value = "",
                    placeholderRegex = /\{\{\s*(\w+)\s*\}\}/,
                    formatString = format || this.money_format;
            if (typeof cents == "string") {
                cents = cents.replace(".", "")
            }

            function defaultOption(opt, def) {
                return typeof opt == "undefined" ? def : opt
            }

            function formatWithDelimiters(number, precision, thousands, decimal) {
                precision = defaultOption(precision, 2);
                thousands = defaultOption(thousands, ",");
                decimal = defaultOption(decimal, ".");
                if (isNaN(number) || number == null) {
                    return 0
                }
                number = (number / 100).toFixed(precision);
                var parts = number.split("."),
                        dollars = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1" + thousands),
                        cents = parts[1] ? decimal + parts[1] : "";
                return dollars + cents
            }
            switch (formatString.match(placeholderRegex)[1]) {
                case "amount":
                    value = formatWithDelimiters(cents, 2);
                    break;
                case "amount_no_decimals":
                    value = formatWithDelimiters(cents, 0);
                    break;
                case "amount_with_comma_separator":
                    value = formatWithDelimiters(cents, 2, ".", ",");
                    break;
                case "amount_no_decimals_with_comma_separator":
                    value = formatWithDelimiters(cents, 0, ".", ",");
                    break
            }
            return formatString.replace(placeholderRegex, value)
        }
        var autoCheckoutButtonSelector = ["[name='checkout']", "[href='/checkout']", "form[action='/checkout'] input[type='submit']", "input[name='checkout']", "button[name='checkout']", "button.new_checkout_button", "input[value='Checkout']", "input[value='Check out']", "button:contains('Checkout'):not(.cart_button)", "button:contains('Check out'):not(.cart_button)", "a[href='/checkout']", "input[name='goto_pp']", "button.additional-checkout-button", "div.additional-checkout-button", "#paypal-express-button", ".additional-checkout-button--apple-pay"].join(", ");
        var directCheckoutButtonSelector = ["button:contains('Checkout'):not(.cart_button)", "button:contains('Check Out'):not(.cart_button)", "button:contains('Check out'):not(.cart_button)", "button:contains('CHECKOUT'):not(.cart_button)"].join(", ");

        jQuery(document).on("click.olEvents", autoCheckoutButtonSelector, StartCheckout);

        function StartCheckout(e, clicked_element) {
            var spend_offer_id = jQuery('input[name=spend_discount]:checked').val();
            if ((parseInt(spend_offer_id) > 0) || (window.auto.auto_add_items != undefined && window.auto.auto_add_items.length > 0)) {
                var note_attributes = [];
                jQuery("[name^='attributes']").each(function () {
                    var $a = jQuery(this);
                    var name = jQuery(this).attr("name");
                    name = name.replace(/^attributes\[/i, "").replace(/\]$/i, "");
                    var v = {
                        name: name,
                        value: $a.val()
                    };
                    if (v.value == "") {
                        return
                    }
                    switch ($a[0].tagName.toLowerCase()) {
                        case "input":
                            if ($a.attr("type") == "checkbox") {
                                if ($a.is(":checked")) {
                                    note_attributes.push(v)
                                }
                            } else {
                                note_attributes.push(v)
                            }
                            break;
                        default:
                            note_attributes.push(v)
                    }
                });

                var note = "";
                if (jQuery("[name='note']").length == 1 && jQuery("[name='note']")[0].value) {
                    note = jQuery("[name='note']")[0].value
                }


                window.auto.spend_offer_id = spend_offer_id;
                window.auto.cart.note_attributes = note_attributes;
                window.auto.cart.note = note;
                if (__st.cid != undefined) {
                    window.auto.customer_id = __st.cid;
                }

                e.preventDefault();
                jQuery.ajax({
                    dataType: "json",
                    type: "POST",
                    url: auto_addurl + '?action=checkout',
                    data: JSON.stringify(window.auto),
                    success: function (data) {
                        if (data.invoice_url != "") {
                            window.location.href = data.invoice_url.substring(0, 2e3)
                        } else {
                            alert("test");
                        }
                    },
                    error: function () {
                    }
                });
            }
        }

        StartCart();
    }
    function addParameterToURL(param) {
        _url = location.href;
        _url += (_url.split('?')[1] ? '&' : '?') + param;
        return _url;
    }

    function getParameterByName(name) {
        var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
        return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
    }

    function removeParam(key, sourceURL) {
        var rtn = sourceURL.split("?")[0],
                param,
                params_arr = [],
                queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
        if (queryString !== "") {
            params_arr = queryString.split("&");
            for (var i = params_arr.length - 1; i >= 0; i -= 1) {
                param = params_arr[i].split("=")[0];
                if (param === key) {
                    params_arr.splice(i, 1);
                }
            }
            rtn = rtn + "?" + params_arr.join("&");
        }
        return rtrim(rtn, '?');
    }
    function rtrim(str, chars) {
        chars = chars || "\s";
        return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
    }
}

setInterval(function () {
        if(flagACtivecart == true){
            jQuery(document).ajaxSuccess(function (a, b, c) {
                if (c.url === '/cart/add.js' || c.url === '/cart/change.js') {
                    console.log('redirect work 1');
                    location = window.location.href;
                }
            });
            $(document).ajaxSuccess(function (a, b, c) {
                if (c.url === '/cart/add.js' || c.url === '/cart/change.js') {
                    console.log('redirect work 1');
                    location = window.location.href;
                }
            });
//            jQuery(document).ajaxComplete(function (a, b, c) {
//                if (c.url === '/cart/add.js' || c.url === '/cart/change.js') {
//                    console.log('redirect work 1');
//                    location = window.location.href;
//                }
//            });
//            $(document).ajaxComplete(function (a, b, c) {
//                if (c.url === '/cart/add.js' || c.url === '/cart/change.js') {
//                    console.log('redirect work 1');
//                    location = window.location.href;
//                }
//            });
            flagACtivecart = false;
        }
}, 1000);
